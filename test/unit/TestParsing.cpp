#include "Utils.hpp"

#include <common/CustomEntity.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

const auto filesPath =
    std::filesystem::current_path().parent_path().parent_path().parent_path() /
    "test/unit/files";

TEST_CASE("read_whole_file")
{
  constexpr auto testFolder = "read_whole_file";

  {
    const auto path               = filesPath / testFolder / "test01";
    const std::string fileContent = read_whole_file(path);

    const std::string expected = R"(test01)";
    REQUIRE(fileContent == expected);
  }

  {
    const auto path               = filesPath / testFolder / "test02";
    const std::string fileContent = read_whole_file(path);

    const std::string expected = R"(test02
)";

    REQUIRE(fileContent == expected);
  }

  {
    const auto path               = filesPath / testFolder / "test03";
    const std::string fileContent = read_whole_file(path);

    const std::string expected = R"(test03




test03 end)";

    REQUIRE(fileContent == expected);
  }

  {
    const auto path               = filesPath / testFolder / "test04";
    const std::string fileContent = read_whole_file(path);

    const std::string expected =
        R"({
"classname" "worldspawn"
"defaultctf" "0"
"defaultteam" "0"
"mapteams" "grunt;scientist"
"newunit" "0"
"gametitle" "0"
"startdark" "0"
"MaxRange" "4096"
"sounds" "1"
"origin" "0 0 0"
"mapversion" "220"
}
{
"classname" "remec_multi_bhop_block"
"delay" "0.12500000"
"delay2" "1.000000"
"zhlt_clipnodedetaillevel" "1"
"zhlt_coplanarpriority" "0"
"zhlt_chopup" "0"
"zhlt_chopdown" "0"
"zhlt_detaillevel" "1"
"targetname" "//{}name{}//"
"destination" "//{}target{}//"
{
( 32 32 32 ) ( 32 32 -32 ) ( 32 -32 32 ) NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -32 -32 32 ) ( -32 -32 -32 ) ( -32 32 32 ) NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 32 -32 32 ) ( 32 -32 -32 ) ( -32 -32 32 ) NULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -32 32 32 ) ( -32 32 -32 ) ( 32 32 32 ) NULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -32 32 -32 ) ( -32 -32 -32 ) ( 32 32 -32 ) NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 32 -32 32 ) ( -32 -32 32 ) ( 32 32 32 ) NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
}
}
)";

    REQUIRE(fileContent == expected);
  }
}

TEST_CASE("erase_comments")
{
  constexpr auto testFolder = "erase_comments";

  {
    const auto path         = filesPath / testFolder / "test01";
    std::string fileContent = read_whole_file(path);
    std::string result      = erase_comments(fileContent);

    const auto pathExpected    = filesPath / testFolder / "test01_expected";
    const std::string expected = read_whole_file(pathExpected);
    REQUIRE(result == expected);
  }

  {
    const auto path         = filesPath / testFolder / "test02";
    std::string fileContent = read_whole_file(path);
    std::string result      = erase_comments(fileContent);

    const auto pathExpected    = filesPath / testFolder / "test02_expected";
    const std::string expected = read_whole_file(pathExpected);
    REQUIRE(result == expected);
  }

  {
    const auto path         = filesPath / testFolder / "test03";
    std::string fileContent = read_whole_file(path);
    std::string result      = erase_comments(fileContent);

    const auto pathExpected    = filesPath / testFolder / "test03_expected";
    const std::string expected = read_whole_file(pathExpected);
    REQUIRE(result == expected);
  }

  {
    const auto path         = filesPath / testFolder / "test04";
    std::string fileContent = read_whole_file(path);
    std::string result      = erase_comments(fileContent);

    const auto pathExpected    = filesPath / testFolder / "test04_expected";
    const std::string expected = read_whole_file(pathExpected);
    REQUIRE(result == expected);
  }
}

TEST_CASE("parse_entity")
{
  static std::unordered_map<std::string, bool> solidMapping {
      {"remec_multi_bhop_block", true}};

  {
    const std::string entityStr = R"({
"classname" "remec_multi_bhop_block"
"d//elay" "{}0.100000"
"delay2" "1.000000"
"{{zhlt_clipnodedetaillevel" "1"
"zhlt_coplanarpriority" "0"
"zhlt_chopup" "0"
"zhlt_chopdown" "0"
"zhlt_detaillevel" "1"
"targetname" "a"
"destination" "b"
{
( -32 -32 -160 ) ( -32 -32 -224 ) ( -32 32 -160 ) {{NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 32 32 -160 ) ( 32 32 -224 ) ( 32 -32 -160 ) }}NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -32 32 -160 ) ( -32 32 -224 ) ( 32 32 -160 ) }}}}NULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 32 -32 -160 ) ( 32 -32 -224 ) ( -32 -32 -160 ) {}{}NULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 32 -32 -160 ) ( -32 -32 -160 ) ( 32 32 -160 ) NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( -32 32 -224 ) ( -32 -32 -224 ) ( 32 32 -224 ) NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
}
}
)";

    const auto [properties, brushes] = parse_entity(entityStr, solidMapping);

    REQUIRE(properties.size() == 10);

    REQUIRE(properties.at("classname") == "remec_multi_bhop_block");
    REQUIRE(properties.at("d//elay") == "{}0.100000");
    REQUIRE(properties.at("delay2") == "1.000000");
    REQUIRE(properties.at("{{zhlt_clipnodedetaillevel") == "1");
    REQUIRE(properties.at("zhlt_coplanarpriority") == "0");
    REQUIRE(properties.at("zhlt_chopup") == "0");
    REQUIRE(properties.at("zhlt_chopdown") == "0");
    REQUIRE(properties.at("zhlt_detaillevel") == "1");
    REQUIRE(properties.at("targetname") == "a");
    REQUIRE(properties.at("destination") == "b");

    REQUIRE(brushes.size() == 1);
    REQUIRE(brushes[0].size() == 6);

    REQUIRE(brushes[0][0].texture == "{{NULL");
    REQUIRE(brushes[0][1].texture == "}}NULL");
    REQUIRE(brushes[0][2].texture == "}}}}NULL");
    REQUIRE(brushes[0][3].texture == "{}{}NULL");
    REQUIRE(brushes[0][4].texture == "NULL");
    REQUIRE(brushes[0][5].texture == "NULL");
  }

  {
    const std::string entityStr = R"({
"classname" "remec_multi_bhop_block"
"d//elay" "{}0.100000"
"delay2" "1.000000"
"{{zhlt_clipnodedetaillevel" "1"
"zhlt_coplanarpriority" "0"
"zhlt_detaillevel" "1"
"targetname" "a"



"destination" "b"

{
( -32 -32 -160 ) ( -32 -32 -224 ) ( -32 32 -160 ) {{NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1


( 32 32 -160 ) ( 32 32 -224 ) ( 32 -32 -160 ) }}NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
( -32 32 -160 ) ( -32 32 -224 ) ( 32 32 -160 ) }}}}NULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 32 -32 -160 ) ( 32 -32 -224 ) ( -32 -32 -160 ) {}{}NULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 32 -32 -160 ) ( -32 -32 -160 ) ( 32 32 -160 ) NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( -32 32 -224 ) ( -32 -32 -224 ) ( 32 32 -224 ) NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1

}

}
)";

    const auto [properties, brushes] = parse_entity(entityStr, solidMapping);

    REQUIRE(properties.size() == 8);

    REQUIRE(properties.at("classname") == "remec_multi_bhop_block");
    REQUIRE(properties.at("d//elay") == "{}0.100000");
    REQUIRE(properties.at("delay2") == "1.000000");
    REQUIRE(properties.at("{{zhlt_clipnodedetaillevel") == "1");
    REQUIRE(properties.at("zhlt_coplanarpriority") == "0");
    REQUIRE(properties.at("zhlt_detaillevel") == "1");
    REQUIRE(properties.at("targetname") == "a");
    REQUIRE(properties.at("destination") == "b");

    REQUIRE(brushes.size() == 1);
    REQUIRE(brushes[0].size() == 6);

    REQUIRE(brushes[0][0].texture == "{{NULL");
    REQUIRE(brushes[0][1].texture == "}}NULL");
    REQUIRE(brushes[0][2].texture == "}}}}NULL");
    REQUIRE(brushes[0][3].texture == "{}{}NULL");
    REQUIRE(brushes[0][4].texture == "NULL");
    REQUIRE(brushes[0][5].texture == "NULL");
  }
}
