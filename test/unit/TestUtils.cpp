#include "Utils.hpp"

#include <common/CustomEntity.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("brushes_from_str")
{
  SECTION("6 face brush")
  {
    const std::string input = R"({
  ( -134 -41 -52 ) ( -134 -40 -52 ) ( -134 -41 -51 ) SOME_TEXTURE [ 0 -1 0 50 ] [ -1 2 1 4 ] 23.5 3 4
  ( -148 -28 -52 ) ( -148 -28 -51 ) ( -147 -28 -52 ) SOME_OTHER_TEXTURE [ 1 0 0 27 ] [ 1 -2 -1 -4 ] -15 -1 -2
  ( -153 -41 -26 ) ( -152 -41 -26 ) ( -153 -40 -26 ) SOME_TEXTURE [ -1 0 0 -32.307175 ] [ 0 -1 0 50 ] 0 1 1
  ( -93 44 -11 ) ( -93 45 -11 ) ( -92 44 -11 ) SOME_TEXTURE [ 1 0 0 27 ] [ 0 -1 0 50 ] 0 1 1
  ( -93 -17 -44 ) ( -92 -17 -44 ) ( -93 -17 -43 ) SOME_TEXTURE [ -1 0 0 -27 ] [ 0 0 -1 2.2004757 ] 0 1 1
  ( -120 44 -49 ) ( -120 44 -48 ) ( -120 45 -49 ) SOME_TEXTURE [ 0 1 0 -50 ] [ 0 0 -1 -2.8588524 ] 0 1 1
  })";

    const auto output = brushes_from_str(input);

    REQUIRE(output.size() == 1);
    REQUIRE(output[0].size() == 6);

    {
      // first face
      REQUIRE(output[0][0].point1.x == -134);
      REQUIRE(output[0][0].point1.y == -41);
      REQUIRE(output[0][0].point1.z == -52);

      REQUIRE(output[0][0].point2.x == -134);
      REQUIRE(output[0][0].point2.y == -40);
      REQUIRE(output[0][0].point2.z == -52);

      REQUIRE(output[0][0].point3.x == -134);
      REQUIRE(output[0][0].point3.y == -41);
      REQUIRE(output[0][0].point3.z == -51);

      REQUIRE(output[0][0].texture == "SOME_TEXTURE");

      REQUIRE(output[0][0].textureAxis1.x == 0);
      REQUIRE(output[0][0].textureAxis1.y == -1);
      REQUIRE(output[0][0].textureAxis1.z == 0);
      REQUIRE(output[0][0].textureAxis1Offset == 50);

      REQUIRE(output[0][0].textureAxis2.x == -1);
      REQUIRE(output[0][0].textureAxis2.y == 2);
      REQUIRE(output[0][0].textureAxis2.z == 1);
      REQUIRE(output[0][0].textureAxis2Offset == 4);

      REQUIRE(output[0][0].textureRotation == 23.5);
      REQUIRE(output[0][0].textureScaleX == 3);
      REQUIRE(output[0][0].textureScaleY == 4);
    }

    {
      // second face
      REQUIRE(output[0][1].point1.x == -148);
      REQUIRE(output[0][1].point1.y == -28);
      REQUIRE(output[0][1].point1.z == -52);

      REQUIRE(output[0][1].point2.x == -148);
      REQUIRE(output[0][1].point2.y == -28);
      REQUIRE(output[0][1].point2.z == -51);

      REQUIRE(output[0][1].point3.x == -147);
      REQUIRE(output[0][1].point3.y == -28);
      REQUIRE(output[0][1].point3.z == -52);

      REQUIRE(output[0][1].texture == "SOME_OTHER_TEXTURE");

      REQUIRE(output[0][1].textureAxis1.x == 1);
      REQUIRE(output[0][1].textureAxis1.y == 0);
      REQUIRE(output[0][1].textureAxis1.z == 0);
      REQUIRE(output[0][1].textureAxis1Offset == 27);

      REQUIRE(output[0][1].textureAxis2.x == 1);
      REQUIRE(output[0][1].textureAxis2.y == -2);
      REQUIRE(output[0][1].textureAxis2.z == -1);
      REQUIRE(output[0][1].textureAxis2Offset == -4);

      REQUIRE(output[0][1].textureRotation == -15);
      REQUIRE(output[0][1].textureScaleX == -1);
      REQUIRE(output[0][1].textureScaleY == -2);
    }

    {
      // third face
      REQUIRE(output[0][2].point1.x == -153);
      REQUIRE(output[0][2].point1.y == -41);
      REQUIRE(output[0][2].point1.z == -26);

      REQUIRE(output[0][2].point2.x == -152);
      REQUIRE(output[0][2].point2.y == -41);
      REQUIRE(output[0][2].point2.z == -26);

      REQUIRE(output[0][2].point3.x == -153);
      REQUIRE(output[0][2].point3.y == -40);
      REQUIRE(output[0][2].point3.z == -26);

      REQUIRE(output[0][2].texture == "SOME_TEXTURE");

      REQUIRE(output[0][2].textureAxis1.x == -1);
      REQUIRE(output[0][2].textureAxis1.y == 0);
      REQUIRE(output[0][2].textureAxis1.z == 0);
      REQUIRE(output[0][2].textureAxis1Offset == -32.307175);

      REQUIRE(output[0][2].textureAxis2.x == 0);
      REQUIRE(output[0][2].textureAxis2.y == -1);
      REQUIRE(output[0][2].textureAxis2.z == 0);
      REQUIRE(output[0][2].textureAxis2Offset == 50);

      REQUIRE(output[0][2].textureRotation == 0);
      REQUIRE(output[0][2].textureScaleX == 1);
      REQUIRE(output[0][2].textureScaleY == 1);
    }

    {
      // fourth face

      REQUIRE(output[0][3].point1.x == -93);
      REQUIRE(output[0][3].point1.y == 44);
      REQUIRE(output[0][3].point1.z == -11);

      REQUIRE(output[0][3].point2.x == -93);
      REQUIRE(output[0][3].point2.y == 45);
      REQUIRE(output[0][3].point2.z == -11);

      REQUIRE(output[0][3].point3.x == -92);
      REQUIRE(output[0][3].point3.y == 44);
      REQUIRE(output[0][3].point3.z == -11);

      REQUIRE(output[0][3].texture == "SOME_TEXTURE");

      REQUIRE(output[0][3].textureAxis1.x == 1);
      REQUIRE(output[0][3].textureAxis1.y == 0);
      REQUIRE(output[0][3].textureAxis1.z == 0);
      REQUIRE(output[0][3].textureAxis1Offset == 27);

      REQUIRE(output[0][3].textureAxis2.x == 0);
      REQUIRE(output[0][3].textureAxis2.y == -1);
      REQUIRE(output[0][3].textureAxis2.z == 0);
      REQUIRE(output[0][3].textureAxis2Offset == 50);

      REQUIRE(output[0][3].textureRotation == 0);
      REQUIRE(output[0][3].textureScaleX == 1);
      REQUIRE(output[0][3].textureScaleY == 1);
    }

    {
      // fifth face
      REQUIRE(output[0][4].point1.x == -93);
      REQUIRE(output[0][4].point1.y == -17);
      REQUIRE(output[0][4].point1.z == -44);

      REQUIRE(output[0][4].point2.x == -92);
      REQUIRE(output[0][4].point2.y == -17);
      REQUIRE(output[0][4].point2.z == -44);

      REQUIRE(output[0][4].point3.x == -93);
      REQUIRE(output[0][4].point3.y == -17);
      REQUIRE(output[0][4].point3.z == -43);

      REQUIRE(output[0][4].texture == "SOME_TEXTURE");

      REQUIRE(output[0][4].textureAxis1.x == -1);
      REQUIRE(output[0][4].textureAxis1.y == 0);
      REQUIRE(output[0][4].textureAxis1.z == 0);
      REQUIRE(output[0][4].textureAxis1Offset == -27);

      REQUIRE(output[0][4].textureAxis2.x == 0);
      REQUIRE(output[0][4].textureAxis2.y == 0);
      REQUIRE(output[0][4].textureAxis2.z == -1);
      REQUIRE(output[0][4].textureAxis2Offset == 2.2004757);

      REQUIRE(output[0][4].textureRotation == 0);
      REQUIRE(output[0][4].textureScaleX == 1);
      REQUIRE(output[0][4].textureScaleY == 1);
    }

    {
      // sixth face
      REQUIRE(output[0][5].point1.x == -120);
      REQUIRE(output[0][5].point1.y == 44);
      REQUIRE(output[0][5].point1.z == -49);

      REQUIRE(output[0][5].point2.x == -120);
      REQUIRE(output[0][5].point2.y == 44);
      REQUIRE(output[0][5].point2.z == -48);

      REQUIRE(output[0][5].point3.x == -120);
      REQUIRE(output[0][5].point3.y == 45);
      REQUIRE(output[0][5].point3.z == -49);

      REQUIRE(output[0][5].texture == "SOME_TEXTURE");

      REQUIRE(output[0][5].textureAxis1.x == 0);
      REQUIRE(output[0][5].textureAxis1.y == 1);
      REQUIRE(output[0][5].textureAxis1.z == 0);
      REQUIRE(output[0][5].textureAxis1Offset == -50);

      REQUIRE(output[0][5].textureAxis2.x == 0);
      REQUIRE(output[0][5].textureAxis2.y == 0);
      REQUIRE(output[0][5].textureAxis2.z == -1);
      REQUIRE(output[0][5].textureAxis2Offset == -2.8588524);

      REQUIRE(output[0][5].textureRotation == 0);
      REQUIRE(output[0][5].textureScaleX == 1);
      REQUIRE(output[0][5].textureScaleY == 1);
    }
  }

  SECTION("7 face brush")
  {
    const std::string input = R"({
  ( -134 -41 -52 ) ( -134 -40 -52 ) ( -134 -41 -51 ) SOME_TEXTURE [ 0 -1 0 50 ] [ -1 2 1 4 ] 23.5 3 4
  ( -148 -28 -52 ) ( -148 -28 -51 ) ( -147 -28 -52 ) SOME_OTHER_TEXTURE [ 1 0 0 27 ] [ 1 -2 -1 -4 ] -15 -1 -2
  ( -153 -41 -26 ) ( -152 -41 -26 ) ( -153 -40 -26 ) SOME_TEXTURE [ -1 0 0 -32.307175 ] [ 0 -1 0 50 ] 0 1 1
  ( -93 44 -11 ) ( -93 45 -11 ) ( -92 44 -11 ) SOME_TEXTURE [ 1 0 0 27 ] [ 0 -1 0 50 ] 0 1 1
  ( -93 -17 -44 ) ( -92 -17 -44 ) ( -93 -17 -43 ) SOME_TEXTURE [ -1 0 0 -27 ] [ 0 0 -1 2.2004757 ] 0 1 1
  ( -120 44 -49 ) ( -120 44 -48 ) ( -120 45 -49 ) SOME_TEXTURE [ 0 1 0 -50 ] [ 0 0 -1 -2.8588524 ] 0 1 1
  ( -123 46 -50 ) ( -121 43 -45 ) ( -125 44 -48 ) SOME_TEXTURE7 [ 0 1 0 -50 ] [ 0 0 -1 -2.8588528 ] 0 1 1
  })";

    const auto output = brushes_from_str(input);

    REQUIRE(output.size() == 1);
    REQUIRE(output[0].size() == 7);

    {
      // first face
      REQUIRE(output[0][0].point1.x == -134);
      REQUIRE(output[0][0].point1.y == -41);
      REQUIRE(output[0][0].point1.z == -52);

      REQUIRE(output[0][0].point2.x == -134);
      REQUIRE(output[0][0].point2.y == -40);
      REQUIRE(output[0][0].point2.z == -52);

      REQUIRE(output[0][0].point3.x == -134);
      REQUIRE(output[0][0].point3.y == -41);
      REQUIRE(output[0][0].point3.z == -51);

      REQUIRE(output[0][0].texture == "SOME_TEXTURE");

      REQUIRE(output[0][0].textureAxis1.x == 0);
      REQUIRE(output[0][0].textureAxis1.y == -1);
      REQUIRE(output[0][0].textureAxis1.z == 0);
      REQUIRE(output[0][0].textureAxis1Offset == 50);

      REQUIRE(output[0][0].textureAxis2.x == -1);
      REQUIRE(output[0][0].textureAxis2.y == 2);
      REQUIRE(output[0][0].textureAxis2.z == 1);
      REQUIRE(output[0][0].textureAxis2Offset == 4);

      REQUIRE(output[0][0].textureRotation == 23.5);
      REQUIRE(output[0][0].textureScaleX == 3);
      REQUIRE(output[0][0].textureScaleY == 4);
    }

    {
      // second face
      REQUIRE(output[0][1].point1.x == -148);
      REQUIRE(output[0][1].point1.y == -28);
      REQUIRE(output[0][1].point1.z == -52);

      REQUIRE(output[0][1].point2.x == -148);
      REQUIRE(output[0][1].point2.y == -28);
      REQUIRE(output[0][1].point2.z == -51);

      REQUIRE(output[0][1].point3.x == -147);
      REQUIRE(output[0][1].point3.y == -28);
      REQUIRE(output[0][1].point3.z == -52);

      REQUIRE(output[0][1].texture == "SOME_OTHER_TEXTURE");

      REQUIRE(output[0][1].textureAxis1.x == 1);
      REQUIRE(output[0][1].textureAxis1.y == 0);
      REQUIRE(output[0][1].textureAxis1.z == 0);
      REQUIRE(output[0][1].textureAxis1Offset == 27);

      REQUIRE(output[0][1].textureAxis2.x == 1);
      REQUIRE(output[0][1].textureAxis2.y == -2);
      REQUIRE(output[0][1].textureAxis2.z == -1);
      REQUIRE(output[0][1].textureAxis2Offset == -4);

      REQUIRE(output[0][1].textureRotation == -15);
      REQUIRE(output[0][1].textureScaleX == -1);
      REQUIRE(output[0][1].textureScaleY == -2);
    }

    {
      // third face
      REQUIRE(output[0][2].point1.x == -153);
      REQUIRE(output[0][2].point1.y == -41);
      REQUIRE(output[0][2].point1.z == -26);

      REQUIRE(output[0][2].point2.x == -152);
      REQUIRE(output[0][2].point2.y == -41);
      REQUIRE(output[0][2].point2.z == -26);

      REQUIRE(output[0][2].point3.x == -153);
      REQUIRE(output[0][2].point3.y == -40);
      REQUIRE(output[0][2].point3.z == -26);

      REQUIRE(output[0][2].texture == "SOME_TEXTURE");

      REQUIRE(output[0][2].textureAxis1.x == -1);
      REQUIRE(output[0][2].textureAxis1.y == 0);
      REQUIRE(output[0][2].textureAxis1.z == 0);
      REQUIRE(output[0][2].textureAxis1Offset == -32.307175);

      REQUIRE(output[0][2].textureAxis2.x == 0);
      REQUIRE(output[0][2].textureAxis2.y == -1);
      REQUIRE(output[0][2].textureAxis2.z == 0);
      REQUIRE(output[0][2].textureAxis2Offset == 50);

      REQUIRE(output[0][2].textureRotation == 0);
      REQUIRE(output[0][2].textureScaleX == 1);
      REQUIRE(output[0][2].textureScaleY == 1);
    }

    {
      // fourth face
      REQUIRE(output[0][3].point1.x == -93);
      REQUIRE(output[0][3].point1.y == 44);
      REQUIRE(output[0][3].point1.z == -11);

      REQUIRE(output[0][3].point2.x == -93);
      REQUIRE(output[0][3].point2.y == 45);
      REQUIRE(output[0][3].point2.z == -11);

      REQUIRE(output[0][3].point3.x == -92);
      REQUIRE(output[0][3].point3.y == 44);
      REQUIRE(output[0][3].point3.z == -11);

      REQUIRE(output[0][3].texture == "SOME_TEXTURE");

      REQUIRE(output[0][3].textureAxis1.x == 1);
      REQUIRE(output[0][3].textureAxis1.y == 0);
      REQUIRE(output[0][3].textureAxis1.z == 0);
      REQUIRE(output[0][3].textureAxis1Offset == 27);

      REQUIRE(output[0][3].textureAxis2.x == 0);
      REQUIRE(output[0][3].textureAxis2.y == -1);
      REQUIRE(output[0][3].textureAxis2.z == 0);
      REQUIRE(output[0][3].textureAxis2Offset == 50);

      REQUIRE(output[0][3].textureRotation == 0);
      REQUIRE(output[0][3].textureScaleX == 1);
      REQUIRE(output[0][3].textureScaleY == 1);
    }

    {
      // fifth face
      REQUIRE(output[0][4].point1.x == -93);
      REQUIRE(output[0][4].point1.y == -17);
      REQUIRE(output[0][4].point1.z == -44);

      REQUIRE(output[0][4].point2.x == -92);
      REQUIRE(output[0][4].point2.y == -17);
      REQUIRE(output[0][4].point2.z == -44);

      REQUIRE(output[0][4].point3.x == -93);
      REQUIRE(output[0][4].point3.y == -17);
      REQUIRE(output[0][4].point3.z == -43);

      REQUIRE(output[0][4].texture == "SOME_TEXTURE");

      REQUIRE(output[0][4].textureAxis1.x == -1);
      REQUIRE(output[0][4].textureAxis1.y == 0);
      REQUIRE(output[0][4].textureAxis1.z == 0);
      REQUIRE(output[0][4].textureAxis1Offset == -27);

      REQUIRE(output[0][4].textureAxis2.x == 0);
      REQUIRE(output[0][4].textureAxis2.y == 0);
      REQUIRE(output[0][4].textureAxis2.z == -1);
      REQUIRE(output[0][4].textureAxis2Offset == 2.2004757);

      REQUIRE(output[0][4].textureRotation == 0);
      REQUIRE(output[0][4].textureScaleX == 1);
      REQUIRE(output[0][4].textureScaleY == 1);
    }

    {
      // sixth face
      REQUIRE(output[0][5].point1.x == -120);
      REQUIRE(output[0][5].point1.y == 44);
      REQUIRE(output[0][5].point1.z == -49);

      REQUIRE(output[0][5].point2.x == -120);
      REQUIRE(output[0][5].point2.y == 44);
      REQUIRE(output[0][5].point2.z == -48);

      REQUIRE(output[0][5].point3.x == -120);
      REQUIRE(output[0][5].point3.y == 45);
      REQUIRE(output[0][5].point3.z == -49);

      REQUIRE(output[0][5].texture == "SOME_TEXTURE");

      REQUIRE(output[0][5].textureAxis1.x == 0);
      REQUIRE(output[0][5].textureAxis1.y == 1);
      REQUIRE(output[0][5].textureAxis1.z == 0);
      REQUIRE(output[0][5].textureAxis1Offset == -50);

      REQUIRE(output[0][5].textureAxis2.x == 0);
      REQUIRE(output[0][5].textureAxis2.y == 0);
      REQUIRE(output[0][5].textureAxis2.z == -1);
      REQUIRE(output[0][5].textureAxis2Offset == -2.8588524);

      REQUIRE(output[0][5].textureRotation == 0);
      REQUIRE(output[0][5].textureScaleX == 1);
      REQUIRE(output[0][5].textureScaleY == 1);
    }
  }

  SECTION("empty lines, textures with brackets")
  {
    const std::string input = R"({
  ( 32 32 32 ) ( 32 32 -32 ) ( 32 -32 32 ) {}NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1

  ( -32 -32 32 ) ( -32 -32 -32 ) ( -32 32 32 ) {}NULL}} [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( 32 -32 32 ) ( 32 -32 -32 ) ( -32 -32 32 ) N{{ULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 32 32 ) ( -32 32 -32 ) ( 32 32 32 ) N}}ULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 32 -32 ) ( -32 -32 -32 ) ( 32 32 -32 ) N{}}ULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1






  ( 32 -32 32 ) ( -32 -32 32 ) ( 32 32 32 ) }}NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  }
  {
  ( 32 32 32 ) ( 32 32 -32 ) ( 32 -32 32 ) {{NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 -32 32 ) ( -32 -32 -32 ) ( -32 32 32 ) }NULL [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( 32 -32 32 ) ( 32 -32 -32 ) ( -32 -32 32 ) N}ULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 32 32 ) ( -32 32 -32 ) ( 32 32 32 ) N{ULL [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 32 -32 ) ( -32 -32 -32 ) ( 32 32 -32 ) }{NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( 32 -32 32 ) ( -32 -32 32 ) ( 32 32 32 ) }NULL [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  })";

    const auto output = brushes_from_str(input);

    REQUIRE(output.size() == 2);
    REQUIRE(output[0].size() == 6);
    REQUIRE(output[1].size() == 6);

    {
      // first brush, first face
      REQUIRE(output[0][0].point1.x == 32);
      REQUIRE(output[0][0].point1.y == 32);
      REQUIRE(output[0][0].point1.z == 32);

      REQUIRE(output[0][0].point2.x == 32);
      REQUIRE(output[0][0].point2.y == 32);
      REQUIRE(output[0][0].point2.z == -32);

      REQUIRE(output[0][0].point3.x == 32);
      REQUIRE(output[0][0].point3.y == -32);
      REQUIRE(output[0][0].point3.z == 32);

      REQUIRE(output[0][0].texture == "{}NULL");

      REQUIRE(output[0][0].textureAxis1.x == 0);
      REQUIRE(output[0][0].textureAxis1.y == 1);
      REQUIRE(output[0][0].textureAxis1.z == 0);
      REQUIRE(output[0][0].textureAxis1Offset == 0);

      REQUIRE(output[0][0].textureAxis2.x == 0);
      REQUIRE(output[0][0].textureAxis2.y == 0);
      REQUIRE(output[0][0].textureAxis2.z == -1);
      REQUIRE(output[0][0].textureAxis2Offset == 0);

      REQUIRE(output[0][0].textureRotation == 0);
      REQUIRE(output[0][0].textureScaleX == 1);
      REQUIRE(output[0][0].textureScaleY == 1);
    }

    {
      // first brush, second face
      REQUIRE(output[0][1].point1.x == -32);
      REQUIRE(output[0][1].point1.y == -32);
      REQUIRE(output[0][1].point1.z == 32);

      REQUIRE(output[0][1].point2.x == -32);
      REQUIRE(output[0][1].point2.y == -32);
      REQUIRE(output[0][1].point2.z == -32);

      REQUIRE(output[0][1].point3.x == -32);
      REQUIRE(output[0][1].point3.y == 32);
      REQUIRE(output[0][1].point3.z == 32);

      REQUIRE(output[0][1].texture == "{}NULL}}");

      REQUIRE(output[0][1].textureAxis1.x == 0);
      REQUIRE(output[0][1].textureAxis1.y == 1);
      REQUIRE(output[0][1].textureAxis1.z == 0);
      REQUIRE(output[0][1].textureAxis1Offset == 0);

      REQUIRE(output[0][1].textureAxis2.x == 0);
      REQUIRE(output[0][1].textureAxis2.y == 0);
      REQUIRE(output[0][1].textureAxis2.z == -1);
      REQUIRE(output[0][1].textureAxis2Offset == 0);

      REQUIRE(output[0][1].textureRotation == 0);
      REQUIRE(output[0][1].textureScaleX == 1);
      REQUIRE(output[0][1].textureScaleY == 1);
    }

    {
      // first brush, third face
      REQUIRE(output[0][2].point1.x == 32);
      REQUIRE(output[0][2].point1.y == -32);
      REQUIRE(output[0][2].point1.z == 32);

      REQUIRE(output[0][2].point2.x == 32);
      REQUIRE(output[0][2].point2.y == -32);
      REQUIRE(output[0][2].point2.z == -32);

      REQUIRE(output[0][2].point3.x == -32);
      REQUIRE(output[0][2].point3.y == -32);
      REQUIRE(output[0][2].point3.z == 32);

      REQUIRE(output[0][2].texture == "N{{ULL");

      REQUIRE(output[0][2].textureAxis1.x == 1);
      REQUIRE(output[0][2].textureAxis1.y == 0);
      REQUIRE(output[0][2].textureAxis1.z == 0);
      REQUIRE(output[0][2].textureAxis1Offset == 0);

      REQUIRE(output[0][2].textureAxis2.x == 0);
      REQUIRE(output[0][2].textureAxis2.y == 0);
      REQUIRE(output[0][2].textureAxis2.z == -1);
      REQUIRE(output[0][2].textureAxis2Offset == 0);

      REQUIRE(output[0][2].textureRotation == 0);
      REQUIRE(output[0][2].textureScaleX == 1);
      REQUIRE(output[0][2].textureScaleY == 1);
    }

    {
      // first brush, fourth face
      REQUIRE(output[0][3].point1.x == -32);
      REQUIRE(output[0][3].point1.y == 32);
      REQUIRE(output[0][3].point1.z == 32);

      REQUIRE(output[0][3].point2.x == -32);
      REQUIRE(output[0][3].point2.y == 32);
      REQUIRE(output[0][3].point2.z == -32);

      REQUIRE(output[0][3].point3.x == 32);
      REQUIRE(output[0][3].point3.y == 32);
      REQUIRE(output[0][3].point3.z == 32);

      REQUIRE(output[0][3].texture == "N}}ULL");

      REQUIRE(output[0][3].textureAxis1.x == 1);
      REQUIRE(output[0][3].textureAxis1.y == 0);
      REQUIRE(output[0][3].textureAxis1.z == 0);
      REQUIRE(output[0][3].textureAxis1Offset == 0);

      REQUIRE(output[0][3].textureAxis2.x == 0);
      REQUIRE(output[0][3].textureAxis2.y == 0);
      REQUIRE(output[0][3].textureAxis2.z == -1);
      REQUIRE(output[0][3].textureAxis2Offset == 0);

      REQUIRE(output[0][3].textureRotation == 0);
      REQUIRE(output[0][3].textureScaleX == 1);
      REQUIRE(output[0][3].textureScaleY == 1);
    }

    {
      // first brush, fifth face
      REQUIRE(output[0][4].point1.x == -32);
      REQUIRE(output[0][4].point1.y == 32);
      REQUIRE(output[0][4].point1.z == -32);

      REQUIRE(output[0][4].point2.x == -32);
      REQUIRE(output[0][4].point2.y == -32);
      REQUIRE(output[0][4].point2.z == -32);

      REQUIRE(output[0][4].point3.x == 32);
      REQUIRE(output[0][4].point3.y == 32);
      REQUIRE(output[0][4].point3.z == -32);

      REQUIRE(output[0][4].texture == "N{}}ULL");

      REQUIRE(output[0][4].textureAxis1.x == 1);
      REQUIRE(output[0][4].textureAxis1.y == 0);
      REQUIRE(output[0][4].textureAxis1.z == 0);
      REQUIRE(output[0][4].textureAxis1Offset == 0);

      REQUIRE(output[0][4].textureAxis2.x == 0);
      REQUIRE(output[0][4].textureAxis2.y == -1);
      REQUIRE(output[0][4].textureAxis2.z == 0);
      REQUIRE(output[0][4].textureAxis2Offset == 0);

      REQUIRE(output[0][4].textureRotation == 0);
      REQUIRE(output[0][4].textureScaleX == 1);
      REQUIRE(output[0][4].textureScaleY == 1);
    }

    {
      // first brush, sixth face
      REQUIRE(output[0][5].point1.x == 32);
      REQUIRE(output[0][5].point1.y == -32);
      REQUIRE(output[0][5].point1.z == 32);

      REQUIRE(output[0][5].point2.x == -32);
      REQUIRE(output[0][5].point2.y == -32);
      REQUIRE(output[0][5].point2.z == 32);

      REQUIRE(output[0][5].point3.x == 32);
      REQUIRE(output[0][5].point3.y == 32);
      REQUIRE(output[0][5].point3.z == 32);

      REQUIRE(output[0][5].texture == "}}NULL");

      REQUIRE(output[0][5].textureAxis1.x == 1);
      REQUIRE(output[0][5].textureAxis1.y == 0);
      REQUIRE(output[0][5].textureAxis1.z == 0);
      REQUIRE(output[0][5].textureAxis1Offset == 0);

      REQUIRE(output[0][5].textureAxis2.x == 0);
      REQUIRE(output[0][5].textureAxis2.y == -1);
      REQUIRE(output[0][5].textureAxis2.z == 0);
      REQUIRE(output[0][5].textureAxis2Offset == 0);

      REQUIRE(output[0][5].textureRotation == 0);
      REQUIRE(output[0][5].textureScaleX == 1);
      REQUIRE(output[0][5].textureScaleY == 1);
    }

    {
      // second brush, first face
      REQUIRE(output[1][0].point1.x == 32);
      REQUIRE(output[1][0].point1.y == 32);
      REQUIRE(output[1][0].point1.z == 32);

      REQUIRE(output[1][0].point2.x == 32);
      REQUIRE(output[1][0].point2.y == 32);
      REQUIRE(output[1][0].point2.z == -32);

      REQUIRE(output[1][0].point3.x == 32);
      REQUIRE(output[1][0].point3.y == -32);
      REQUIRE(output[1][0].point3.z == 32);

      REQUIRE(output[1][0].texture == "{{NULL");

      REQUIRE(output[1][0].textureAxis1.x == 0);
      REQUIRE(output[1][0].textureAxis1.y == 1);
      REQUIRE(output[1][0].textureAxis1.z == 0);
      REQUIRE(output[1][0].textureAxis1Offset == 0);

      REQUIRE(output[1][0].textureAxis2.x == 0);
      REQUIRE(output[1][0].textureAxis2.y == 0);
      REQUIRE(output[1][0].textureAxis2.z == -1);
      REQUIRE(output[1][0].textureAxis2Offset == 0);

      REQUIRE(output[1][0].textureRotation == 0);
      REQUIRE(output[1][0].textureScaleX == 1);
      REQUIRE(output[1][0].textureScaleY == 1);
    }

    {
      // second brush, second face
      REQUIRE(output[1][1].point1.x == -32);
      REQUIRE(output[1][1].point1.y == -32);
      REQUIRE(output[1][1].point1.z == 32);

      REQUIRE(output[1][1].point2.x == -32);
      REQUIRE(output[1][1].point2.y == -32);
      REQUIRE(output[1][1].point2.z == -32);

      REQUIRE(output[1][1].point3.x == -32);
      REQUIRE(output[1][1].point3.y == 32);
      REQUIRE(output[1][1].point3.z == 32);

      REQUIRE(output[1][1].texture == "}NULL");

      REQUIRE(output[1][1].textureAxis1.x == 0);
      REQUIRE(output[1][1].textureAxis1.y == 1);
      REQUIRE(output[1][1].textureAxis1.z == 0);
      REQUIRE(output[1][1].textureAxis1Offset == 0);

      REQUIRE(output[1][1].textureAxis2.x == 0);
      REQUIRE(output[1][1].textureAxis2.y == 0);
      REQUIRE(output[1][1].textureAxis2.z == -1);
      REQUIRE(output[1][1].textureAxis2Offset == 0);

      REQUIRE(output[1][1].textureRotation == 0);
      REQUIRE(output[1][1].textureScaleX == 1);
      REQUIRE(output[1][1].textureScaleY == 1);
    }

    {
      // second brush, third face
      REQUIRE(output[1][2].point1.x == 32);
      REQUIRE(output[1][2].point1.y == -32);
      REQUIRE(output[1][2].point1.z == 32);

      REQUIRE(output[1][2].point2.x == 32);
      REQUIRE(output[1][2].point2.y == -32);
      REQUIRE(output[1][2].point2.z == -32);

      REQUIRE(output[1][2].point3.x == -32);
      REQUIRE(output[1][2].point3.y == -32);
      REQUIRE(output[1][2].point3.z == 32);

      REQUIRE(output[1][2].texture == "N}ULL");

      REQUIRE(output[1][2].textureAxis1.x == 1);
      REQUIRE(output[1][2].textureAxis1.y == 0);
      REQUIRE(output[1][2].textureAxis1.z == 0);
      REQUIRE(output[1][2].textureAxis1Offset == 0);

      REQUIRE(output[1][2].textureAxis2.x == 0);
      REQUIRE(output[1][2].textureAxis2.y == 0);
      REQUIRE(output[1][2].textureAxis2.z == -1);
      REQUIRE(output[1][2].textureAxis2Offset == 0);

      REQUIRE(output[1][2].textureRotation == 0);
      REQUIRE(output[1][2].textureScaleX == 1);
      REQUIRE(output[1][2].textureScaleY == 1);
    }

    {
      // second brush, fourth face
      REQUIRE(output[1][3].point1.x == -32);
      REQUIRE(output[1][3].point1.y == 32);
      REQUIRE(output[1][3].point1.z == 32);

      REQUIRE(output[1][3].point2.x == -32);
      REQUIRE(output[1][3].point2.y == 32);
      REQUIRE(output[1][3].point2.z == -32);

      REQUIRE(output[1][3].point3.x == 32);
      REQUIRE(output[1][3].point3.y == 32);
      REQUIRE(output[1][3].point3.z == 32);

      REQUIRE(output[1][3].texture == "N{ULL");

      REQUIRE(output[1][3].textureAxis1.x == 1);
      REQUIRE(output[1][3].textureAxis1.y == 0);
      REQUIRE(output[1][3].textureAxis1.z == 0);
      REQUIRE(output[1][3].textureAxis1Offset == 0);

      REQUIRE(output[1][3].textureAxis2.x == 0);
      REQUIRE(output[1][3].textureAxis2.y == 0);
      REQUIRE(output[1][3].textureAxis2.z == -1);
      REQUIRE(output[1][3].textureAxis2Offset == 0);

      REQUIRE(output[1][3].textureRotation == 0);
      REQUIRE(output[1][3].textureScaleX == 1);
      REQUIRE(output[1][3].textureScaleY == 1);
    }

    {
      // second brush, fifth face
      REQUIRE(output[1][4].point1.x == -32);
      REQUIRE(output[1][4].point1.y == 32);
      REQUIRE(output[1][4].point1.z == -32);

      REQUIRE(output[1][4].point2.x == -32);
      REQUIRE(output[1][4].point2.y == -32);
      REQUIRE(output[1][4].point2.z == -32);

      REQUIRE(output[1][4].point3.x == 32);
      REQUIRE(output[1][4].point3.y == 32);
      REQUIRE(output[1][4].point3.z == -32);

      REQUIRE(output[1][4].texture == "}{NULL");

      REQUIRE(output[1][4].textureAxis1.x == 1);
      REQUIRE(output[1][4].textureAxis1.y == 0);
      REQUIRE(output[1][4].textureAxis1.z == 0);
      REQUIRE(output[1][4].textureAxis1Offset == 0);

      REQUIRE(output[1][4].textureAxis2.x == 0);
      REQUIRE(output[1][4].textureAxis2.y == -1);
      REQUIRE(output[1][4].textureAxis2.z == 0);
      REQUIRE(output[1][4].textureAxis2Offset == 0);

      REQUIRE(output[1][4].textureRotation == 0);
      REQUIRE(output[1][4].textureScaleX == 1);
      REQUIRE(output[1][4].textureScaleY == 1);
    }

    {
      // second brush, sixth face
      REQUIRE(output[1][5].point1.x == 32);
      REQUIRE(output[1][5].point1.y == -32);
      REQUIRE(output[1][5].point1.z == 32);

      REQUIRE(output[1][5].point2.x == -32);
      REQUIRE(output[1][5].point2.y == -32);
      REQUIRE(output[1][5].point2.z == 32);

      REQUIRE(output[1][5].point3.x == 32);
      REQUIRE(output[1][5].point3.y == 32);
      REQUIRE(output[1][5].point3.z == 32);

      REQUIRE(output[1][5].texture == "}NULL");

      REQUIRE(output[1][5].textureAxis1.x == 1);
      REQUIRE(output[1][5].textureAxis1.y == 0);
      REQUIRE(output[1][5].textureAxis1.z == 0);
      REQUIRE(output[1][5].textureAxis1Offset == 0);

      REQUIRE(output[1][5].textureAxis2.x == 0);
      REQUIRE(output[1][5].textureAxis2.y == -1);
      REQUIRE(output[1][5].textureAxis2.z == 0);
      REQUIRE(output[1][5].textureAxis2Offset == 0);

      REQUIRE(output[1][5].textureRotation == 0);
      REQUIRE(output[1][5].textureScaleX == 1);
      REQUIRE(output[1][5].textureScaleY == 1);
    }
  }
}

TEST_CASE("replace_brushes_textures")
{
  const auto inputBrushes = brushes_from_str(R"({
( -134 -41 -52 ) ( -134 -40 -52 ) ( -134 -41 -51 ) ORIGINAL_TEXTURE [ 0 -1 0 50 ] [ 0 0 -1 -4 ] 0 1 1
( -148 -28 -52 ) ( -148 -28 -51 ) ( -147 -28 -52 ) ORIGINAL_TEXTURE [ 1 0 0 27 ] [ 0 0 -1 -4 ] 0 1 1
( -153 -41 -26 ) ( -152 -41 -26 ) ( -153 -40 -26 ) ORIGINAL_TEXTURE [ -1 0 0 -32.307175 ] [ 0 -1 0 50 ] 0 1 1
( -93 44 -11 ) ( -93 45 -11 ) ( -92 44 -11 ) ORIGINAL_TEXTURE [ 1 0 0 27 ] [ 0 -1 0 50 ] 0 1 1
( -93 -17 -44 ) ( -92 -17 -44 ) ( -93 -17 -43 ) ORIGINAL_TEXTURE [ -1 0 0 -27 ] [ 0 0 -1 2.2004757 ] 0 1 1
( -120 44 -49 ) ( -120 44 -48 ) ( -120 45 -49 ) ORIGINAL_TEXTURE [ 0 1 0 -50 ] [ 0 0 -1 -2.8588524 ] 0 1 1
})");

  const auto expectedBrushes = brushes_from_str(R"({
( -134 -41 -52 ) ( -134 -40 -52 ) ( -134 -41 -51 ) REPLACEMENT_TEXTURE [ 0 -1 0 50 ] [ 0 0 -1 -4 ] 0 1 1
( -148 -28 -52 ) ( -148 -28 -51 ) ( -147 -28 -52 ) REPLACEMENT_TEXTURE [ 1 0 0 27 ] [ 0 0 -1 -4 ] 0 1 1
( -153 -41 -26 ) ( -152 -41 -26 ) ( -153 -40 -26 ) REPLACEMENT_TEXTURE [ -1 0 0 -32.307175 ] [ 0 -1 0 50 ] 0 1 1
( -93 44 -11 ) ( -93 45 -11 ) ( -92 44 -11 ) REPLACEMENT_TEXTURE [ 1 0 0 27 ] [ 0 -1 0 50 ] 0 1 1
( -93 -17 -44 ) ( -92 -17 -44 ) ( -93 -17 -43 ) REPLACEMENT_TEXTURE [ -1 0 0 -27 ] [ 0 0 -1 2.2004757 ] 0 1 1
( -120 44 -49 ) ( -120 44 -48 ) ( -120 45 -49 ) REPLACEMENT_TEXTURE [ 0 1 0 -50 ] [ 0 0 -1 -2.8588524 ] 0 1 1
})");

  const auto output =
      replace_brushes_textures(inputBrushes, "REPLACEMENT_TEXTURE");

  REQUIRE(brushes_to_str(output) == brushes_to_str(expectedBrushes));
}

TEST_CASE("get_origin")
{
  {
    const auto brushes = brushes_from_str(R"({
  ( 32 32 32 ) ( 32 32 -32 ) ( 32 -32 32 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 -32 32 ) ( -32 -32 -32 ) ( -32 32 32 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( 32 -32 32 ) ( 32 -32 -32 ) ( -32 -32 32 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 32 32 ) ( -32 32 -32 ) ( 32 32 32 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 32 -32 ) ( -32 -32 -32 ) ( 32 32 -32 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( 32 -32 32 ) ( -32 -32 32 ) ( 32 32 32 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  })");

    const Point3D expected = {0, 0, 0};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( -48 16 22.6274 ) ( -38.6274 -38.6274 -9.37258 ) ( -16 48 -22.6274 ) TEXTURE_NAME [ 0.5 0.5 -0.707107 0 ] [ 0.146447 -0.853553 -0.5 0 ] 0 1 1
  ( 48 -16 -22.6274 ) ( 16 -48 22.6274 ) ( 38.6274 38.6274 9.37258 ) TEXTURE_NAME [ 0.5 0.5 -0.707107 0 ] [ 0.146447 -0.853553 -0.5 0 ] 0 1 1
  ( 48 -16 -22.6274 ) ( -6.62742 -6.62742 -54.6274 ) ( 16 -48 22.6274 ) TEXTURE_NAME [ 0.5 0.5 -0.707107 0 ] [ -0.853553 0.146447 -0.5 0 ] 0 1 1
  ( 6.62742 6.62742 54.6274 ) ( -48 16 22.6274 ) ( 38.6274 38.6274 9.37258 ) TEXTURE_NAME [ 0.5 0.5 -0.707107 0 ] [ -0.853553 0.146447 -0.5 0 ] 0 1 1
  ( 38.6274 38.6274 9.37258 ) ( -16 48 -22.6274 ) ( 48 -16 -22.6274 ) TEXTURE_NAME [ -0.146447 0.853553 0.5 0 ] [ -0.853553 0.146447 -0.5 0 ] 0 1 1
  ( 16 -48 22.6274 ) ( -38.6274 -38.6274 -9.37258 ) ( 6.62742 6.62742 54.6274 ) TEXTURE_NAME [ -0.146447 0.853553 0.5 0 ] [ -0.853553 0.146447 -0.5 0 ] 0 1 1
  })");

    const Point3D expected = {0.000001, -0.000010, -0.000002};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( -128 -32 -32 ) ( -128 -96 -32 ) ( -128 -32 -64 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( 0 -96 -32 ) ( 0 -32 -32 ) ( 0 -96 -64 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -96 0 -64 ) ( -32 0 -64 ) ( -96 0 -32 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 -128 -64 ) ( -96 -128 -64 ) ( -32 -128 -32 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 -32 -96 ) ( -96 -32 -96 ) ( -32 -96 -96 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -32 -96 0 ) ( -96 -96 0 ) ( -32 -32 0 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -96 -128 -32 ) ( -96 -128 -64 ) ( -112 -112 -16 ) TEXTURE_NAME [ 0.707107 -0.707107 0 0 ] [ 0 -0 -1 0 ] 0 1 1
  ( -128 -96 -32 ) ( -128 -32 -32 ) ( -112 -112 -16 ) TEXTURE_NAME [ -0 -1 -0 0 ] [ -0.707107 0 -0.707107 0 ] 0 1 1
  ( -128 -32 -64 ) ( -128 -96 -64 ) ( -112 -16 -80 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -32 -128 -64 ) ( -16 -112 -80 ) ( -96 -128 -64 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0.707107 -0.707107 0 ] 0 1 1
  ( -96 -96 0 ) ( -32 -96 0 ) ( -112 -112 -16 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -0.707107 -0.707107 0 ] 0 1 1
  ( -32 0 -32 ) ( -32 0 -64 ) ( -16 -16 -16 ) TEXTURE_NAME [ -0.707107 0.707107 0 0 ] [ -0 0 -1 0 ] 0 1 1
  ( -32 -32 0 ) ( -96 -32 0 ) ( -16 -16 -16 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -96 0 -64 ) ( -112 -16 -80 ) ( -32 0 -64 ) TEXTURE_NAME [ -1 -0 -0 0 ] [ 0 -0.707107 -0.707107 0 ] 0 1 1
  ( -128 -32 -32 ) ( -128 -32 -64 ) ( -112 -16 -16 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( 0 -96 -64 ) ( 0 -32 -64 ) ( -16 -112 -80 ) TEXTURE_NAME [ 0 1 0 0 ] [ -0.707107 0 -0.707107 0 ] 0 1 1
  ( 0 -32 -32 ) ( 0 -96 -32 ) ( -16 -16 -16 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( 0 -96 -32 ) ( 0 -96 -64 ) ( -16 -112 -16 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  })");

    const Point3D expected = {-64, -64, -48};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( -423.833 -322.439 -476.494 ) ( -425.585 -340.231 -492.012 ) ( -340.384 -265.103 -418.225 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -512 -390.043 -338.933 ) ( -506.505 -383.924 -345.392 ) ( -442.877 -401.784 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -477.522 -427.233 -463.328 ) ( -512 -390.043 -338.933 ) ( -403.841 -463.742 -501.854 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -506.505 -383.924 -345.392 ) ( -512 -390.043 -338.933 ) ( -423.833 -322.439 -476.494 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -386.532 -391.48 -512 ) ( -403.841 -463.742 -501.854 ) ( -349.499 -366.085 -506.216 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -278.701 -353.176 -487.576 ) ( -256 -512 -446.927 ) ( -308.697 -256 -407.017 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -425.585 -340.231 -492.012 ) ( -477.522 -427.233 -463.328 ) ( -425.323 -340.422 -492.149 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -310.944 -281.315 -362.152 ) ( -280.548 -479.258 -325.254 ) ( -442.877 -401.784 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -506.505 -383.924 -345.392 ) ( -423.833 -322.439 -476.494 ) ( -340.384 -265.103 -418.225 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -386.532 -391.479 -512 ) ( -349.499 -366.085 -506.216 ) ( -425.322 -340.422 -492.149 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  })");

    const Point3D expected = {-383.999933, -383.997694, -384.017049};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( -512 -512 -256 ) ( -512 -512 -512 ) ( -512 -256 -256 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -511 -256 -256 ) ( -511 -256 -512 ) ( -511 -512 -256 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -256 -256 ) ( -512 -256 -512 ) ( -511 -256 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -511 -512 -256 ) ( -511 -512 -512 ) ( -512 -512 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -511 -512 -256 ) ( -512 -512 -256 ) ( -511 -256 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -512 -256 -512 ) ( -512 -512 -512 ) ( -511 -256 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  })");

    const Point3D expected = {-511.500000, -384.000000, -384.000000};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( -511 -256 -320 ) ( -511 -256 -384 ) ( -511 -332.8 -268.8 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -512 -320 ) ( -512 -512 -384 ) ( -512 -464 -272 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -511 -512 -320 ) ( -511 -512 -384 ) ( -512 -512 -320 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -256 -320 ) ( -512 -256 -384 ) ( -511 -256 -320 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -416 -256 ) ( -512 -384 -256 ) ( -511 -416 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -512 -256 -384 ) ( -512 -294.4 -460.8 ) ( -511 -256 -384 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -406.857 -498.286 ) ( -512 -496.941 -444.236 ) ( -511 -406.857 -498.286 ) TEXTURE_NAME [ -1 -0 0 0 ] [ 0 -0.857493 0.514496 0 ] 0 1 1
  ( -512 -384 -256 ) ( -512 -332.8 -268.8 ) ( -511 -384 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -0.970143 0.242536 0 ] 0 1 1
  ( -511 -464 -272 ) ( -511 -512 -320 ) ( -512 -464 -272 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -464 -272 ) ( -512 -416 -256 ) ( -511 -464 -272 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -512 -332.8 -268.8 ) ( -512 -256 -320 ) ( -511 -332.8 -268.8 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -0.83205 0.5547 0 ] 0 1 1
  ( -511 -512 -384 ) ( -511 -496.941 -444.235 ) ( -512 -512 -384 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0.242536 -0.970143 0 ] 0 1 1
  ( -512 -294.4 -460.8 ) ( -512 -406.857 -498.286 ) ( -511 -294.4 -460.8 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  })");

    const Point3D expected = {-511.500000, -384.000000, -377.143000};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( -1024 -949 -331 ) ( -1024 -926 -354 ) ( -1024 -768 -256 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -256 ) ( -1024 -768 -288 ) ( -512 -768 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -288 ) ( -1024 -926 -354 ) ( -512 -768 -288 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -949 -331 ) ( -1024 -768 -256 ) ( -512 -949 -331 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -926 -354 ) ( -1024 -949 -331 ) ( -512 -926 -354 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -768 -256 ) ( -512 -768 -288 ) ( -512 -949 -331 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  }
  {
  ( -512 -949 -331 ) ( -512 -926 -354 ) ( -512 -1024 -512 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -992 -512 ) ( -1024 -1024 -512 ) ( -512 -992 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -926 -354 ) ( -1024 -992 -512 ) ( -512 -926 -354 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -1024 -512 ) ( -1024 -949 -331 ) ( -512 -1024 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -949 -331 ) ( -1024 -926 -354 ) ( -512 -949 -331 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -1024 -512 ) ( -1024 -992 -512 ) ( -1024 -949 -331 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  }
  {
  ( -1024 -949 -693 ) ( -1024 -926 -670 ) ( -1024 -1024 -512 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -1024 -512 ) ( -1024 -992 -512 ) ( -512 -1024 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -992 -512 ) ( -1024 -926 -670 ) ( -512 -992 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -949 -693 ) ( -1024 -1024 -512 ) ( -512 -949 -693 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -926 -670 ) ( -1024 -949 -693 ) ( -512 -926 -670 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -1024 -512 ) ( -512 -992 -512 ) ( -512 -949 -693 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  }
  {
  ( -512 -949 -693 ) ( -512 -926 -670 ) ( -512 -768 -768 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -736 ) ( -1024 -768 -768 ) ( -512 -768 -736 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -926 -670 ) ( -1024 -768 -736 ) ( -512 -926 -670 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -768 -768 ) ( -1024 -949 -693 ) ( -512 -768 -768 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -949 -693 ) ( -1024 -926 -670 ) ( -512 -949 -693 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -768 ) ( -1024 -768 -736 ) ( -1024 -949 -693 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  }
  {
  ( -1024 -587 -693 ) ( -1024 -610 -670 ) ( -1024 -768 -768 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -768 ) ( -1024 -768 -736 ) ( -512 -768 -768 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -736 ) ( -1024 -610 -670 ) ( -512 -768 -736 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -587 -693 ) ( -1024 -768 -768 ) ( -512 -587 -693 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -610 -670 ) ( -1024 -587 -693 ) ( -512 -610 -670 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -768 -768 ) ( -512 -768 -736 ) ( -512 -587 -693 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  }
  {
  ( -512 -587 -693 ) ( -512 -610 -670 ) ( -512 -512 -512 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -544 -512 ) ( -1024 -512 -512 ) ( -512 -544 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -610 -670 ) ( -1024 -544 -512 ) ( -512 -610 -670 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -512 -512 ) ( -1024 -587 -693 ) ( -512 -512 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -587 -693 ) ( -1024 -610 -670 ) ( -512 -587 -693 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -512 -512 ) ( -1024 -544 -512 ) ( -1024 -587 -693 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  }
  {
  ( -1024 -587 -331 ) ( -1024 -610 -354 ) ( -1024 -512 -512 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -512 -512 ) ( -1024 -544 -512 ) ( -512 -512 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -544 -512 ) ( -1024 -610 -354 ) ( -512 -544 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -587 -331 ) ( -1024 -512 -512 ) ( -512 -587 -331 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -610 -354 ) ( -1024 -587 -331 ) ( -512 -610 -354 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -512 -512 -512 ) ( -512 -544 -512 ) ( -512 -587 -331 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  }
  {
  ( -512 -587 -331 ) ( -512 -610 -354 ) ( -512 -768 -256 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -288 ) ( -1024 -768 -256 ) ( -512 -768 -288 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -610 -354 ) ( -1024 -768 -288 ) ( -512 -610 -354 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -768 -256 ) ( -1024 -587 -331 ) ( -512 -768 -256 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -1024 -587 -331 ) ( -1024 -610 -354 ) ( -512 -587 -331 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -1024 -768 -256 ) ( -1024 -768 -288 ) ( -1024 -587 -331 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  })");

    const Point3D expected = {-768.000000, -858.500000, -305.000000};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( 4096 4096 4096 ) ( 4096 4096 -4096 ) ( 4096 -4096 4096 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -4096 -4096 4096 ) ( -4096 -4096 -4096 ) ( -4096 4096 4096 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( 4096 -4096 4096 ) ( 4096 -4096 -4096 ) ( -4096 -4096 4096 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -4096 4096 4096 ) ( -4096 4096 -4096 ) ( 4096 4096 4096 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -4096 4096 -4096 ) ( -4096 -4096 -4096 ) ( 4096 4096 -4096 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( 4096 -4096 4096 ) ( -4096 -4096 4096 ) ( 4096 4096 4096 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  })");

    const Point3D expected = {0, 0, 0};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }

  {
    const auto brushes = brushes_from_str(R"({
  ( -512 -512 -512 ) ( -512 -512 -4096 ) ( -512 -4096 -512 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -4096 -4096 -512 ) ( -4096 -4096 -4096 ) ( -4096 -512 -512 ) TEXTURE_NAME [ 0 1 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -4096 -4096 -512 ) ( -512 -4096 -512 ) ( -4096 -4096 -4096 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -4096 -512 -4096 ) ( -512 -512 -4096 ) ( -4096 -512 -512 ) TEXTURE_NAME [ -1 -0 -0 0 ] [ 0 0 -1 0 ] 0 1 1
  ( -4096 -4096 -512 ) ( -4096 -512 -512 ) ( -512 -4096 -512 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  ( -4096 -512 -4096 ) ( -4096 -4096 -4096 ) ( -512 -512 -4096 ) TEXTURE_NAME [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
  })");

    const Point3D expected = {-2304.000000, -2304.000000, -2304.000000};
    const auto output      = origin_from_str(get_origin(brushes));

    REQUIRE(output == expected);
  }
}

TEST_CASE("origin_from_str")
{
  {
    const auto result = origin_from_str("-148 -132 -33");

    REQUIRE(result.x == -148);
    REQUIRE(result.y == -132);
    REQUIRE(result.z == -33);
  }

  {
    const auto result = origin_from_str("148 132 33");

    REQUIRE(result.x == 148);
    REQUIRE(result.y == 132);
    REQUIRE(result.z == 33);
  }

  {
    const auto result = origin_from_str("148 -132 33");

    REQUIRE(result.x == 148);
    REQUIRE(result.y == -132);
    REQUIRE(result.z == 33);
  }
}

TEST_CASE("properties_map_to_str")
{
  const PropertiesMap props {
      {"targetname", "name"},
      {"classname", "classname"},
      {"prop1", "prop1value"},
      {"prop2", "prop2value"},
  };

  const auto expected = R"(
"prop1" "prop1value"
"prop2" "prop2value"
"classname" "classname"
"targetname" "name")";

  const auto output = properties_map_to_str(props);

  REQUIRE(output == expected);
}

TEST_CASE("ends_with")
{
  REQUIRE(ends_with("string", "loooooongstring") == false);
  REQUIRE(ends_with("string", "stri") == false);
  REQUIRE(ends_with("string", "rin") == false);
  REQUIRE(ends_with("string", "g"));
  REQUIRE(ends_with("string", "ng"));
  REQUIRE(ends_with("string", "ing"));
  REQUIRE(ends_with("string", "string"));
}

TEST_CASE("get_sky_name")
{
  SECTION("no key")
  {
    const std::string key;
    REQUIRE(get_sky_name(key).empty());
  }

  SECTION("has key but no sky specified")
  {
    const std::string key = R"("skyname" "")";

    REQUIRE(get_sky_name(key).empty());
  }

  SECTION("has key with sky specified")
  {
    const std::string key = R"("skyname" "nebula")";

    REQUIRE(get_sky_name(key) == "nebula");
  }
}
