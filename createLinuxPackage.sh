#!/bin/bash

pushd linux_build

tar -czvf remec_linux_x64.tar.gz remec *.so
mv remec_linux_x64.tar.gz ..

popd
