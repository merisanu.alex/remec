# DelayedTeleport

Teleport target to destination after a delay

## Properties

| Property     | Default value | Description              | Possible values                                                 |
| ------------ | ------------- | ------------------------ | --------------------------------------------------------------- |
| `targetname` |               | Name                     |                                                                 |
| `delay`      | `0.0`         | Delay before trigger     |                                                                 |
| `target`     |               | Where to teleport to     |                                                                 |
| `wait`       | `10`          | Delay before reset       |                                                                 |
| `killtarget` |               | Kill target              |                                                                 |
| `netname`    |               | Target path              |                                                                 |
| `master`     |               | Master                   |                                                                 |
| `sounds`     |               | Sound style              | <ul><li>No Sound</li></ul>                                      |
| `message`    |               | Message (set sound too!) |                                                                 |
| `spawnflags` | `0`           | Spawn flags              | <ul><li>Monsters</li><li>No Clients</li><li>Pushables</li></ul> |

## Generates

```mermaid
graph TD;

TGT["target"]

subgraph remec_delayed_teleport
  TM["trigger_multiple"]
  style TM stroke:violet,color:violet;
  MM["multi_manager"]
  style MM stroke:orange,color:orange;
  DR["func_door"]
  style DR stroke:lightgreen,color:lightgreen;
  MS["multisource"]
  style MS stroke:gold,color:gold;
  TT["trigger_teleport"]
  style TT stroke:lime,color:lime;

  TM --> MM
  linkStyle 0 fill:none,stroke:violet;
  MM --delay--> DR
  linkStyle 1 fill:none,stroke:orange;
  DR --> MS
  linkStyle 2 fill:none,stroke:lightgreen;

  MS -.master of.-> TT
  linkStyle 3 fill:none,stroke:gold;
end

TT --> TGT
linkStyle 4 fill:none,stroke:lime;

```
