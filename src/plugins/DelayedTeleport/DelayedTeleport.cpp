#include "DelayedTeleport.hpp"

#include <Utils.hpp>
#include <fmt/format.h>

tl::expected<std::string, std::string> DelayedTeleport::serialize() const
{
  try {
    const auto triggerProperties = getFilteredProperties();
    const auto brushes           = getBrushes();

    return fmt::format(
        R"(
// trigger_multiple triggers {original_name}_mm instantly
{{
{layer}"classname" "trigger_multiple"
"targetname" "{original_name}"
"target" "{original_name}_mm"{trigger_properties}
{brushes}
}}

// manager triggers {original_name}_door after {delay:.{decimals_count}f} seconds
{{
{layer}"classname" "multi_manager"
"origin" "{origin}"
"targetname" "{original_name}_mm"
"spawnflags" "0"
"{original_name}_door" "{delay:.{decimals_count}f}"
}}

// door triggers {original_name}_ms instantly enabling it and disables it after 1 second
{{
{layer}"classname" "func_door"
"targetname" "{original_name}_door"
"wait" "1"
"delay" "0"
"lip" "0"
"speed" "0"
"spawnflags" "9"
"target" "{original_name}_ms"
"angles" "90 0 0"
{brushes}
}}

// multisource is the master of trigger_teleport
{{
{layer}"classname" "multisource"
"origin" "{origin}"
"targetname" "{original_name}_ms"
}}

// when {original_name}_door is "open" the teleport is enabled
{{
{layer}"classname" "trigger_teleport"
"target" "{target}"
"spawnflags" "0"
"master" "{original_name}_ms"
{brushes}
}}
)",
        fmt::arg("original_name", getName()),
        fmt::arg("layer", getLayer()),
        fmt::arg("decimals_count", DecimalsCount),
        fmt::arg("brushes", brushes_to_str(replace_brushes_textures(brushes))),
        fmt::arg("delay", getOrDefault<double>("delay")),
        fmt::arg("origin", get_origin(brushes)),
        fmt::arg("target", getOrDefault("target")),
        fmt::arg("trigger_properties",
                 properties_map_to_str(triggerProperties)));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &DelayedTeleport::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {
          "delay", 0.0, "Delay before trigger", OwnProperty::Type::String, {}},
      OwnProperty {"target",
                   "",
                   "Target to teleport to",
                   OwnProperty::Type::TargetDestination,
                   {}},
      OwnProperty {
          "wait", 10, "Delay before reset", OwnProperty::Type::Integer, {}},
      OwnProperty {"killtarget",
                   "",
                   "Kill target",
                   OwnProperty::Type::TargetDestination,
                   {}},
      OwnProperty {"netname",
                   "",
                   "Target Path",
                   OwnProperty::Type::TargetDestination,
                   {}},
      OwnProperty {
          "master", "", "master", OwnProperty::Type::TargetDestination, {}},
      OwnProperty {
          "sounds",
          0,
          "Sound style",
          OwnProperty::Type::Choices,
          {
              {0, "No Sound"},
          },
      },
      OwnProperty {"message",
                   "",
                   "Message (set sound too!)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"spawnflags",
                   0,
                   "Spawnflags",
                   OwnProperty::Type::Flags,
                   {
                       {1, "Monsters", 0},
                       {2, "No Clients", 0},
                       {4, "Pushables", 0},
                   }},
  };

  return props;
}

REGISTER_PLUGIN(DelayedTeleport)
