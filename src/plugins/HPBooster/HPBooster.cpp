#include "HPBooster.hpp"

#include <Utils.hpp>
#include <fmt/format.h>
#include <functional>

tl::expected<std::string, std::string> HPBooster::serialize() const
{
  try {
    using namespace std::string_literals;

    const auto brushes = getBrushes();

    const auto &originalBrush = brushes[0];

    std::function<Brush(void)> bottomBlockBrushFunc;
    std::function<Brush(void)> topBlockBrushFunc;

    // booster must be at least 120 units tall in order for it to work on lan without plugins
    constexpr auto MaxHeight = 120;

    if (getContext().tbOrJack == "tb") {
      const auto bottomBlockHeight = getOrDefault<double>("bottomheight");
      const auto topBlockheight    = getOrDefault<double>("topheight");

      const auto newBottomTopZPos =
          originalBrush[0].point2.z + bottomBlockHeight;
      const auto newTopBottomZPos = originalBrush[3].point1.z - topBlockheight;

      const auto myHeight = std::abs(newTopBottomZPos - newBottomTopZPos);

      if (myHeight > MaxHeight) {
        throw std::runtime_error {fmt::format(
            "{} must be a maximum of {} units tall (current height is {})",
            ClassName,
            MaxHeight + bottomBlockHeight + topBlockheight,
            myHeight)};
      }

      bottomBlockBrushFunc =
          [this, &originalBrush, bottomBlockHeight, newBottomTopZPos] {
            // bottom: plane 2: point1,2,3 (Z)
            // top: plane 3: point1,2,3 (Z)

            auto brush = originalBrush;

            brush[3].point1.z = newBottomTopZPos;
            brush[3].point2.z = newBottomTopZPos;
            brush[3].point3.z = newBottomTopZPos;

            return brush;
          };

      topBlockBrushFunc =
          [this, &originalBrush, topBlockheight, newTopBottomZPos] {
            auto brush = originalBrush;

            brush[2].point1.z = newTopBottomZPos;
            brush[2].point2.z = newTopBottomZPos;
            brush[2].point3.z = newTopBottomZPos;

            return brush;
          };
    }
    else {
      const auto bottomBlockHeight = getOrDefault<double>("bottomheight");
      const auto topBlockheight    = getOrDefault<double>("topheight");

      bottomBlockBrushFunc = [this,
                              &originalBrush,
                              bottomBlockHeight,
                              topBlockheight] {
        auto brush = originalBrush;

        const auto [minZ, maxZ] = find_min_max_coordinate(brush, Coordinate::Z);

        const auto myHeight =
            std::abs((maxZ - topBlockheight) - (minZ + bottomBlockHeight));

        if (myHeight > MaxHeight) {
          throw std::runtime_error {fmt::format(
              "{} must be a maximum of {} units tall (current height is {})",
              ClassName,
              MaxHeight + bottomBlockHeight + topBlockheight,
              myHeight + bottomBlockHeight + topBlockheight)};
        }

        for (auto &face : brush) {
          if (face.point1.z == maxZ) {
            face.point1.z = minZ + bottomBlockHeight;
          }

          if (face.point2.z == maxZ) {
            face.point2.z = minZ + bottomBlockHeight;
          }

          if (face.point3.z == maxZ) {
            face.point3.z = minZ + bottomBlockHeight;
          }
        }

        return brush;
      };

      topBlockBrushFunc = [this,
                           &originalBrush,
                           bottomBlockHeight,
                           topBlockheight] {
        auto brush = originalBrush;

        const auto [minZ, maxZ] = find_min_max_coordinate(brush, Coordinate::Z);

        const auto myHeight =
            std::abs((maxZ - topBlockheight) - (minZ + bottomBlockHeight));

        if (myHeight > MaxHeight) {
          throw std::runtime_error {fmt::format(
              "{} must be a maximum of {} units tall (current height is {})",
              ClassName,
              MaxHeight + bottomBlockHeight + topBlockheight,
              myHeight + bottomBlockHeight + topBlockheight)};
        }

        for (auto &face : brush) {
          if (face.point1.z == minZ) {
            face.point1.z = maxZ - topBlockheight;
          }

          if (face.point2.z == minZ) {
            face.point2.z = maxZ - topBlockheight;
          }

          if (face.point3.z == minZ) {
            face.point3.z = maxZ - topBlockheight;
          }
        }

        return brush;
      };
    }

    const auto topBlockBrush    = topBlockBrushFunc();
    const auto bottomBlockBrush = bottomBlockBrushFunc();

    return fmt::format(
        R"(
{{
{layer}"classname" "func_wall"
"renderamt" "{bottom_render_amt}"
"rendercolor" "{bottom_render_color}"
"rendermode" "{bottom_render_mode}"
{bottom_block_brush}
}}

{{
{layer}"classname" "func_door"
"rendercolor" "0 0 0"
"angles" "90 0 0"
"speed" "10000"
"stopsnd" "3"
"wait" "0.1"
"lip" "-32"
"dmg" "-10000"
"targetname" "remec_inf_hp_booster"
"rendermode" "2"
"zhlt_lightflags" "0"
"style" "0"
{door_brush}
}}

{{
{layer}"classname" "func_wall"
"renderamt" "{top_render_amt}"
"rendercolor" "{top_render_color}"
"rendermode" "{top_render_mode}"
{top_block_brush}
}}

{{
{layer}"classname" "trigger_multiple"
"target" "remec_inf_hp_booster"
"wait" "1"
"style" "32"
{trigger_brush}
}}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("original_brush", brushes_to_str(brushes)),
        fmt::arg("door_brush", brush_to_str(topBlockBrush)),
        fmt::arg("trigger_brush",
                 brush_to_str(replace_brush_textures(originalBrush))),
        fmt::arg("bottom_block_brush", brush_to_str(bottomBlockBrush)),
        fmt::arg("bottom_render_mode", getOrDefault<int>("bottomrendermode")),
        fmt::arg("bottom_render_amt", getOrDefault<int>("bottomrenderamt")),
        fmt::arg("bottom_render_color", getOrDefault("bottomrendercolor")),
        fmt::arg("top_block_brush", brush_to_str(topBlockBrush)),
        fmt::arg("top_render_mode", getOrDefault<int>("toprendermode")),
        fmt::arg("top_render_amt", getOrDefault<int>("toprenderamt")),
        fmt::arg("top_render_color", getOrDefault("toprendercolor")));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &HPBooster::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {"bottomheight",
                   16.0,
                   "Bottom block height",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"bottomrendermode",
                   1,
                   "Bottom block render mode",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"bottomrenderamt",
                   60,
                   "Bottom block FX Amount (1 - 255)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"bottomrendercolor",
                   "0 0 0",
                   "Bottom block FX Color (R G B)",
                   OwnProperty::Type::Color255,
                   {}},
      OwnProperty {
          "topheight", 16.0, "Top block height", OwnProperty::Type::String, {}},
      OwnProperty {"toprendermode",
                   1,
                   "Top block render mode",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"toprenderamt",
                   60,
                   "Top block FX Amount (1 - 255)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"toprendercolor",
                   "0 0 0",
                   "Top block FX Color (R G B)",
                   OwnProperty::Type::Color255,
                   {}},
  };

  return props;
}

REGISTER_PLUGIN(HPBooster)
