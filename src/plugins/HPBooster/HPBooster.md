# HPBooster

Infinite HP booster

**NOTE**: the brush should be, at a maximum, **\*152** units high\*

## Properties

| Property            | Default value | Description              | Possible values                                                                                    |
| ------------------- | ------------- | ------------------------ | -------------------------------------------------------------------------------------------------- |
| `bottomheight`      | `16`          | Bottom block height      |                                                                                                    |
| `topheight`         | `16`          | Top block height         |                                                                                                    |
| `bottomrendermode`  | `1`           | Bottom block render mode | <ul><li>Normal</li><li>Color</li><li>Texture</li><li>Glow</li><li>Solid</li><li>Additive</li></ul> |
| `toprendermode`     | `1`           | Top block render mode    | <ul><li>Normal</li><li>Color</li><li>Texture</li><li>Glow</li><li>Solid</li><li>Additive</li></ul> |
| `bottomrenderamt`   | `60`          | Bottom block FX Amount   | `0-255`                                                                                            |
| `toprenderamt`      | `60`          | Top block FX Amount      | `0-255`                                                                                            |
| `bottomrendercolor` | `0 0 0`       | Bottom block FX Color    | `0-255` `0-255` `0-255`                                                                            |
| `toprendercolor`    | `0 0 0`       | Top block FX Color       | `0-255` `0-255` `0-255`                                                                            |

## Generates

```mermaid
graph TD;

subgraph remec_hp_booster
  FWT["func_wall"]
  style FWT stroke:lightblue,color:lightblue;
  FWB["func_wall"]
  style FWB stroke:lightblue,color:lightblue;
  FD["func_door"]
  style FD stroke:lightgreen,color:lightgreen;
  TM["trigger_multiple"]
  style TM stroke:violet,color:violet;

  TM --> FD
  linkStyle 0 fill:none,stroke:violet;
end

```
