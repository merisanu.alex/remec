#include "SkyLight.hpp"

#include "common/LogWrapper.hpp"
#include "tracing/ITracer.hpp"

#include <Utils.hpp>
#include <array>
#include <bitset>

namespace {
auto findCstrikeDirForSky(const Context &context)
{
  REMEC_TRACE(Category::remec_plugin, "findCstrikeDirForSky");

  const auto &skyname = context.skyName;

  if (skyname.empty()) {
    throw std::runtime_error {"Map has no sky set !"};
  }

  const auto &halfLifePath = context.halfLifePath;

  if (halfLifePath.empty()) {
    throw std::runtime_error {"Path to HalfLife not specified !"};
  }

  const std::array dirs {halfLifePath / "cstrike",
                         halfLifePath / "cstrike_downloads",
                         halfLifePath / "cstrike_hd",
                         halfLifePath / "valve"};

  for (auto dir : dirs) {
    dir /= "gfx";
    dir /= "env";

    if (std::filesystem::exists(dir / fmt::format("{}bk.tga", skyname))) {
      logging(Level::Debug,
              "Found sky '{}' in '{}'",
              asColor<Color::blue>(skyname),
              asColor<Color::blue>(dir.string()));
      return dir;
    }
  }

  throw std::runtime_error {"cstrike dir of sky not found !"};
}
} // namespace

tl::expected<std::string, std::string> SkyLight::serialize() const
{
  try {
    const auto faces      = getOrDefault<int>("faces");
    const auto &context   = getContext();
    const auto cstrikeDir = findCstrikeDirForSky(context);
    const auto facesBits  = std::bitset<6> {static_cast<std::uint32_t>(faces)};

    const std::array FacesSuffixes {
        std::pair {"up", facesBits[0]},
        std::pair {"bk", facesBits[1]},
        std::pair {"lf", facesBits[2]},
        std::pair {"ft", facesBits[3]},
        std::pair {"rt", facesBits[4]},
        std::pair {"dn", facesBits[5]},
    };

    std::vector<std::filesystem::path> faceFilesForDetection;

    for (const auto &[suffix, enabled] : FacesSuffixes) {
      if (not enabled) {
        continue;
      }

      faceFilesForDetection.push_back(
          cstrikeDir / fmt::format("{}{}.tga", context.skyName, suffix));
    }

    const auto dominantColor = faceFilesForDetection.empty()
                                   ? RGB {255, 255, 128}
                                   : get_dominant_color(faceFilesForDetection);

    if (not faceFilesForDetection.empty()) {
      logging(Level::Info,
              "Deduced dominant color: {} {} {}",
              asColor<Color::red>(dominantColor.r),
              asColor<Color::green>(dominantColor.g),
              asColor<Color::blue>(dominantColor.b));
    }

    auto props        = getFilteredProperties();
    const auto origin = props.extract("origin");
    const auto light =
        fmt::format("{r} {g} {b} {i}",
                    fmt::arg("r", dominantColor.r),
                    fmt::arg("g", dominantColor.g),
                    fmt::arg("b", dominantColor.b),
                    fmt::arg("i", getOrDefault<int>("intensity")));
    auto diffuse = getOrDefault("diffuse_light");

    if (diffuse.empty()) {
      diffuse = light;
    }

    return fmt::format(
        R"(
{{
{layer}"classname" "light_environment"
"origin" "{origin}"
"_light" "{light}"
"pitch" "{pitch}"
"_spread" "{spread:.{decimals_count}f}"
"_diffuse_light" "{diffuse_light}"{light_env_props}
}}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("origin", origin.mapped()),
        fmt::arg("light", light),
        fmt::arg("pitch", getOrDefault<int>("pitch")),
        fmt::arg("spread", getOrDefault<double>("spread")),
        fmt::arg("diffuse_light", diffuse),
        fmt::arg("decimals_count", DecimalsCount),
        fmt::arg("light_env_props", properties_map_to_str(props)));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &SkyLight::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {"faces",
                   63,
                   "Sky faces to include in dominant color detection",
                   OwnProperty::Type::Flags,
                   {
                       {1, "up", 1},
                       {2, "back", 1},
                       {4, "left", 1},
                       {8, "front", 1},
                       {16, "right", 1},
                       {32, "down", 1},
                   }},
      OwnProperty {
          "intensity", 200, "Light intensity", OwnProperty::Type::Integer, {}},
      OwnProperty {"pitch",
                   -90,
                   "Light pitch (-90 down, 90 up)",
                   OwnProperty::Type::Integer,
                   {}},
      OwnProperty {
          "spread", 0.0, "Shadow spread", OwnProperty::Type::String, {}},
      OwnProperty {"diffuse_light",
                   "",
                   "Ambient color",
                   OwnProperty::Type::Color255,
                   {}},
      OwnProperty {"_fade", "1.0", "ZHLT Fade", OwnProperty::Type::String, {}},
      OwnProperty {"_falloff",
                   0,
                   "ZHLT Falloff",
                   OwnProperty::Type::Choices,
                   {
                       {0, "Default"},
                       {1, "Inverse Linear"},
                       {2, "Inverse Square"},
                   }},
  };

  return props;
}

REGISTER_PLUGIN(SkyLight)
