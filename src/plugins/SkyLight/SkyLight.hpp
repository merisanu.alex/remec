#pragma once

#include <common/CustomEntity.hpp>

class SkyLight final : public CustomEntity<SkyLight> {
public:
  using CustomEntity<SkyLight>::CustomEntity;

  static constexpr inline auto ClassName = "remec_sky_light";
  static constexpr inline auto Version   = "2.0.0";
  static constexpr inline auto Description =
      "light_environment that takes it's color from the sky";
  static constexpr inline auto TargetNameMandatory = false;
  static constexpr inline auto Solid               = false;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
