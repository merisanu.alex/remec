# SkyLight

A `light_environment` point entity that automatically takes
the *predominant color* from the map sky.

**Note:** a sky **MUST** be set for this entity to work!

## Properties

| Property        | Default value        | Description                                         | Possible values                                                                         |
| --------------- | -------------------- | --------------------------------------------------- | --------------------------------------------------------------------------------------- |
| `faces`         | `63`<sup>all</sup>   | Which sky faces to consider when deducing the color | <ul><li>up</li><li>back</li><li>left</li><li>front</li><li>right</li><li>down</li></ul> |
| `intensity`     | `200`                | Light intensity                                     |                                                                                         |
| `pitch`         | `-90`<sup>down</sup> | Light pitch                                         | `0-360`                                                                                 |
| `spread`        | `0.0`                | Shadow spread                                       |                                                                                         |
| `diffuse_light` | *predominant color*  | Ambient color                                       | `0-255` `0-255` `0-255`                                                                 |
| `_fade`         | `1.0`                | ZHLT Fade                                           |                                                                                         |
| `_falloff`      | `0`                  | ZHLT Falloff                                        | <ul><li>Default</li><li>Inverse Linear</li><li>Inverse Square</li></ul>                 |

## Generates

```mermaid
graph TD;

subgraph remec_sky_light
  LE["light_environment"]
end
```
