#pragma once

#include <common/CustomEntity.hpp>

class MultiBhopBlock final : public CustomEntity<MultiBhopBlock> {
public:
  using CustomEntity<MultiBhopBlock>::CustomEntity;

  static constexpr inline auto ClassName           = "remec_multi_bhop_block";
  static constexpr inline auto Version             = "2.0.0";
  static constexpr inline auto Description         = "Static bhop block";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
