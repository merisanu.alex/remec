# SlipRamp

A surf brush that does **NOT** allow the player to sit on top (player will slip).

***NOTE:*** Make sure you are using the **latest** *[UMHLT](https://github.com/FWGS/UMHLT)* or this will **NOT** work!

## Properties

| Property                   | Default value        | Description                                | Possible values                  |
| -------------------------- | -------------------- | ------------------------------------------ | -------------------------------- |
| `standingshapename`        | `remec_slip_ramp_sh` | Name of the generated standing hull shape  |                                  |
| `crouchingshapename`       | `remec_slip_ramp_ch` | Name of the generated crouching hull shape |                                  |
| `zhlt_detaillevel`         | `1`                  | Detail level                               |                                  |
| `zhlt_chopdown`            | `0`                  | Lower its level to chop others             |                                  |
| `zhlt_chopup`              | `0`                  | Raise its level to chop others             |                                  |
| `zhlt_clipnodedetaillevel` | `1`                  | Detail level of cliphulls                  |                                  |
| `zhlt_noclip`              | `0`                  | Passable                                   | <ul><li>No</li><li>Yes</li></ul> |

## Generates

```mermaid
graph TD;

subgraph remec_slip_ramp
  SHS["info_hullshape"]
  CHS["info_hullshape"]
  FD["func_detail"]

  FD -.zhlt_hull1.-> SHS
  FD -.zhlt_hull3.-> CHS
end
```
