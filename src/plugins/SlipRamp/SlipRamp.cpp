#include "SlipRamp.hpp"

#include "Utils.hpp"

#include <fmt/format.h>

namespace {
std::vector<Brush> adjust_brushes(std::vector<Brush> brushesToAdjust,
                                  const Point3D &relativeTo)
{
  if (brushesToAdjust.empty()) {
    throw std::runtime_error {"Adjusting failed ! No brushes provided !"};
  }

  for (auto &brush : brushesToAdjust) {
    for (auto i = 0u; i != brush.size(); ++i) {
      auto &adjustedFace = brush[i];

      adjustedFace.point1 += relativeTo;
      adjustedFace.point2 += relativeTo;
      adjustedFace.point3 += relativeTo;
    }
  }

  return brushesToAdjust;
}
} // namespace

tl::expected<std::string, std::string> SlipRamp::serialize() const
{
  try {
    const auto standingHullName     = getOrDefault("standingshapename");
    const auto crouchingHullName    = getOrDefault("crouchingshapename");
    const auto funcDetailProperties = getFilteredProperties();
    const auto myBrushes            = getBrushes();

    static const auto standingHsBrushes = brushes_from_str(R"(
{
( -16 16 72 ) ( -16 -16 72 ) ( -16 16 32 ) AAATRIGGER [ 0 1 0 0 ] [ 0 0 1 0 ] 0 1 1
( 0 0 0 ) ( -16 16 32 ) ( -16 -16 32 ) AAATRIGGER [ 0 1 0 0 ] [ 0 0 1 0 ] 0 1 1
( -16 -16 72 ) ( 16 -16 72 ) ( -16 -16 32 ) AAATRIGGER [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 0 0 0 ) ( -16 -16 32 ) ( 16 -16 32 ) AAATRIGGER [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 16 16 72 ) ( 16 -16 72 ) ( -16 16 72 ) AAATRIGGER [ 1 0 0 0 ] [ 0 -1 0 0 ] 0 1 1
( 0 0 0 ) ( 16 16 32 ) ( -16 16 32 ) AAATRIGGER [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 16 16 72 ) ( -16 16 72 ) ( 16 16 32 ) AAATRIGGER [ 1 0 0 0 ] [ 0 0 -1 0 ] 0 1 1
( 0 0 0 ) ( 16 -16 32 ) ( 16 16 32 ) AAATRIGGER [ 0 1 0 0 ] [ 0 0 1 0 ] 0 1 1
( 16 -16 72 ) ( 16 16 72 ) ( 16 -16 32 ) AAATRIGGER [ 0 1 0 0 ] [ 0 0 1 0 ] 0 1 1
}
{
( -4 4 40 ) ( -4 -4 40 ) ( -4 4 32 ) ORIGIN [ 0 0 1 -8 ] [ 0 1 0 12 ] 0 0.5 0.5
( -4 -4 32 ) ( -4 -4 40 ) ( 4 -4 32 ) ORIGIN [ 1 0 0 0 ] [ 0 0 -1 8 ] 0 0.5 0.5
( 4 -4 32 ) ( 4 4 32 ) ( -4 -4 32 ) ORIGIN [ 1 0 0 0 ] [ 0 1 0 12 ] 0 0.5 0.5
( 4 4 40 ) ( 4 -4 40 ) ( -4 4 40 ) ORIGIN [ 1 0 0 0 ] [ 0 1 0 12 ] 0 0.5 0.5
( -4 4 40 ) ( -4 4 32 ) ( 4 4 40 ) ORIGIN [ 1 0 0 0 ] [ 0 0 -1 8 ] 0 0.5 0.5
( 4 -4 40 ) ( 4 4 40 ) ( 4 -4 32 ) ORIGIN [ 0 0 1 -8 ] [ 0 1 0 12 ] 0 0.5 0.5
}
)");

    static const auto crouchingHsBrushes = brushes_from_str(R"(
{
( -16 -16 32 ) ( -16 16 32 ) ( -16 -16 36 ) AAATRIGGER [ 0 1 0 8 ] [ 0 0 1 -10 ] 0 1 1
( -16 -16 32 ) ( 0 0 0 ) ( -16 16 32 ) AAATRIGGER [ 0 1 0 8 ] [ 0 0 1 -8 ] 0 1 1
( 16 -16 32 ) ( -16 -16 32 ) ( 16 -16 36 ) AAATRIGGER [ 1 0 0 -8 ] [ 0 0 -1 10 ] 0 1 1
( 16 -16 32 ) ( 0 0 0 ) ( -16 -16 32 ) AAATRIGGER [ 1 0 0 -8 ] [ 0 0 -1 8 ] 0 1 1
( -16 -16 36 ) ( -16 16 36 ) ( 16 -16 36 ) AAATRIGGER [ 1 0 0 -8 ] [ 0 -1 0 -8 ] 0 1 1
( -16 16 32 ) ( 0 0 0 ) ( 16 16 32 ) AAATRIGGER [ 1 0 0 -8 ] [ 0 0 -1 8 ] 0 1 1
( -16 16 32 ) ( 16 16 32 ) ( -16 16 36 ) AAATRIGGER [ 1 0 0 -8 ] [ 0 0 -1 10 ] 0 1 1
( 16 16 32 ) ( 0 0 0 ) ( 16 -16 32 ) AAATRIGGER [ 0 1 0 8 ] [ 0 0 1 -8 ] 0 1 1
( 16 16 32 ) ( 16 -16 32 ) ( 16 16 36 ) AAATRIGGER [ 0 1 0 8 ] [ 0 0 1 -10 ] 0 1 1
}
{
( -4 -4 22 ) ( -4 -4 14 ) ( -4 4 22 ) ORIGIN [ 0 0 1 -12 ] [ 0 1 0 8 ] 0 0.5 0.5
( -4 -4 22 ) ( 4 -4 22 ) ( -4 -4 14 ) ORIGIN [ 1 0 0 -8 ] [ 0 0 -1 12 ] 0 0.5 0.5
( 4 4 14 ) ( -4 4 14 ) ( 4 -4 14 ) ORIGIN [ 1 0 0 -8 ] [ 0 1 0 8 ] 0 0.5 0.5
( 4 -4 22 ) ( -4 -4 22 ) ( 4 4 22 ) ORIGIN [ 1 0 0 -8 ] [ 0 1 0 8 ] 0 0.5 0.5
( -4 4 14 ) ( 4 4 14 ) ( -4 4 22 ) ORIGIN [ 1 0 0 -8 ] [ 0 0 -1 12 ] 0 0.5 0.5
( 4 4 22 ) ( 4 4 14 ) ( 4 -4 22 ) ORIGIN [ 0 0 1 -12 ] [ 0 1 0 8 ] 0 0.5 0.5
}
)");

    const auto relativeTo = origin_from_str(get_origin(myBrushes));

    const auto hullShapes = fmt::format(
        R"(
{{
{layer}"classname" "info_hullshape"
"targetname" "{standing_name}"
"angles" "0 0 0"
{standing_brushes}
}}

{{
{layer}"classname" "info_hullshape"
"targetname" "{crouching_name}"
"angles" "0 0 0"
{crouching_brushes}
}}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("standing_name", standingHullName),
        fmt::arg("crouching_name", crouchingHullName),
        fmt::arg("standing_brushes",
                 brushes_to_str(adjust_brushes(standingHsBrushes, relativeTo))),
        fmt::arg(
            "crouching_brushes",
            brushes_to_str(adjust_brushes(crouchingHsBrushes, relativeTo))));

    return fmt::format(
        R"(
{hull_shapes}

{{
{layer}"classname" "func_detail"
"zhlt_hull1" "{standing_hull_name}"
"zhlt_hull3" "{crouching_hull_name}"{detail_properties}
{brushes}
}}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("hull_shapes", hullShapes),
        fmt::arg("brushes", brushes_to_str(myBrushes)),
        fmt::arg("detail_properties",
                 properties_map_to_str(funcDetailProperties)),
        fmt::arg("standing_hull_name", standingHullName),
        fmt::arg("crouching_hull_name", crouchingHullName));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &SlipRamp::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {"standingshapename",
                   "remec_slip_ramp_sh",
                   "Name of standing hullshape",
                   OwnProperty::Type::TargetSource,
                   {}},
      OwnProperty {"crouchingshapename",
                   "remec_slip_ramp_ch",
                   "Name of crouching hullshape",
                   OwnProperty::Type::TargetSource,
                   {}},
      OwnProperty {"zhlt_detaillevel",
                   1,
                   "Detail level",
                   OwnProperty::Type::Integer,
                   {}},
      OwnProperty {"zhlt_chopdown",
                   0,
                   "Lower its level to chop others",
                   OwnProperty::Type::Integer,
                   {}},
      OwnProperty {"zhlt_chopup",
                   0,
                   "Raise its level to chop others",
                   OwnProperty::Type::Integer,
                   {}},
      OwnProperty {"zhlt_clipnodedetaillevel",
                   1,
                   "Detail level of cliphulls",
                   OwnProperty::Type::Integer,
                   {}},
      OwnProperty {"zhlt_noclip",
                   0,
                   "Passable",
                   OwnProperty::Type::Choices,
                   {
                       {0, "No"},
                       {1, "Yes"},
                   }},
  };

  return props;
}

REGISTER_PLUGIN(SlipRamp)
