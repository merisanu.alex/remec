#pragma once

#include <common/CustomEntity.hpp>

class MultiTargetTeleport final : public CustomEntity<MultiTargetTeleport> {
public:
  using CustomEntity<MultiTargetTeleport>::CustomEntity;

  static constexpr inline auto ClassName = "remec_multi_target_teleport";
  static constexpr inline auto Version   = "2.0.0";
  static constexpr inline auto Description =
      "Teleport to multiple switchable targets";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = true;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
