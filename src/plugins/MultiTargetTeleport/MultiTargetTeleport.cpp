#include "MultiTargetTeleport.hpp"

#include <Utils.hpp>
#include <fmt/format.h>

/*
  TP_CT0 --TP--> TPDEST1
  TP_CT1 --TP--> TPDEST2
  TP_CT2 --TP--> TPDEST0

  MM0  ------> TP_CT0
  MM0  ------> BTN_CT0

  MM1  ------> TP_CT1
  MM1  ------> BTN_CT1

  MM2  ------> TP_CT2
  MM2  ------> BTN_CT2

  BTN_CT0 --BTN-> MM1
  BTN_CT1 --BTN-> MM2
  BTN_CT2 --BTN-> MM0

  BTN --> MM0
*/
tl::expected<std::string, std::string> MultiTargetTeleport::serialize() const
{
  try {
    const auto filteredProps = getFilteredProperties();
    const auto sortedTargets =
        sort(map(filteredProps,
                 [](const PropertiesMap::value_type &item) {
                   return item.first;
                 }),
             SizeAwareLexicographicCompare);

    if (sortedTargets.size() < 2) {
      throw std::runtime_error {
          fmt::format("{} requires at least 2 targets", ClassName)};
    }

    const auto originalName = getName();
    const auto trigger      = getOrDefault("trigger");

    if (trigger.empty()) {
      throw std::runtime_error {
          fmt::format("{} requires a trigger", ClassName)};
    }

    std::string loop;

    for (auto i = 0u; i < sortedTargets.size(); ++i) {
      const auto lastIteration = i + 1 == sortedTargets.size();
      const auto nextIndex     = lastIteration ? 0 : (i + 1);

      const auto currentMultiManagerName =
          originalName + (i != 0 ? fmt::format("_mm{}", i) : "");

      const auto nextMultiManagerName =
          originalName +
          (not lastIteration ? fmt::format("_mm{}", nextIndex) : "");

      loop += fmt::format(
          R"(
{{
{layer}"classname" "trigger_changetarget"
"targetname" "{original_name}_tpct{current_index}"
"target" "{original_name}_tt"
"m_iszNewTarget" "{next_target}"
}}

{{
{layer}"classname" "multi_manager"
"targetname" "{current_mm}"
"{original_name}_tpct{current_index}" "{delay:.{decimals_count}f}"
"{original_name}_triggerct{current_index}" "0"
}}

{{
{layer}"classname" "trigger_changetarget"
"targetname" "{original_name}_triggerct{current_index}"
"target" "{trigger}"
"m_iszNewTarget" "{next_mm}"
}}
)",
          fmt::arg("layer", getLayer()),
          fmt::arg("decimals_count", DecimalsCount),
          fmt::arg("original_name", originalName),
          fmt::arg("current_index", i),
          fmt::arg("current_mm", currentMultiManagerName),
          fmt::arg("delay", std::stod(filteredProps.at(sortedTargets[i]))),
          fmt::arg("next_mm", nextMultiManagerName),
          fmt::arg("next_index", nextIndex),
          fmt::arg("next_target", sortedTargets[nextIndex]),
          fmt::arg("trigger", trigger));
    }

    return fmt::format(R"(
{{
{layer}"classname" "trigger_teleport"
"targetname" "{original_name}_tt"
"target" "{first_target}"
{brushes}
}}

{loop}
)",
                       fmt::arg("layer", getLayer()),
                       fmt::arg("original_name", originalName),
                       fmt::arg("first_target", sortedTargets.front()),
                       fmt::arg("brushes", brushes_to_str(getBrushes())),
                       fmt::arg("loop", loop));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &MultiTargetTeleport::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"trigger",
                   "",
                   "Button/trigger that should change the target",
                   OwnProperty::Type::TargetDestination,
                   {}}};

  return props;
}

REGISTER_PLUGIN(MultiTargetTeleport)
