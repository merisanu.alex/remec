#include "MultiTargetTeleport.hpp"

#include <PluginDiscovery.hpp>
#include <PluginEntityFactoryImpl.hpp>
#include <SharedLibraryUtils.hpp>
#include <Utils.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

class MultiTargetTeleportFixture {
public:
  MultiTargetTeleportFixture()
  {
    const auto discoveredPluginsPaths = discover_plugins(REMEC_BINARY_DIR);

    m_entityFactory = std::make_unique<PluginEntityFactoryImpl>(
        "", discoveredPluginsPaths, "");

    auto lib =
        m_entityFactory->getLibraryForClassName(MultiTargetTeleport::ClassName);

    newInstanceFunc = get_library_function_T<newInstance_t>(lib, newInstance_s);
    deleteInstanceFunc =
        get_library_function_T<deleteInstance_t>(lib, deleteInstance_s);
  }

  std::unique_ptr<PluginEntityFactoryImpl> m_entityFactory;
  newInstance_t newInstanceFunc       = nullptr;
  deleteInstance_t deleteInstanceFunc = nullptr;
};

TEST_CASE_METHOD(MultiTargetTeleportFixture, "TestMultiTargetTeleport")
{
  SECTION("getters")
  {
    auto ars = newInstanceFunc(Context {},
                               0,
                               1,
                               {{"classname", MultiTargetTeleport::ClassName},
                                {"targetname", "mymm"},
                                {"trigger", "someBTN"}},
                               {});

    REQUIRE(ars->getEntityClassName() ==
            std::string(MultiTargetTeleport::ClassName));
    REQUIRE(ars->getName() == "mymm");
    REQUIRE(ars->getOriginalStartPos() == 0);
    REQUIRE(ars->getOriginalLen() == 1);
    REQUIRE(ars->getProperties().size() == 3);
    REQUIRE(ars->getBrushes().size() == 0);

    deleteInstanceFunc(ars);
  }

  SECTION("serialize")
  {
    auto ars =
        newInstanceFunc(Context {},
                        0,
                        1,
                        {
                            {"targetname", "mymm"},
                            {"classname", MultiTargetTeleport::ClassName},
                            {"trigger", "someBTN"},
                            {"target1", "0"},
                            {"target2", "1"},
                            {"target3", "2"},
                            {"target4", "3"},
                            {"target5", "4"},
                            {"target6", "5"},
                            {"target7", "6"},
                            {"target8", "7"},
                            {"target9", "8"},
                            {"target10", "9"},
                        },
                        brushes_from_str(R"({
( -134 -41 -52 ) ( -134 -40 -52 ) ( -134 -41 -51 ) some_texture [ 0 -1 0 50] [ 0 0 -1 -4 ] 0 1 1
( -148 -28 -52 ) ( -148 -28 -51 ) ( -147 -28 -52 ) some_texture [ 1 0 0 27 ] [ 0 0 -1 -4 ] 0 1 1
( -153 -41 -26 ) ( -152 -41 -26 ) ( -153 -40 -26 ) some_texture [ -1 0 0 -32.307175 ] [ 0 -1 0 50 ] 0 1 1
( -93 44 -11 ) ( -93 45 -11 ) ( -92 44 -11 ) some_texture [ 1 0 0 27 ] [ 0 -1 0 50 ] 0 1 1
( -93 -17 -44 ) ( -92 -17 -44 ) ( -93 -17 -43 ) some_texture [ -1 0 0 -27 ] [ 0 0 -1 2.2004757 ] 0 1 1
( -120 44 -49 ) ( -120 44 -48 ) ( -120 45 -49 ) some_texture [ 0 1 0 -50 ] [ 0 0 -1 -2.8588524 ] 0 1 1
})"));

    const std::string expected = R"(
{
"classname" "trigger_teleport"
"targetname" "mymm_tt"
"target" "target1"
{
( -134.000000 -41.000000 -52.000000 ) ( -134.000000 -40.000000 -52.000000 ) ( -134.000000 -41.000000 -51.000000 ) some_texture [ 0.000000 -1.000000 0.000000 50.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -148.000000 -28.000000 -52.000000 ) ( -148.000000 -28.000000 -51.000000 ) ( -147.000000 -28.000000 -52.000000 ) some_texture [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -153.000000 -41.000000 -26.000000 ) ( -152.000000 -41.000000 -26.000000 ) ( -153.000000 -40.000000 -26.000000 ) some_texture [ -1.000000 0.000000 0.000000 -32.307175 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 44.000000 -11.000000 ) ( -93.000000 45.000000 -11.000000 ) ( -92.000000 44.000000 -11.000000 ) some_texture [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 -17.000000 -44.000000 ) ( -92.000000 -17.000000 -44.000000 ) ( -93.000000 -17.000000 -43.000000 ) some_texture [ -1.000000 0.000000 0.000000 -27.000000 ] [ 0.000000 0.000000 -1.000000 2.200476 ] 0.000000 1.000000 1.000000
( -120.000000 44.000000 -49.000000 ) ( -120.000000 44.000000 -48.000000 ) ( -120.000000 45.000000 -49.000000 ) some_texture [ 0.000000 1.000000 0.000000 -50.000000 ] [ 0.000000 0.000000 -1.000000 -2.858852 ] 0.000000 1.000000 1.000000
}

}


{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct0"
"target" "mymm_tt"
"m_iszNewTarget" "target2"
}

{
"classname" "multi_manager"
"targetname" "mymm"
"mymm_tpct0" "0.000000"
"mymm_triggerct0" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct0"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm1"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct1"
"target" "mymm_tt"
"m_iszNewTarget" "target3"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm1"
"mymm_tpct1" "1.000000"
"mymm_triggerct1" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct1"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm2"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct2"
"target" "mymm_tt"
"m_iszNewTarget" "target4"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm2"
"mymm_tpct2" "2.000000"
"mymm_triggerct2" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct2"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm3"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct3"
"target" "mymm_tt"
"m_iszNewTarget" "target5"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm3"
"mymm_tpct3" "3.000000"
"mymm_triggerct3" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct3"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm4"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct4"
"target" "mymm_tt"
"m_iszNewTarget" "target6"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm4"
"mymm_tpct4" "4.000000"
"mymm_triggerct4" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct4"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm5"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct5"
"target" "mymm_tt"
"m_iszNewTarget" "target7"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm5"
"mymm_tpct5" "5.000000"
"mymm_triggerct5" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct5"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm6"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct6"
"target" "mymm_tt"
"m_iszNewTarget" "target8"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm6"
"mymm_tpct6" "6.000000"
"mymm_triggerct6" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct6"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm7"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct7"
"target" "mymm_tt"
"m_iszNewTarget" "target9"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm7"
"mymm_tpct7" "7.000000"
"mymm_triggerct7" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct7"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm8"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct8"
"target" "mymm_tt"
"m_iszNewTarget" "target10"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm8"
"mymm_tpct8" "8.000000"
"mymm_triggerct8" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct8"
"target" "someBTN"
"m_iszNewTarget" "mymm_mm9"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_tpct9"
"target" "mymm_tt"
"m_iszNewTarget" "target1"
}

{
"classname" "multi_manager"
"targetname" "mymm_mm9"
"mymm_tpct9" "9.000000"
"mymm_triggerct9" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_triggerct9"
"target" "someBTN"
"m_iszNewTarget" "mymm"
}

)";

    const auto outStr        = ars->serialize().value();
    const auto output        = split(outStr, '\n');
    const auto expectedSplit = split(expected, '\n');

    // REQUIRE(outStr == expected);
    REQUIRE(output.size() == expectedSplit.size());

    for (auto i = 0u; i != output.size(); ++i) {
      REQUIRE(output[i] == expectedSplit[i]);
    }

    deleteInstanceFunc(ars);
  }
}
