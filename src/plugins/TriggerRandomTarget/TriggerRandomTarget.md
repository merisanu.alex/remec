# TriggerRandomTarget

This entity receives a list of targets (**2+**) from which it will randomly choose 1 target and trigger it.

Depending on `type`, it will either trigger 1 entity once, or will continuously trigger different entities.

If `starttype` is set to `Start on trigger`, the entity will not trigger any target untill it is itself triggered by something.

***IMPORTANT NOTES***:

- Place this entity where players **CANNOT** reach it (or they will **die**)!

- Although it is a *point entity*, it will generate *brushes*!

- There is no limit to how many targets it can trigger **but** it is recommended
to have a maximum of **6 targets/entity** (randomness quality decreases for 7+ targets)

## Properties

| Property     | Default value                        | Description            | Possible values                                                |
| ------------ | ------------------------------------ | ---------------------- | -------------------------------------------------------------- |
| `targetname` |                                      | Name                   |                                                                |
| `delay`      | `0.1`                                | Delay between triggers |                                                                |
| `starttype`  | `Start automatically`<sup>(1)</sup>  |                        | <ul><li>Start automatically</li><li>Start on trigger</li></ul> |
| `type`       | `Trigger continuously`<sup>(1)</sup> |                        | <ul><li>Trigger continuously</li><li>Trigger once</li></ul>    |
| `target1`    |                                      | Target to trigger      |                                                                |
| `target2`    |                                      | ...                    |                                                                |
| `target3`    |                                      | ...                    |                                                                |
| ....         |                                      | ...                    |                                                                |

## Generates

- ### Trigger continuously + Start automatically

```mermaid
graph LR;

TGT1["target 1"]
TGT2["target 2"]
TGT3["target 3"]
TGT4["target 4"]
TGT5["target 5"]
TGT6["target 6"]
TGT7["target 7"]
TGT8["target 8"]

subgraph remec_trigger_random_target
  subgraph Group 1
    G1_BEAM["env_beam"]
    style G1_BEAM stroke:cyan,color:cyan;
    G1_BEAM_START["info_target"]
    G1_BTN1["func_button"]
    style G1_BTN1 stroke:red,color:red;
    G1_BTN2["func_button"]
    style G1_BTN2 stroke:red,color:red;
    G1_BTN3["func_button"]
    style G1_BTN3 stroke:red,color:red;
    G1_BTN4["func_button"]
    style G1_BTN4 stroke:red,color:red;
    G1_BTN5["func_button"]
    style G1_BTN5 stroke:red,color:red;
    G1_BTN6["func_button"]
    style G1_BTN6 stroke:red,color:red;
    G1_BEAM_END1["info_target"]
    G1_BEAM_END2["info_target"]
    G1_BEAM_END3["info_target"]
    G1_BEAM_END4["info_target"]
    G1_BEAM_END5["info_target"]
    G1_BEAM_END6["info_target"]
    MM["multi_manager"]
    style MM stroke:orange,color:orange;

    G1_BEAM -.- G1_BEAM_START
    linkStyle 0 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN1 -.-> G1_BEAM_END1
    linkStyle 1 stroke:cyan,stroke-dasharray:10;
    linkStyle 2 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN2 -.-> G1_BEAM_END2
    linkStyle 3 stroke:cyan,stroke-dasharray:10;
    linkStyle 4 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN3 -.-> G1_BEAM_END3
    linkStyle 5 stroke:cyan,stroke-dasharray:10;
    linkStyle 6 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN4 -.-> G1_BEAM_END4
    linkStyle 7 stroke:cyan,stroke-dasharray:10;
    linkStyle 8 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN5 -.-> G1_BEAM_END5
    linkStyle 9 stroke:cyan,stroke-dasharray:10;
    linkStyle 10 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN6 -.-> G1_BEAM_END6
    linkStyle 11 stroke:cyan,stroke-dasharray:10;
    linkStyle 12 stroke:cyan,stroke-dasharray:10;
    G1_BTN6 --> MM
    linkStyle 13 stroke:red;
  end

  subgraph Group 2
    G2_BEAM["env_beam"]
    style G2_BEAM stroke:cyan,color:cyan;
    G2_BEAM_START["info_target"]
    G2_BTN1["func_button"]
    style G2_BTN1 stroke:red,color:red;
    G2_BTN2["func_button"]
    style G2_BTN2 stroke:red,color:red;
    G2_BTN3["func_button"]
    style G2_BTN3 stroke:red,color:red;
    G2_BEAM_END1["info_target"]
    G2_BEAM_END2["info_target"]
    G2_BEAM_END3["info_target"]

    G2_BEAM -.- G2_BEAM_START
    linkStyle 14 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN1 -.-> G2_BEAM_END1
    linkStyle 15 stroke:cyan,stroke-dasharray:10;
    linkStyle 16 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN2 -.-> G2_BEAM_END2
    linkStyle 17 stroke:cyan,stroke-dasharray:10;
    linkStyle 18 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN3 -.-> G2_BEAM_END3
    linkStyle 19 stroke:cyan,stroke-dasharray:10;
    linkStyle 20 stroke:cyan,stroke-dasharray:10;
  end
end

MM ---> G2_BEAM
linkStyle 21 stroke:orange;

linkStyle default fill:none,stroke:red;

G1_BTN1 --------> TGT1
G1_BTN2 --------> TGT2
G1_BTN3 --------> TGT3
G1_BTN4 --------> TGT4
G1_BTN5 --------> TGT5

G2_BTN1 ---> TGT6
G2_BTN2 ---> TGT7
G2_BTN3 ---> TGT8
```

- ### Trigger once + Start automatically

```mermaid
graph LR;

TGT1["target 1"]
TGT2["target 2"]
TGT3["target 3"]
TGT4["target 4"]
TGT5["target 5"]
TGT6["target 6"]
TGT7["target 7"]
TGT8["target 8"]

subgraph remec_trigger_random_target
  MM_BEAMS["multi_manager"]
  style MM_BEAMS stroke:orange,color:orange;

  subgraph Group 1
    G1_BEAM["env_beam"]
    style G1_BEAM stroke:cyan,color:cyan;
    G1_BEAM_START["info_target"]
    G1_BTN1["func_button"]
    style G1_BTN1 stroke:red,color:red;
    G1_BTN2["func_button"]
    style G1_BTN2 stroke:red,color:red;
    G1_BTN3["func_button"]
    style G1_BTN3 stroke:red,color:red;
    G1_BTN4["func_button"]
    style G1_BTN4 stroke:red,color:red;
    G1_BTN5["func_button"]
    style G1_BTN5 stroke:red,color:red;
    G1_BTN6["func_button"]
    style G1_BTN6 stroke:red,color:red;
    G1_BEAM_END1["info_target"]
    G1_BEAM_END2["info_target"]
    G1_BEAM_END3["info_target"]
    G1_BEAM_END4["info_target"]
    G1_BEAM_END5["info_target"]
    G1_BEAM_END6["info_target"]
    G1_TGT_MM1["multi_manager"]
    style G1_TGT_MM1 stroke:orange,color:orange;
    G1_TGT_MM2["multi_manager"]
    style G1_TGT_MM2 stroke:orange,color:orange;
    G1_TGT_MM3["multi_manager"]
    style G1_TGT_MM3 stroke:orange,color:orange;
    G1_TGT_MM4["multi_manager"]
    style G1_TGT_MM4 stroke:orange,color:orange;
    G1_TGT_MM5["multi_manager"]
    style G1_TGT_MM5 stroke:orange,color:orange;
    MM["multi_manager"]
    style MM stroke:orange,color:orange;

    G1_BEAM -.- G1_BEAM_START
    linkStyle 0 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN1 -.-> G1_BEAM_END1
    linkStyle 1 stroke:cyan,stroke-dasharray:10;
    linkStyle 2 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN2 -.-> G1_BEAM_END2
    linkStyle 3 stroke:cyan,stroke-dasharray:10;
    linkStyle 4 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN3 -.-> G1_BEAM_END3
    linkStyle 5 stroke:cyan,stroke-dasharray:10;
    linkStyle 6 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN4 -.-> G1_BEAM_END4
    linkStyle 7 stroke:cyan,stroke-dasharray:10;
    linkStyle 8 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN5 -.-> G1_BEAM_END5
    linkStyle 9 stroke:cyan,stroke-dasharray:10;
    linkStyle 10 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN6 -.-> G1_BEAM_END6
    linkStyle 11 stroke:cyan,stroke-dasharray:10;
    linkStyle 12 stroke:cyan,stroke-dasharray:10;
    G1_BTN6 --> MM
    linkStyle 13 stroke:red;
  end

  subgraph Group 2
    G2_BEAM["env_beam"]
    style G2_BEAM stroke:cyan,color:cyan;
    G2_BEAM_START["info_target"]
    G2_BTN1["func_button"]
    style G2_BTN1 stroke:red,color:red;
    G2_BTN2["func_button"]
    style G2_BTN2 stroke:red,color:red;
    G2_BTN3["func_button"]
    style G2_BTN3 stroke:red,color:red;
    G2_TGT_MM1["multi_manager"]
    style G2_TGT_MM1 stroke:orange,color:orange;
    G2_TGT_MM2["multi_manager"]
    style G2_TGT_MM2 stroke:orange,color:orange;
    G2_TGT_MM3["multi_manager"]
    style G2_TGT_MM3 stroke:orange,color:orange;
    G2_BEAM_END1["info_target"]
    G2_BEAM_END2["info_target"]
    G2_BEAM_END3["info_target"]

    G2_BEAM -.- G2_BEAM_START
    linkStyle 14 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN1 -.-> G2_BEAM_END1
    linkStyle 15 stroke:cyan,stroke-dasharray:10;
    linkStyle 16 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN2 -.-> G2_BEAM_END2
    linkStyle 17 stroke:cyan,stroke-dasharray:10;
    linkStyle 18 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN3 -.-> G2_BEAM_END3
    linkStyle 19 stroke:cyan,stroke-dasharray:10;
    linkStyle 20 stroke:cyan,stroke-dasharray:10;
  end
end

MM ---> G2_BEAM
linkStyle 21 stroke:orange;

MM_BEAMS --> G1_BEAM
linkStyle 22 stroke:orange;
MM_BEAMS --> G2_BEAM
linkStyle 23 stroke:orange;

linkStyle default fill:none,stroke:red;

G1_BTN1 --------> G1_TGT_MM1
G1_BTN2 --------> G1_TGT_MM2
G1_BTN3 --------> G1_TGT_MM3
G1_BTN4 --------> G1_TGT_MM4
G1_BTN5 --------> G1_TGT_MM5

G1_TGT_MM1 ------> TGT1
G1_TGT_MM1 ------> MM_BEAMS
linkStyle 29 stroke:orange;
linkStyle 30 stroke:orange;
G1_TGT_MM2 ------> TGT2
G1_TGT_MM2 ------> MM_BEAMS
linkStyle 31 stroke:orange;
linkStyle 32 stroke:orange;
G1_TGT_MM3 ------> TGT3
G1_TGT_MM3 ------> MM_BEAMS
linkStyle 33 stroke:orange;
linkStyle 34 stroke:orange;
G1_TGT_MM4 ------> TGT4
G1_TGT_MM4 ------> MM_BEAMS
linkStyle 35 stroke:orange;
linkStyle 36 stroke:orange;
G1_TGT_MM5 ------> TGT5
G1_TGT_MM5 ------> MM_BEAMS
linkStyle 37 stroke:orange;
linkStyle 38 stroke:orange;

G2_BTN1 --------> G2_TGT_MM1
G2_BTN2 --------> G2_TGT_MM2
G2_BTN3 --------> G2_TGT_MM3

G2_TGT_MM1 ----> TGT6
G2_TGT_MM1 ------> MM_BEAMS
linkStyle 42 stroke:orange;
linkStyle 43 stroke:orange;
G2_TGT_MM2 ----> TGT7
G2_TGT_MM2 ------> MM_BEAMS
linkStyle 44 stroke:orange;
linkStyle 45 stroke:orange;
G2_TGT_MM3 ----> TGT8
G2_TGT_MM3 ------> MM_BEAMS
linkStyle 46 stroke:orange;
linkStyle 47 stroke:orange;
```

- ### Trigger continuously + Start on trigger

```mermaid
graph LR;

TGR["external trigger"]

TGT1["target 1"]
TGT2["target 2"]
TGT3["target 3"]
TGT4["target 4"]
TGT5["target 5"]
TGT6["target 6"]
TGT7["target 7"]
TGT8["target 8"]

subgraph remec_trigger_random_target
  subgraph Group 1
    G1_BEAM["env_beam"]
    style G1_BEAM stroke:cyan,color:cyan;
    G1_BEAM_START["info_target"]
    G1_BTN1["func_button"]
    style G1_BTN1 stroke:red,color:red;
    G1_BTN2["func_button"]
    style G1_BTN2 stroke:red,color:red;
    G1_BTN3["func_button"]
    style G1_BTN3 stroke:red,color:red;
    G1_BTN4["func_button"]
    style G1_BTN4 stroke:red,color:red;
    G1_BTN5["func_button"]
    style G1_BTN5 stroke:red,color:red;
    G1_BTN6["func_button"]
    style G1_BTN6 stroke:red,color:red;

    G1_BEAM_END1["info_target"]
    G1_BEAM_END2["info_target"]
    G1_BEAM_END3["info_target"]
    G1_BEAM_END4["info_target"]
    G1_BEAM_END5["info_target"]
    G1_BEAM_END6["info_target"]

    G1_BEAM -.- G1_BEAM_START
    linkStyle 0 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN1 -.-> G1_BEAM_END1
    linkStyle 1 stroke:cyan,stroke-dasharray:10;
    linkStyle 2 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN2 -.-> G1_BEAM_END2
    linkStyle 3 stroke:cyan,stroke-dasharray:10;
    linkStyle 4 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN3 -.-> G1_BEAM_END3
    linkStyle 5 stroke:cyan,stroke-dasharray:10;
    linkStyle 6 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN4 -.-> G1_BEAM_END4
    linkStyle 7 stroke:cyan,stroke-dasharray:10;
    linkStyle 8 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN5 -.-> G1_BEAM_END5
    linkStyle 9 stroke:cyan,stroke-dasharray:10;
    linkStyle 10 stroke:cyan,stroke-dasharray:10;
    G1_BEAM -.- G1_BTN6 -.-> G1_BEAM_END6
    linkStyle 11 stroke:cyan,stroke-dasharray:10;
    linkStyle 12 stroke:cyan,stroke-dasharray:10;
  end

  subgraph Group 2
    G2_BEAM["env_beam"]
    style G2_BEAM stroke:cyan,color:cyan;
    G2_BEAM_START["info_target"]
    G2_BTN1["func_button"]
    style G2_BTN1 stroke:red,color:red;
    G2_BTN2["func_button"]
    style G2_BTN2 stroke:red,color:red;
    G2_BEAM_END1["info_target"]
    G2_BEAM_END2["info_target"]

    G2_BEAM -.- G2_BEAM_START
    linkStyle 13 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN1 -.-> G2_BEAM_END1
    linkStyle 14 stroke:cyan,stroke-dasharray:10;
    linkStyle 15 stroke:cyan,stroke-dasharray:10;
    G2_BEAM -.- G2_BTN2 -.-> G2_BEAM_END2
    linkStyle 16 stroke:cyan,stroke-dasharray:10;
    linkStyle 17 stroke:cyan,stroke-dasharray:10;
  end

  MM["multi_manager"]
  style MM stroke:orange,color:orange;
  MM --> G1_BEAM
  linkStyle 18 stroke:orange;
  MM --> G2_BEAM
  linkStyle 19 stroke:orange;
end

G1_BTN1 ---> TGT1
linkStyle 20 stroke:red;
G1_BTN2 ---> TGT2
linkStyle 21 stroke:red;
G1_BTN3 ---> TGT3
linkStyle 22 stroke:red;
G1_BTN4 ---> TGT4
linkStyle 23 stroke:red;
G1_BTN5 ---> TGT5
linkStyle 24 stroke:red;
G1_BTN6 ---> TGT6
linkStyle 25 stroke:red;

G2_BTN1 ---> TGT7
linkStyle 26 stroke:red;
G2_BTN2 ---> TGT8
linkStyle 27 stroke:red;

TGR --> MM

```
