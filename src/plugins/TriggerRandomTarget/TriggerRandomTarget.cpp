#include "TriggerRandomTarget.hpp"

#include <Utils.hpp>
#include <array>
#include <cassert>
#include <fmt/format.h>

namespace {
auto getEmptyBrush()
{
  auto brush = replace_brush_textures(Brush {6}, "NOCLIP");

  for (auto &face : brush) {
    face.textureAxis1.x = -1;
    face.textureAxis2.z = -1;
  }

  brush[0].textureAxis1.x = 0;
  brush[5].textureAxis1.x = 0;

  brush[2].textureAxis2.y = -1;
  brush[3].textureAxis2.y = -1;
  brush[2].textureAxis2.z = 0;
  brush[3].textureAxis2.z = 0;

  return brush;
}
} // namespace

tl::expected<std::string, std::string> TriggerRandomTarget::serialize() const
{
  try {
    const auto brushes = getBrushes();
    const auto props   = getFilteredProperties();
    const auto targets = sort(map(props,
                                  [](const auto &pair) {
                                    return pair.first;
                                  }),
                              SizeAwareLexicographicCompare);

    if (targets.size() < 2) {
      throw std::runtime_error {
          fmt::format("{} requires at least 2 targets", ClassName)};
    }

    std::vector<std::vector<std::string>> targetGroups;

    if (targets.size() > 6) {
      std::vector<std::string> *currentGroup = nullptr;

      // split into groups of 5 targets + 1 target to next group
      for (auto i = 0u; i < targets.size(); ++i) {
        if (i % 5 == 0) {
          currentGroup = &targetGroups.emplace_back();
        }

        currentGroup->push_back(targets[i]);
      }
    }
    else {
      auto group = &targetGroups.emplace_back();

      for (auto i = 0u; i < targets.size(); ++i) {
        group->push_back(targets[i]);
      }
    }

    constexpr auto GroupRadius = 32.0;

    const Point3D origin = origin_from_str(getOrDefault("origin"));

    const auto getTargetOrigin = [&origin, GroupRadius](auto targetIndex,
                                                        auto groupIndex) {
      constexpr auto InfoTargetSize = 9;

      assert(targetIndex <= 5);

      switch (targetIndex) {
      case 0:
        return Point3D {origin.x + InfoTargetSize + groupIndex * GroupRadius,
                        origin.y,
                        origin.z};

      case 1:
        return Point3D {origin.x + groupIndex * GroupRadius,
                        origin.y + InfoTargetSize,
                        origin.z};

      case 2:
        return Point3D {origin.x - InfoTargetSize + groupIndex * GroupRadius,
                        origin.y,
                        origin.z};

      case 3:
        return Point3D {origin.x + groupIndex * GroupRadius,
                        origin.y - InfoTargetSize,
                        origin.z};

      case 4:
        return Point3D {origin.x + groupIndex * GroupRadius,
                        origin.y,
                        origin.z - InfoTargetSize};

      case 5:
        return Point3D {origin.x + groupIndex * GroupRadius,
                        origin.y,
                        origin.z + InfoTargetSize};
      }

      return Point3D {};
    };

    const auto getButtonBrushes = [&origin, GroupRadius](auto targetIndex,
                                                         auto groupIndex) {
      static const auto EmptyBrush = getEmptyBrush();

      // clang-format off
    static constexpr std::array IndexOffsetsFromStartBeam {
      /// btn1
      std::array{
        std::array{ Point3D{4, -13, 17 }, Point3D{4, 9, -4 }, Point3D{4, 9, 17 } },
        std::array{ Point3D{22, -4, 17 }, Point3D{-5, -4, -3 }, Point3D{-5, -4, 17 } },
        std::array{ Point3D{22, 9, -4 }, Point3D{-5, -13, -4 }, Point3D{22, -13, -4 } },
        std::array{ Point3D{22, 9, 4 }, Point3D{-5, -13, 4 }, Point3D{-5, 9, 4 } },
        std::array{ Point3D{22, 4, 17 }, Point3D{-5, 4, -4 }, Point3D{22, 4, -4 } },
        std::array{ Point3D{5, 9, 15 }, Point3D{5, -12, -5 }, Point3D{5, -12, 15 } },
      },
      /// btn2
      std::array{
        std::array{ Point3D{-4, 4, 17 }, Point3D{-4, 26, -4 }, Point3D{-4, 26, 17 } },
        std::array{ Point3D{15, 4, 17 }, Point3D{-12, 4, -4 }, Point3D{-12, 4, 17 } },
        std::array{ Point3D{15, 26, -4 }, Point3D{-12, 4, -4 }, Point3D{15, 4, -4 } },
        std::array{ Point3D{15, 26, 4 }, Point3D{-12, 4, 4 }, Point3D{-12, 26, 4 } },
        std::array{ Point3D{14, 5, 17 }, Point3D{-12, 5, -4 }, Point3D{14, 5, -4 } },
        std::array{ Point3D{4, 26, 15 }, Point3D{4, 4, -5 }, Point3D{4, 4, 15 } },
      },
      /// btn3
      std::array{
        std::array{ Point3D{-5, -13, 17 }, Point3D{-5, 9, -4 }, Point3D{-5, 9, 17 } },
        std::array{ Point3D{13, -4, 17 }, Point3D{-14, -4, -3 }, Point3D{-14, -4, 17 } },
        std::array{ Point3D{13, 9, -4 }, Point3D{-14, -13, -4 }, Point3D{13, -13, -4 } },
        std::array{ Point3D{13, 9, 4 }, Point3D{-14, -13, 4 }, Point3D{-14, 9, 4 } },
        std::array{ Point3D{13, 4, 17 }, Point3D{-14, 4, -4 }, Point3D{13, 4, -4 } },
        std::array{ Point3D{-4, 9, 15 }, Point3D{-4, -12, -5 }, Point3D{-4, -12, 15 } },
      },
      /// btn4
      std::array{
        std::array{ Point3D{-4, -26, 17 }, Point3D{-4, -4, -4 }, Point3D{-4, -4, 17 } },
        std::array{ Point3D{13, -5, 17 }, Point3D{-13, -5, -4 }, Point3D{-13, -5, 17 } },
        std::array{ Point3D{13, -4, -4 }, Point3D{-14, -26, -4 }, Point3D{13, -26, -4 } },
        std::array{ Point3D{13, -4, 4 }, Point3D{-14, -26, 4 }, Point3D{-14, -4, 4 } },
        std::array{ Point3D{13, -4, 17 }, Point3D{-14, -4, -4 }, Point3D{13, -4, -4 } },
        std::array{ Point3D{4, -4, 17 }, Point3D{4, -26, -4 }, Point3D{4, -26, 17 } },
      },
      // btn5
      std::array{
        std::array{ Point3D{-4, 10, -4 }, Point3D{-4, -12, -31 }, Point3D{-4, 10, -31 } },
        std::array{ Point3D{-17, -4, -4 }, Point3D{4, -4, -31 }, Point3D{-17, -4, -31 } },
        std::array{ Point3D{-16, -12, -5 }, Point3D{5, 10, -5 }, Point3D{-16, 10, -5 } },
        std::array{ Point3D{-17, 10, -4 }, Point3D{4, -12, -4 }, Point3D{-17, -12, -4 } },
        std::array{ Point3D{-17, 4, -4 }, Point3D{4, 4, -31 }, Point3D{4, 4, -4 } },
        std::array{ Point3D{4, 10, -4 }, Point3D{4, -12, -31 }, Point3D{4, -12, -4 } },
      },
      // btn6
      std::array{
        std::array{ Point3D{-4, -10, 4, }, Point3D{-4, 12, 31, }, Point3D{-4, -10, 31, } },
        std::array{ Point3D{-17, -4, 4, }, Point3D{4, -4, 31, }, Point3D{4, -4, 4, } },
        std::array{ Point3D{-17, -10, 4, }, Point3D{4, 12, 4, }, Point3D{-17, 12, 4, } },
        std::array{ Point3D{-16, 12, 5, }, Point3D{4, -10, 5, }, Point3D{-16, -10, 5, } },
        std::array{ Point3D{-17, 4, 4, }, Point3D{4, 4, 31, }, Point3D{-17, 4, 31, } },
        std::array{ Point3D{4, -10, 4, }, Point3D{4, 12, 31, }, Point3D{4, 12, 4, } },
      }
    };
      // clang-format on

      static constexpr std::array<std::array<Point3D, 6>, 6>
          IndexOffsetOfOriginBrush {
              std::array {Point3D {1, 0, 0},
                          Point3D {1, 0, 0},
                          Point3D {1, 0, 0},
                          Point3D {1, 0, 0},
                          Point3D {1, 0, 0},
                          Point3D {2, 0, 0}},

              std::array {Point3D {0, 1, 0},
                          Point3D {0, 1, 0},
                          Point3D {0, 1, 0},
                          Point3D {0, 1, 0},
                          Point3D {0, 2, 0},
                          Point3D {0, 1, 0}},

              std::array {Point3D {-2, 0, 0},
                          Point3D {-1, 0, 0},
                          Point3D {-1, 0, 0},
                          Point3D {-1, 0, 0},
                          Point3D {-1, 0, 0},
                          Point3D {-1, 0, 0}},

              std::array {Point3D {0, -1, 0},
                          Point3D {0, -2, 0},
                          Point3D {0, -1, 0},
                          Point3D {0, -1, 0},
                          Point3D {0, -1, 0},
                          Point3D {0, -1, 0}},

              std::array {Point3D {0, 0, -1},
                          Point3D {0, 0, -1},
                          Point3D {0, 0, -2},
                          Point3D {0, 0, -1},
                          Point3D {0, 0, -1},
                          Point3D {0, 0, -1}},

              std::array {Point3D {0, 0, 1},
                          Point3D {0, 0, 1},
                          Point3D {0, 0, 1},
                          Point3D {0, 0, 2},
                          Point3D {0, 0, 1},
                          Point3D {0, 0, 1}},
          };

      auto newBrush       = EmptyBrush;
      auto newOriginBrush = replace_brush_textures(EmptyBrush, "ORIGIN");

      for (auto i = 0u; i < newBrush.size(); ++i) {
        newOriginBrush[i].point1 = newBrush[i].point1 =
            origin + IndexOffsetsFromStartBeam[targetIndex][i][0];
        newBrush[i].point1.x += groupIndex * GroupRadius;

        newOriginBrush[i].point2 = newBrush[i].point2 =
            origin + IndexOffsetsFromStartBeam[targetIndex][i][1];
        newBrush[i].point2.x += groupIndex * GroupRadius;

        newOriginBrush[i].point3 = newBrush[i].point3 =
            origin + IndexOffsetsFromStartBeam[targetIndex][i][2];
        newBrush[i].point3.x += groupIndex * GroupRadius;

        newOriginBrush[i].point1 += IndexOffsetOfOriginBrush[targetIndex][i];
        newOriginBrush[i].point1.x += (groupIndex * GroupRadius);

        newOriginBrush[i].point2 += IndexOffsetOfOriginBrush[targetIndex][i];
        newOriginBrush[i].point2.x += (groupIndex * GroupRadius);

        newOriginBrush[i].point3 += IndexOffsetOfOriginBrush[targetIndex][i];
        newOriginBrush[i].point3.x += (groupIndex * GroupRadius);
      }

      std::vector<Brush> newBrushes;
      newBrushes.push_back(newBrush);
      newBrushes.push_back(newOriginBrush);

      return newBrushes;
    };

    std::string entity;

    const auto type      = getOrDefault("type");
    const auto starttype = getOrDefault("starttype");

    const auto getBeamTargets = [this, &targetGroups](auto delay) {
      std::string beamTargets;

      for (auto groupIndex = 0u; groupIndex < targetGroups.size();
           ++groupIndex) {
        beamTargets +=
            fmt::format(R"("{original_name}_rtrt_bm{group_index}" "{delay}"
)",
                        fmt::arg("original_name", getName()),
                        fmt::arg("delay", delay),
                        fmt::arg("group_index", groupIndex));
      }

      return beamTargets;
    };

    if (starttype != "1" or type != "1") {
      const auto beamTargets = getBeamTargets(0);

      entity = fmt::format(R"(
{{
{layer}"classname" "multi_manager"
"targetname" "{original_name}"
"origin" "{origin}"
{beam_targets}
}}
)",
                           fmt::arg("original_name", getName()),
                           fmt::arg("layer", getLayer()),
                           fmt::arg("origin", origin_to_str(origin)),
                           fmt::arg("beam_targets", beamTargets));
    }

    const auto delay = getOrDefault<double>("delay");

    // random strike + toggle
    auto beamFlags = 6;

    if (starttype == "1") {
      // start on
      beamFlags += 1;
    }

    for (auto groupIndex = 0u; groupIndex < targetGroups.size(); ++groupIndex) {
      const auto &targetGroup = targetGroups[groupIndex];

      entity += fmt::format(
          R"(
// <group {group_index} start>
{{
{layer}"classname" "env_beam"
"origin" "{origin}"
"damage" "999999"
"spawnflags" "{beam_flags}"
"targetname" "{original_name}_rtrt_bm{group_index}"
"LightningStart" "{original_name}_rtrt_bm{group_index}"
"LightningEnd" "{original_name}_rtrt_end{group_index}"
"life" "0.01"
"StrikeTime" "{delay}"
"BoltWidth" "0"
"NoiseAmplitude" "0"
"Radius" "256"
"TextureScroll" "35"
"framerate" "0"
"framestart" "0"
"renderamt" "100"
"rendercolor" "0 0 0"
"renderfx" "0"
"texture" "sprites/laserbeam.spr"
}}

)",
          fmt::arg("original_name", getName()),
          fmt::arg("layer", getLayer()),
          fmt::arg("delay", delay),
          fmt::arg("origin",
                   origin_to_str(
                       origin +
                       Point3D {static_cast<double>(groupIndex * GroupRadius),
                                0,
                                0})),
          fmt::arg("beam_flags", beamFlags),
          fmt::arg("group_index", groupIndex));

      const auto moreGroups =
          groupIndex + 1 < targetGroups.size() and targetGroups.size() > 1;

      for (auto targetIndex = 0u; targetIndex < targetGroup.size();
           ++targetIndex) {
        const auto targetOriginStr =
            origin_to_str(getTargetOrigin(targetIndex, groupIndex));

        const auto target = targetGroup[targetIndex];

        entity += type == "1"
                      ? std::string {}
                      : fmt::format(R"(
{{
{layer}"classname" "multi_manager"
"targetname" "{original_name}_rtrt_mm_proxy{target_index}"
"origin" "{target_origin}"
"{target}" "0"
"{original_name}" "0"
}}
)",
                                    fmt::arg("original_name", getName()),
                                    fmt::arg("layer", getLayer()),
                                    fmt::arg("target", target),
                                    fmt::arg("target_index", targetIndex),
                                    fmt::arg("target_origin", targetOriginStr));

        const auto targetName =
            type == "1"
                ? target
                : fmt::format("{}_rtrt_mm_proxy{}", getName(), targetIndex);

        entity += fmt::format(
            R"(
{{
{layer}"classname" "func_button"
"targetname" "{original_name}_rtrt_end{group_index}"
"spawnflags" "1"
"health" "1"
"target" "{target}"
{brushes}
}}
)",
            fmt::arg("original_name", getName()),
            fmt::arg("layer", getLayer()),
            fmt::arg("group_index", groupIndex),
            fmt::arg("target", targetName),
            fmt::arg(
                "brushes",
                brushes_to_str(getButtonBrushes(targetIndex, groupIndex))));

        const auto lastTarget = targetIndex + 1 == targetGroup.size();

        if (lastTarget and moreGroups) {
          const auto nextBeamTarget =
              fmt::format("{original_name}_rtrt_bm{next_group_index}",
                          fmt::arg("original_name", getName()),
                          fmt::arg("next_group_index", groupIndex + 1));

          entity += fmt::format(
              R"(
{{
{layer}"classname" "func_button"
"targetname" "{original_name}_rtrt_end{group_index}"
"spawnflags" "1"
"health" "1"
"target" "{next_beam_target}"
{brushes}
}}
)",
              fmt::arg("original_name", getName()),
              fmt::arg("layer", getLayer()),
              fmt::arg("group_index", groupIndex),
              fmt::arg(
                  "brushes",
                  brushes_to_str(getButtonBrushes(targetIndex, groupIndex))),
              fmt::arg("next_beam_target", nextBeamTarget));
        }
      }

      entity += fmt::format("// </group {} end>\n", groupIndex);
    }

    return entity;
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &TriggerRandomTarget::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"origin", "", "Origin", OwnProperty::Type::String, {}},
      OwnProperty {"delay",
                   0.1,
                   "Delay between triggers",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"starttype",
                   "1",
                   "Start type",
                   OwnProperty::Type::Choices,
                   {
                       {1, "Start automatically"},
                       {2, "Start on trigger"},
                   }},
      OwnProperty {"type",
                   "1",
                   "Type",
                   OwnProperty::Type::Choices,
                   {
                       {1, "Trigger continuously"},
                       {2, "Trigger once"},
                   }}};

  return props;
}

REGISTER_PLUGIN(TriggerRandomTarget)
