#pragma once

#include <common/CustomEntity.hpp>

class TriggerSequence final : public CustomEntity<TriggerSequence> {
public:
  using CustomEntity<TriggerSequence>::CustomEntity;

  static constexpr inline auto ClassName = "remec_trigger_sequence";
  static constexpr inline auto Version   = "2.0.0";
  static constexpr inline auto Description =
      "Trigger targets in a specific sequence";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = false;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
