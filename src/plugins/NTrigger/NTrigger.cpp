#include "NTrigger.hpp"

#include <Utils.hpp>
#include <fmt/format.h>

/*
  TM ----> MM0

  MM0 ----> TM_CT0
  MM0 ----> UT0

  MM1 ----> TM_CT1
  MM1 ----> UT1

  MM2 ----> TM_CT2
  MM2 ----> UT2
  MM2 ----> {target}

  TM_CT0 --TM--> MM1
  TM_CT1 --TM--> MM2
  TM_CT2 --TM--> MM0
*/

tl::expected<std::string, std::string> NTrigger::serialize() const
{
  try {
    const auto brushes = getBrushes();
    const auto props   = getFilteredProperties();
    const auto iterations =
        sort(filter(map(props,
                        [](const auto &kv) {
                          try {
                            return static_cast<int>(std::stoi(kv.first));
                          }
                          catch (...) {
                            return -1;
                          }
                        }),
                    [](const auto &val) {
                      return val != -1;
                    }));

    const auto getUserTargetProp = [&iterations,
                                    &props](const std::size_t &index) {
      const auto hasUserTarget = contains(iterations, static_cast<int>(index));

      if (hasUserTarget) {
        return fmt::format(R"(
"{}" "0")",
                           props.at(std::to_string(index)));
      }

      return std::string {};
    };

    const auto count = static_cast<std::size_t>(getOrDefault<int>("count"));

    if (count < 2) {
      throw std::runtime_error {fmt::format(
          "{} requires property 'count' to be greater or equal to 2",
          ClassName)};
    }

    std::string loop;

    for (auto i = 0u; i < count; ++i) {
      const auto lastIteration = i + 1 == count;
      const auto nextIndex     = lastIteration ? 0 : (i + 1);
      const auto actualTargetTrigger =
          lastIteration ? fmt::format("\n\"{}\" \"{}\"",
                                      getOrDefault("target"),
                                      getOrDefault<double>("delay"))
                        : std::string {};

      loop += fmt::format(R"(
{{
{layer}"classname" "multi_manager"
"targetname" "{original_name}_nt_mm{current_index}"
"{original_name}_nt_ct{current_index}" "0"{user_target}{actual_target}
}}

{{
{layer}"classname" "trigger_changetarget"
"targetname" "{original_name}_nt_ct{current_index}"
"target" "{original_name}_nt_tm"
"m_iszNewTarget" "{original_name}_nt_mm{next_index}"
}}
)",
                          fmt::arg("layer", getLayer()),
                          fmt::arg("original_name", getName()),
                          fmt::arg("current_index", i),
                          fmt::arg("next_index", nextIndex),
                          fmt::arg("user_target", getUserTargetProp(i)),
                          fmt::arg("actual_target", actualTargetTrigger));
    }

    return fmt::format(
        R"(
{{
{layer}"classname" "trigger_multiple"
"targetname" "{original_name}_nt_tm"
"target" "{original_name}_nt_mm0"
{brushes}
}}
{loop}

{{
{layer}"classname" "trigger_changetarget"
"targetname" "{original_name}"
"target" "{original_name}_nt_tm"
"m_iszNewTarget" "{original_name}_nt_mm0"
}}
)",
        fmt::arg("layer", getLayer()),
        fmt::arg("original_name", getName()),
        fmt::arg("brushes", brushes_to_str(getBrushes())),
        fmt::arg("loop", loop));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &NTrigger::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"target",
                   "",
                   "Target to trigger",
                   OwnProperty::Type::TargetDestination,
                   {}},
      OwnProperty {"count",
                   2,
                   "After how many triggers to actually trigger the target",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"delay",
                   0.0,
                   "Delay before actual trigger",
                   OwnProperty::Type::String,
                   {}},
  };

  return props;
}

REGISTER_PLUGIN(NTrigger)
