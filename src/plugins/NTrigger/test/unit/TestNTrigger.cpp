#include "NTrigger.hpp"

#include <PluginDiscovery.hpp>
#include <PluginEntityFactoryImpl.hpp>
#include <SharedLibraryUtils.hpp>
#include <Utils.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

class NTriggerFixture {
public:
  NTriggerFixture()
  {
    const auto discoveredPluginsPaths = discover_plugins(REMEC_BINARY_DIR);

    m_entityFactory = std::make_unique<PluginEntityFactoryImpl>(
        "", discoveredPluginsPaths, "");

    auto lib = m_entityFactory->getLibraryForClassName(NTrigger::ClassName);

    newInstanceFunc = get_library_function_T<newInstance_t>(lib, newInstance_s);
    deleteInstanceFunc =
        get_library_function_T<deleteInstance_t>(lib, deleteInstance_s);
  }

  std::unique_ptr<PluginEntityFactoryImpl> m_entityFactory;
  newInstance_t newInstanceFunc       = nullptr;
  deleteInstance_t deleteInstanceFunc = nullptr;
};

TEST_CASE_METHOD(NTriggerFixture, "TestNTrigger")
{
  SECTION("getters")
  {
    const auto ars = newInstanceFunc(Context {},
                                     0,
                                     1,
                                     {{"classname", NTrigger::ClassName},
                                      {"targetname", "mymm"},
                                      {"target", "someBTN"},
                                      {"count", "2"}},
                                     {});

    REQUIRE(ars->getEntityClassName() == std::string(NTrigger::ClassName));
    REQUIRE(ars->getName() == "mymm");
    REQUIRE(ars->getOriginalStartPos() == 0);
    REQUIRE(ars->getOriginalLen() == 1);
    REQUIRE(ars->getProperties().size() == 4);
    REQUIRE(ars->getBrushes().size() == 0);

    deleteInstanceFunc(ars);
  }

  SECTION("serialize")
  {
    const auto ars = newInstanceFunc(Context {},
                                     0,
                                     1,
                                     {
                                         {"targetname", "mymm"},
                                         {"classname", NTrigger::ClassName},
                                         {"target", "someBTN"},
                                         {"count", "10"},
                                         {"0", "target0"},
                                         {"1", "target1"},
                                         {"2", "target2"},
                                         {"3", "target3"},
                                         {"4", "target4"},

                                         {"6", "target6"},
                                         {"7", "target7"},
                                         {"8", "target8"},
                                         {"9", "target9"},
                                     },
                                     brushes_from_str(R"({
( -134 -41 -52 ) ( -134 -40 -52 ) ( -134 -41 -51 ) some_texture [ 0 -1 0 50] [ 0 0 -1 -4 ] 0 1 1
( -148 -28 -52 ) ( -148 -28 -51 ) ( -147 -28 -52 ) some_texture [ 1 0 0 27 ] [ 0 0 -1 -4 ] 0 1 1
( -153 -41 -26 ) ( -152 -41 -26 ) ( -153 -40 -26 ) some_texture [ -1 0 0 -32.307175 ] [ 0 -1 0 50 ] 0 1 1
( -93 44 -11 ) ( -93 45 -11 ) ( -92 44 -11 ) some_texture [ 1 0 0 27 ] [ 0 -1 0 50 ] 0 1 1
( -93 -17 -44 ) ( -92 -17 -44 ) ( -93 -17 -43 ) some_texture [ -1 0 0 -27 ] [ 0 0 -1 2.2004757 ] 0 1 1
( -120 44 -49 ) ( -120 44 -48 ) ( -120 45 -49 ) some_texture [ 0 1 0 -50 ] [ 0 0 -1 -2.8588524 ] 0 1 1
})"));

    const std::string expected = R"(
{
"classname" "trigger_multiple"
"targetname" "mymm_nt_tm"
"target" "mymm_nt_mm0"
{
( -134.000000 -41.000000 -52.000000 ) ( -134.000000 -40.000000 -52.000000 ) ( -134.000000 -41.000000 -51.000000 ) some_texture [ 0.000000 -1.000000 0.000000 50.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -148.000000 -28.000000 -52.000000 ) ( -148.000000 -28.000000 -51.000000 ) ( -147.000000 -28.000000 -52.000000 ) some_texture [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 0.000000 -1.000000 -4.000000 ] 0.000000 1.000000 1.000000
( -153.000000 -41.000000 -26.000000 ) ( -152.000000 -41.000000 -26.000000 ) ( -153.000000 -40.000000 -26.000000 ) some_texture [ -1.000000 0.000000 0.000000 -32.307175 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 44.000000 -11.000000 ) ( -93.000000 45.000000 -11.000000 ) ( -92.000000 44.000000 -11.000000 ) some_texture [ 1.000000 0.000000 0.000000 27.000000 ] [ 0.000000 -1.000000 0.000000 50.000000 ] 0.000000 1.000000 1.000000
( -93.000000 -17.000000 -44.000000 ) ( -92.000000 -17.000000 -44.000000 ) ( -93.000000 -17.000000 -43.000000 ) some_texture [ -1.000000 0.000000 0.000000 -27.000000 ] [ 0.000000 0.000000 -1.000000 2.200476 ] 0.000000 1.000000 1.000000
( -120.000000 44.000000 -49.000000 ) ( -120.000000 44.000000 -48.000000 ) ( -120.000000 45.000000 -49.000000 ) some_texture [ 0.000000 1.000000 0.000000 -50.000000 ] [ 0.000000 0.000000 -1.000000 -2.858852 ] 0.000000 1.000000 1.000000
}

}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm0"
"mymm_nt_ct0" "0"
"target0" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct0"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm1"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm1"
"mymm_nt_ct1" "0"
"target1" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct1"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm2"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm2"
"mymm_nt_ct2" "0"
"target2" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct2"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm3"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm3"
"mymm_nt_ct3" "0"
"target3" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct3"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm4"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm4"
"mymm_nt_ct4" "0"
"target4" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct4"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm5"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm5"
"mymm_nt_ct5" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct5"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm6"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm6"
"mymm_nt_ct6" "0"
"target6" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct6"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm7"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm7"
"mymm_nt_ct7" "0"
"target7" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct7"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm8"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm8"
"mymm_nt_ct8" "0"
"target8" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct8"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm9"
}

{
"classname" "multi_manager"
"targetname" "mymm_nt_mm9"
"mymm_nt_ct9" "0"
"target9" "0"
"someBTN" "0"
}

{
"classname" "trigger_changetarget"
"targetname" "mymm_nt_ct9"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm0"
}


{
"classname" "trigger_changetarget"
"targetname" "mymm"
"target" "mymm_nt_tm"
"m_iszNewTarget" "mymm_nt_mm0"
}
)";

    const auto outStr        = ars->serialize().value();
    const auto output        = split(outStr, '\n');
    const auto expectedSplit = split(expected, '\n');

    // REQUIRE(outStr == expected);
    // REQUIRE(output.size() == expectedSplit.size());

    for (auto i = 0u; i != output.size(); ++i) {
      REQUIRE(output[i] == expectedSplit[i]);
    }

    deleteInstanceFunc(ars);
  }
}
