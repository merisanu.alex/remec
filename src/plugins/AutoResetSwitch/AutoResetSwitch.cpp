#include "AutoResetSwitch.hpp"

#include <Utils.hpp>
#include <fmt/format.h>

tl::expected<std::string, std::string> AutoResetSwitch::serialize() const
{
  try {
    const auto brushes          = getBrushes();
    const auto buttonProperties = getFilteredProperties();

    return fmt::format(
        R"(
// button triggers {original_name}_mm after {delay:.{decimals_count}f} second(s)
{{
{layer}"classname" "func_button"
"target" "{original_name}_mm"
"delay" "{delay:.{decimals_count}f}"{button_properties}
{button_brushes}
}}

// {original_name}_mm triggers {target} instantly and {original_name}_mm2 after {delay2:.{decimals_count}f} second(s)
{{
{layer}"classname" "multi_manager"
"origin" "{origin}"
"targetname" "{original_name}_mm"
"{target}" "0"
"{original_name}_mm2" "{delay2:.{decimals_count}f}"
"spawnflags" "1"
}}

// {original_name}_mm2 triggers {target} instantly
{{
{layer}"classname" "multi_manager"
"origin" "{origin}"
"targetname" "{original_name}_mm2"
"{target}" "0"
"spawnflags" "1"
}}
)",
        fmt::arg("original_name", getName()),
        fmt::arg("layer", getLayer()),
        fmt::arg("decimals_count", DecimalsCount),
        fmt::arg("button_brushes", brushes_to_str(brushes)),
        fmt::arg("button_properties", properties_map_to_str(buttonProperties)),
        fmt::arg("origin", get_origin(brushes)),
        fmt::arg("delay", getOrDefault<double>("delay")),
        fmt::arg("delay2", getOrDefault<double>("delay2")),
        fmt::arg("target", getOrDefault("target")));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &AutoResetSwitch::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"delay",
                   0.0,
                   "Delay before first trigger",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"delay2",
                   1.0,
                   "Delay before reset trigger",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"target",
                   "",
                   "Target to trigger",
                   OwnProperty::Type::TargetDestination,
                   {}},
      OwnProperty {"globalname",
                   "",
                   "Global Entity Name",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"renderfx",
                   0,
                   "Render FX",
                   OwnProperty::Type::Choices,
                   {
                       {0, "Normal"},
                       {1, "Slow Pulse"},
                       {2, "Fast Pulse"},
                       {3, "Slow Wide Pulse"},
                       {4, "Fast Wide Pulse"},
                       {5, "Slow Fade Away"},
                       {6, "Fast Fade Away"},
                       {7, "Slow Become Solid"},
                       {8, "Fast Become Solid"},
                       {9, "Slow Strobe"},
                       {10, "Fast Strobe"},
                       {11, "Faster Strobe"},
                       {12, "Slow Flicker"},
                       {13, "Fast Flicker"},
                       {14, "Constant Glow"},
                       {15, "Distort"},
                       {16, "Hologram (Distort + fade)"},
                   }},

      OwnProperty {"rendermode",
                   0,
                   "Render Mode",
                   OwnProperty::Type::Choices,
                   {
                       {0, "Normal"},
                       {1, "Color"},
                       {2, "Texture"},
                       {3, "Glow"},
                       {4, "Solid"},
                       {5, "Additive"},
                   }},
      OwnProperty {"renderamt",
                   "",
                   "FX Amount (1 - 255)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"rendercolor",
                   "0 0 0",
                   "FX Color (R G B)",
                   OwnProperty::Type::Color255,
                   {}},
      OwnProperty {"angles",
                   "0 0 0",
                   "Pitch Yaw Roll (Y Z X)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {
          "zhlt_lightflags",
          0,
          "ZHLT Lightflags",
          OwnProperty::Type::Choices,
          {
              {0, "Default"},
              {1, "Embedded Fix"},
              {2, "Opaque (blocks light)"},
              {3, "Opaque + Embedded fix"},
              {6, "Opaque + Concave Fix"},
          },
      },
      OwnProperty {"light_origin",
                   "",
                   "Light Origin Target",
                   OwnProperty::Type::TargetDestination,
                   {}},
      OwnProperty {"speed", 5, "Speed", OwnProperty::Type::Integer, {}},
      OwnProperty {"health",
                   "",
                   "Health (shootable if > 0)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"lip", 0, "Lip", OwnProperty::Type::Integer, {}},
      OwnProperty {
          "master", "", "master", OwnProperty::Type::TargetDestination, {}},
      OwnProperty {
          "sounds",
          0,
          "Sounds",
          OwnProperty::Type::Choices,
          {
              {0, "None"},
              {1, "Big zap & Warmup"},
              {2, "Access Denied"},
              {3, "Access Granted"},
              {4, "Quick Combolock"},
              {5, "Power Deadbolt 1"},
              {6, "Power Deadbolt 2"},
              {7, "Plunger"},
              {8, "Small zap"},
              {9, "Keycard Sound"},
              {10, "Buzz"},
              {11, "Buzz Off"},
              {14, "Lightswitch"},
          },
      },
      OwnProperty {"wait",
                   3,
                   "Delay before reset (-1 stay)",
                   OwnProperty::Type::Integer,
                   {}},
      OwnProperty {"spawnflags",
                   0,
                   "Spawnflags",
                   OwnProperty::Type::Flags,
                   {
                       {1, "Don't move", 0},
                       {32, "Toggle", 0},
                       {64, "Sparks", 0},
                       {256, "Touch Activates", 0},
                   }},
      OwnProperty {
          "locked_sound",
          0,
          "Locked Sound",
          OwnProperty::Type::Choices,
          {
              {0, "None"},
              {2, "Access Denied"},
              {8, "Small zap"},
              {10, "Buzz"},
              {11, "Buzz Off"},
              {12, "Latch Locked"},
          },
      },
      OwnProperty {
          "unlocked_sound",
          0,
          "Unlocked Sound",
          OwnProperty::Type::Choices,
          {
              {0, "None"},
              {1, "Big zap & Warmup"},
              {3, "Access Granted"},
              {4, "Quick Combolock"},
              {5, "Power Deadbolt 1"},
              {6, "Power Deadbolt 2"},
              {7, "Plunger"},
              {8, "Small zap"},
              {9, "Keycard Sound"},
              {10, "Buzz"},
              {13, "Latch Unlocked"},
              {14, "Lightswitch"},
          },
      },
      OwnProperty {
          "locked_sentence",
          0,
          "Locked Sentence",
          OwnProperty::Type::Choices,
          {
              {0, "None"},
              {1, "Gen. Access Denied"},
              {2, "Security Lockout"},
              {3, "Blast Door"},
              {4, "Fire Door"},
              {5, "Chemical Door"},
              {6, "Radiation Door"},
              {7, "Gen. Containment"},
              {8, "Maintenance Door"},
              {9, "Broken Shut Door"},
          },
      },
      OwnProperty {
          "unlocked_sentence",
          0,
          "Unlocked Sentence",
          OwnProperty::Type::Choices,
          {
              {0, "None"},
              {1, "Gen. Access Granted"},
              {2, "Security Disengaged"},
              {3, "Blast Door"},
              {4, "Fire Door"},
              {5, "Chemical Door"},
              {6, "Radiation Door"},
              {7, "Gen. Containment"},
              {8, "Maintenance area"},
          },
      },
      OwnProperty {"_minlight",
                   "",
                   "Minimum light level",
                   OwnProperty::Type::String,
                   {}},
  };

  return props;
}

REGISTER_PLUGIN(AutoResetSwitch)
