#pragma once

#include <common/CustomEntity.hpp>

class MultiManager final : public CustomEntity<MultiManager> {
public:
  using CustomEntity<MultiManager>::CustomEntity;

  static constexpr inline auto ClassName           = "remec_multi_manager";
  static constexpr inline auto Version             = "2.0.0";
  static constexpr inline auto Description         = "No limit Multi Manager";
  static constexpr inline auto TargetNameMandatory = true;
  static constexpr inline auto Solid               = false;
  static constexpr inline auto Studio              = false;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
