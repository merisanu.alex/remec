#include "MultiManager.hpp"

#include <Utils.hpp>
#include <fmt/format.h>
#include <sstream>

tl::expected<std::string, std::string> MultiManager::serialize() const
{
  try {
    auto props                = getFilteredProperties();
    const auto origin         = props.extract("origin");
    std::string spawnFlagsStr = "0";

    if (const auto spawnFlags = props.extract("spawnflags")) {
      spawnFlagsStr = spawnFlags.mapped();
    }

    auto sortedPropKeys = sort(map(props,
                                   [](const auto &kv) {
                                     return kv.first;
                                   }),
                               SizeAwareLexicographicCompare);

    constexpr auto MaxTargetsPerMM = 8;

    auto mmCounter = 0u;

    const auto getMMName = [&mmCounter, entName = this->getName()] {
      if (mmCounter == 0u) {
        return entName;
      }

      return fmt::format("{}{}", entName, mmCounter);
    };

    std::ostringstream ss;

    auto propIndex = 0u;
    for (const auto &propName : sortedPropKeys) {
      const auto &propValue = props[propName];

      if ((propIndex % (MaxTargetsPerMM - (props.size() > MaxTargetsPerMM))) ==
          0) {
        if (propIndex != 0u) {
          ++mmCounter;

          ss << fmt::format(R"("{}" "0"
)",
                            getMMName())
             << "}\n";
        }

        ss << fmt::format(R"(
{{
{layer}"classname" "multi_manager"
"targetname" "{name}"
"origin" "{origin}"
"spawnflags" "{spawn_flags}"
"{target_name}" "{delay:.{decimals_count}f}"
)",
                          fmt::arg("layer", getLayer()),
                          fmt::arg("decimals_count", DecimalsCount),
                          fmt::arg("name", getMMName()),
                          fmt::arg("origin", origin.mapped()),
                          fmt::arg("target_name", propName),
                          fmt::arg("spawn_flags", spawnFlagsStr),
                          fmt::arg("delay", std::stod(propValue)));
      }
      else {
        ss << fmt::format(R"("{target_name}" "{delay:.{decimals_count}f}"
)",
                          fmt::arg("decimals_count", DecimalsCount),
                          fmt::arg("target_name", propName),
                          fmt::arg("delay", std::stod(propValue)));
      }

      ++propIndex;
    }

    if (propIndex - 1 % MaxTargetsPerMM != 0) {
      ss << "}\n";
    }

    return ss.str();
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &MultiManager::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"spawnflags",
                   0,
                   "Spawnflags",
                   OwnProperty::Type::Flags,
                   {
                       {1, "multithreaded", 0},
                   }},
  };
  return props;
}

REGISTER_PLUGIN(MultiManager)
