#include "MultiManager.hpp"

#include <PluginDiscovery.hpp>
#include <PluginEntityFactoryImpl.hpp>
#include <SharedLibraryUtils.hpp>
#include <Utils.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

class MultiManagerFixture {
public:
  MultiManagerFixture()
  {
    const auto discoveredPluginsPaths = discover_plugins(REMEC_BINARY_DIR);

    m_entityFactory = std::make_unique<PluginEntityFactoryImpl>(
        "", discoveredPluginsPaths, "");

    auto lib = m_entityFactory->getLibraryForClassName(MultiManager::ClassName);

    newInstanceFunc = get_library_function_T<newInstance_t>(lib, newInstance_s);
    deleteInstanceFunc =
        get_library_function_T<deleteInstance_t>(lib, deleteInstance_s);
  }

  std::unique_ptr<PluginEntityFactoryImpl> m_entityFactory;
  newInstance_t newInstanceFunc       = nullptr;
  deleteInstance_t deleteInstanceFunc = nullptr;
};

TEST_CASE_METHOD(MultiManagerFixture, "TestMultiManager")
{
  SECTION("getters")
  {
    auto ars = newInstanceFunc(Context {},
                               0,
                               1,
                               {{"classname", MultiManager::ClassName},
                                {"targetname", "mymm"},
                                {"origin", "1 1 1"}},
                               {});

    REQUIRE(ars->getEntityClassName() == std::string(MultiManager::ClassName));
    REQUIRE(ars->getName() == "mymm");
    REQUIRE(ars->getOriginalStartPos() == 0);
    REQUIRE(ars->getOriginalLen() == 1);
    REQUIRE(ars->getProperties().size() == 3);
    REQUIRE(ars->getBrushes().size() == 0);

    deleteInstanceFunc(ars);
  }

  SECTION("serialize")
  {
    auto ars = newInstanceFunc(Context {},
                               0,
                               1,
                               {
                                   {"targetname", "mymm"},
                                   {"classname", MultiManager::ClassName},
                                   {"origin", "1 1 1"},
                                   {"target1", "0"},
                                   {"target2", "1"},
                                   {"target3", "2"},
                                   {"target4", "2"},
                                   {"target5", "2"},
                                   {"target6", "2"},
                                   {"target7", "2"},
                                   {"target8", "2"},
                                   {"target9", "2"},
                                   {"target10", "2"},
                               },
                               {});

    const std::string expected = R"(
{
"classname" "multi_manager"
"targetname" "mymm"
"origin" "1 1 1"
"spawnflags" "0"
"target1" "0.000000"
"target2" "1.000000"
"target3" "2.000000"
"target4" "2.000000"
"target5" "2.000000"
"target6" "2.000000"
"target7" "2.000000"
"mymm1" "0"
}

{
"classname" "multi_manager"
"targetname" "mymm1"
"origin" "1 1 1"
"spawnflags" "0"
"target8" "2.000000"
"target9" "2.000000"
"target10" "2.000000"
}
)";

    const auto outStr        = ars->serialize().value();
    const auto output        = split(outStr, '\n');
    const auto expectedSplit = split(expected, '\n');

    REQUIRE(output.size() == expectedSplit.size());

    for (auto i = 0u; i != output.size(); ++i) {
      REQUIRE(output[i] == expectedSplit[i]);
    }

    deleteInstanceFunc(ars);
  }
}
