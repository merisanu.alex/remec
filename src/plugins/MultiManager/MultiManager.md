# MultiManager

Trigger multiple targets with or without delay.

Normal `multi_manager` has a **maximum limit of 8** whereas this entity has **no maximum limit**!

## Properties

| Property     | Default value | Description               | Possible values                 |
| ------------ | ------------- | ------------------------- | ------------------------------- |
| `targetname` |               | Name                      |                                 |
| `spawnflags` | `0`           | Spawn flags               | <ul><li>Multithreaded</li></ul> |
| `target1`    |               | Value is delay in seconds |                                 |
| `target2`    |               | ...                       |                                 |
| `target3`    |               | ...                       |                                 |
| ...          |               | ...                       |                                 |

## Generates

```mermaid
graph LR;

linkStyle default fill:none,stroke:orange;

T1["target1"]
T2["target2"]
T3["target3"]
T4["target4"]
T5["target5"]
T6["target6"]
T7["target7"]
T8["target8"]
T9["target9"]
TN["targetN"]

TGR["external trigger"]

subgraph remec_multi_manager
  MM1["multi_manager"]
  style MM1 stroke:orange,color:orange;
  MM2["multi_manager"]
  style MM2 stroke:orange,color:orange;
end

TGR --> MM1
linkStyle 0 reset;

MM1 --delay target 1--> T1
MM1 --delay target 2--> T2
MM1 --delay target 3--> T3
MM1 --delay target 4--> T4
MM1 --delay target 5--> T5
MM1 --delay target 6--> T6
MM1 --delay target 7--> T7
MM1 --> MM2

MM2 --delay target 8--> T8
MM2 --delay target 9--> T9
MM2 --delay target N--> TN

```
