get_filename_component(TARGET_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)

add_library(${TARGET_NAME}
            SHARED
            ${TARGET_NAME}.cpp)

set_property(
    TARGET ${TARGET_NAME}
    PROPERTY LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")

target_link_libraries(
    ${TARGET_NAME}
    PUBLIC
    fmt::fmt
    ${CMAKE_PROJECT_NAME}_lib
)

target_include_directories(
    ${TARGET_NAME}
    PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
)

if (WIN32)
    add_custom_command(
        OUTPUT "${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/${TARGET_NAME}.dll"
        DEPENDS ${TARGET_NAME}
        COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_BINARY_DIR}/src/plugins/${TARGET_NAME}/${CMAKE_BUILD_TYPE}/${TARGET_NAME}.dll" "${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/${TARGET_NAME}.dll"
    )

    add_custom_target(${TARGET_NAME}_cp ALL DEPENDS "${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/${TARGET_NAME}.dll")
endif()

if (ENABLE_TESTING)
    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/test/unit)
        FetchContent_Declare(catch2
            GIT_REPOSITORY https://github.com/catchorg/Catch2
            GIT_TAG v3.3.2
        )

        FetchContent_MakeAvailable(catch2)

        set(TEST_TARGET_NAME Test${TARGET_NAME})

        add_executable( ${TEST_TARGET_NAME}
                        test/unit/${TEST_TARGET_NAME}.cpp)

        target_link_libraries(  ${TEST_TARGET_NAME}
                                PRIVATE
                                Catch2::Catch2
                                ${TARGET_NAME})

        if (UNIX)
            target_compile_definitions(${TEST_TARGET_NAME} PRIVATE -DREMEC_BINARY_DIR="${CMAKE_BINARY_DIR}")
        else()
            target_compile_definitions(${TEST_TARGET_NAME} PRIVATE -DREMEC_BINARY_DIR="${CMAKE_BINARY_DIR}/src/plugins/${TARGET_NAME}/${CMAKE_BUILD_TYPE}")
        endif()

        add_test(NAME ${TEST_TARGET_NAME} COMMAND ${TEST_TARGET_NAME})
    endif()
endif()
