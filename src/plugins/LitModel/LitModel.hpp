#pragma once

#include <common/CustomEntity.hpp>

class LitModel final : public CustomEntity<LitModel> {
public:
  using CustomEntity<LitModel>::CustomEntity;

  static constexpr inline auto ClassName = "remec_lit_model";
  static constexpr inline auto Version   = "1.0.0";
  static constexpr inline auto Description =
      "cycler_sprite with light and more";
  static constexpr inline auto TargetNameMandatory = false;
  static constexpr inline auto Solid               = false;
  static constexpr inline auto Studio              = true;

  tl::expected<std::string, std::string> serialize() const override;

  static const std::vector<OwnProperty> &GetOwnProperties() noexcept;
};
