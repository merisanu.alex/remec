#include "AutoResetWall.hpp"

#include <Utils.hpp>
#include <fmt/format.h>

tl::expected<std::string, std::string> AutoResetWall::serialize() const
{
  try {
    const auto brushes       = getBrushes();
    const auto fwtProperties = getFilteredProperties();

    return fmt::format(
        R"(
{{
{layer}"classname" "func_wall_toggle"
"targetname" "{original_name}_fwt"{fwt_properties}
{wall_brushes}
}}

// {original_name}_mm triggers {original_name}_fwt and {original_name}_mm2 after {delay:.{decimals_count}f} second(s)
{{
{layer}"classname" "multi_manager"
"origin" "{origin}"
"targetname" "{original_name}"
"{original_name}_fwt" "{delay:.{decimals_count}f}"
"{original_name}_mm2" "{delay:.{decimals_count}f}"
"spawnflags" "1"
}}

// {original_name}_mm2 triggers {original_name}_fwt after {delay2:.{decimals_count}f} second(s)
{{
{layer}"classname" "multi_manager"
"origin" "{origin}"
"targetname" "{original_name}_mm2"
"{original_name}_fwt" "{delay2:.{decimals_count}f}"
"spawnflags" "1"
}}
)",
        fmt::arg("original_name", getName()),
        fmt::arg("layer", getLayer()),
        fmt::arg("decimals_count", DecimalsCount),
        fmt::arg("wall_brushes", brushes_to_str(brushes)),
        fmt::arg("fwt_properties", properties_map_to_str(fwtProperties)),
        fmt::arg("origin", get_origin(brushes)),
        fmt::arg("delay", getOrDefault<double>("delay")),
        fmt::arg("delay2", getOrDefault<double>("delay2")));
  }
  catch (std::exception &ex) {
    return tl::make_unexpected(ex.what());
  }
  catch (...) {
    return tl::make_unexpected("unknown error");
  }
}

const std::vector<OwnProperty> &AutoResetWall::GetOwnProperties() noexcept
{
  static const std::vector<OwnProperty> props {
      OwnProperty {
          "targetname", "", "Name", OwnProperty::Type::TargetSource, {}},
      OwnProperty {"delay",
                   0.0,
                   "Delay before first trigger",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"delay2",
                   1.0,
                   "Delay before reset trigger",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"spawnflags",
                   0,
                   "Spawn flags",
                   OwnProperty::Type::Flags,
                   {
                       {1, "Starts invisible", 0},
                       {2048, "Not in Deathmatch", 0},
                   }},
      OwnProperty {"globalname",
                   "",
                   "Global Entity Name",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"_minlight",
                   "",
                   "Minimum light level",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"renderfx",
                   0,
                   "Render FX",
                   OwnProperty::Type::Choices,
                   {
                       {0, "Normal"},
                       {1, "Slow Pulse"},
                       {2, "Fast Pulse"},
                       {3, "Slow Wide Pulse"},
                       {4, "Fast Wide Pulse"},
                       {5, "Slow Fade Away"},
                       {6, "Fast Fade Away"},
                       {7, "Slow Become Solid"},
                       {8, "Fast Become Solid"},
                       {9, "Slow Strobe"},
                       {10, "Fast Strobe"},
                       {11, "Faster Strobe"},
                       {12, "Slow Flicker"},
                       {13, "Fast Flicker"},
                       {14, "Constant Glow"},
                       {15, "Distort"},
                       {16, "Hologram (Distort + fade)"},
                   }},

      OwnProperty {"rendermode",
                   0,
                   "Render Mode",
                   OwnProperty::Type::Choices,
                   {
                       {0, "Normal"},
                       {1, "Color"},
                       {2, "Texture"},
                       {3, "Glow"},
                       {4, "Solid"},
                       {5, "Additive"},
                   }},
      OwnProperty {"renderamt",
                   "",
                   "FX Amount (1 - 255)",
                   OwnProperty::Type::String,
                   {}},
      OwnProperty {"rendercolor",
                   "0 0 0",
                   "FX Color (R G B)",
                   OwnProperty::Type::Color255,
                   {}},
      OwnProperty {
          "zhlt_lightflags",
          0,
          "ZHLT Lightflags",
          OwnProperty::Type::Choices,
          {
              {0, "Default"},
              {1, "Embedded Fix"},
              {2, "Opaque (blocks light)"},
              {3, "Opaque + Embedded fix"},
              {6, "Opaque + Concave Fix"},
          },
      },
      OwnProperty {"light_origin",
                   "",
                   "Light Origin Target",
                   OwnProperty::Type::TargetDestination,
                   {}},
  };

  return props;
}

REGISTER_PLUGIN(AutoResetWall)
