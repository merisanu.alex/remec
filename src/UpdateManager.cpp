#include "UpdateManager.hpp"

#include "PluginDiscovery.hpp"
#include "SharedLibraryUtils.hpp"
#include "Utils.hpp"
#include "common/IPlugin.hpp"
#include "common/LogWrapper.hpp"
#include "tracing/ITracer.hpp"
#include "util/Version.hpp"

#include <filesystem>
#include <fmt/format.h>
#include <fstream>

#ifdef REMEC_AUTO_PLUGIN_UPDATE
#ifndef CPPHTTPLIB_OPENSSL_SUPPORT
#define CPPHTTPLIB_OPENSSL_SUPPORT
#endif

#include <elzip/elzip.hpp>
#endif

#ifdef _WIN32
#include <httplib.h>
#ifdef REMEC_AUTO_PLUGIN_UPDATE
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "advapi32.lib")
#pragma comment(lib, "crypt32.lib")
#pragma comment(lib, "user32.lib")
#endif
#else
#include <httplib.h>
#endif

#include <nlohmann/json.hpp>

#ifndef REMEC_AUTO_PLUGIN_UPDATE
std::vector<std::filesystem::path>
updateIfNecessary(const std::vector<std::filesystem::path> &pluginsPaths,
                  const std::filesystem::path &)
{
  return pluginsPaths;
}
#else
namespace
{
auto getPluginsVersions(
    const std::vector<std::filesystem::path> &pluginsPaths) noexcept
{
  REMEC_TRACE(Category::remec_update_manager, "getPluginsVersions");

  struct Data
  {
    Version version;
    std::filesystem::path path;
  };

  std::unordered_map<std::string, Data> results;

  for (const auto &pluginLibPath : pluginsPaths) {
    auto lib = open_library(pluginLibPath);

    auto classNameF = get_library_function_T<className_t>(lib, className_s);
    auto versionF   = get_library_function_T<version_t>(lib, version_s);

    results[classNameF()] = {version_from_str(versionF()), pluginLibPath};

    close_library(lib);
  }

  return results;
}

auto deleteOldPlugins(
    const std::vector<std::filesystem::path> &pluginsPaths) noexcept
{
  for (const auto &oldPluginPath : pluginsPaths) {
    std::filesystem::remove(oldPluginPath);
  }
}
} // namespace

std::vector<std::filesystem::path>
updateIfNecessary(const std::vector<std::filesystem::path> &pluginsPaths,
                  const std::filesystem::path &pluginsDirPath)
{
  REMEC_TRACE(Category::remec_update_manager, "updateIfNecessary");

  static constexpr auto RemecReleasesEndpoint =
      "/api/v4/projects/37711736/releases";

  REMEC_TRACE_BEGIN(Category::remec_update_manager, "GET releases");

  httplib::Client httpClient {"https://gitlab.com"};

  const auto response = httpClient.Get(RemecReleasesEndpoint);

  if (not response) {
    logging(Level::Error, "No response from gitlab");

    return pluginsPaths;
  }

  if (response->status != 200) {
    logging(Level::Error,
            "Gitlab response: {} ({})",
            asColor<Color::red>(response->status),
            asColor<Color::red>(response->reason));

    return pluginsPaths;
  }

  REMEC_TRACE_END(Category::remec_update_manager);

  REMEC_TRACE_BEGIN(Category::remec_update_manager, "Parse response");

  const auto bodyJson = nlohmann::json::parse(response->body);

  if (bodyJson.is_null() or not bodyJson.is_array()) {
    logging(Level::Error, "No response from gitlab");

    return pluginsPaths;
  }

  const auto &latestRelease = bodyJson.front();
  const auto description    = latestRelease["description"].get<std::string>();

  // Each release should have this at the end of the release notes
  //
  //   #### Plugin versions
  //   remec_auto_reset_switch:1.0.0
  //   remec_auto_reset_wall:1.0.0
  //   remec_delayed_teleport:1.0.0
  //   remec_hp_booster:1.0.0
  //   remec_multi_bhop_block:1.0.0
  //   remec_multi_manager:1.0.0
  //   remec_multi_target_teleport:1.0.0
  //   remec_n_trigger:1.0.0
  //   remec_sky_light:1.0.0
  //   remec_slip_ramp:1.0.0
  //   remec_trigger_random_target:1.0.0

  const auto pluginVersionsStart = description.find("#### Plugin versions");

  if (pluginVersionsStart == std::string::npos) {
    const auto name = latestRelease["name"].get<std::string>();

    logging(Level::Debug, "Ignoring release '{}'", asColor<Color::blue>(name));

    REMEC_TRACE_END(Category::remec_update_manager);

    return pluginsPaths;
  }

  const auto pluginsVersionsStr = description.substr(pluginVersionsStart + 20);
  const auto pluginsVersions    = split(pluginsVersionsStr, '\n');

  REMEC_TRACE_END(Category::remec_update_manager);

  const auto existingPluginsVersions = getPluginsVersions(pluginsPaths);

  const auto tmpDirPath = pluginsDirPath / "tmp";

  std::filesystem::create_directory(tmpDirPath);

  try {
    REMEC_TRACE_BEGIN(Category::remec_update_manager, "Download new release");

    const auto newReleaseZipUrl =
        latestRelease["assets"]["links"].front()["url"].get<std::string>();

    const auto adjustedUrl = replace_all(
        newReleaseZipUrl.substr(newReleaseZipUrl.find(".com") + 4), "//", "/");

    const auto adjustedUrlResponse = httpClient.Get(adjustedUrl);

    if (not adjustedUrlResponse) {
      throw std::runtime_error {"No response for asset link"};
    }

    if (adjustedUrlResponse->status != 200) {
      throw std::runtime_error {
          fmt::format("Invalid response for asset link '{}':'{}'",
                      adjustedUrlResponse->status,
                      adjustedUrlResponse->reason)};
    }

    const auto zipPath = tmpDirPath / "remec.zip";

    (std::ofstream {zipPath, std::ios::binary} << adjustedUrlResponse->body);

    REMEC_TRACE_END(Category::remec_update_manager);
    REMEC_TRACE_BEGIN(Category::remec_update_manager, "Extract zip");

    elz::extractZip(zipPath, tmpDirPath);

    REMEC_TRACE_END(Category::remec_update_manager);

    REMEC_TRACE(Category::remec_update_manager, "Update plugins");

    deleteOldPlugins(pluginsPaths);

    const auto newPluginsPaths    = discover_plugins(tmpDirPath);
    const auto newPluginsVersions = getPluginsVersions(newPluginsPaths);

    std::vector<std::filesystem::path> newPluginPaths;

    for (const auto &[className, data] : newPluginsVersions) {
      (void)className;

      const auto newPluginPath =
          pluginsDirPath / fmt::format("{}.{}{}",
                                       data.path.filename().stem().string(),
                                       version_to_str(data.version),
                                       SharedLibraryExtension());

      newPluginPaths.push_back(newPluginPath);

      std::filesystem::copy_file(data.path, newPluginPath);
    }

    // update fgd
    std::filesystem::copy_file(
        tmpDirPath / "remec.fgd",
        pluginsDirPath / "remec.fgd",
        std::filesystem::copy_options::overwrite_existing);

    std::filesystem::remove_all(tmpDirPath);

    return newPluginPaths;
  } catch (const std::exception &ex) {
    std::filesystem::remove_all(tmpDirPath);

    throw std::runtime_error {
        fmt::format("updateIfNecessary: failed with error '{}'", ex.what())};
  } catch (...) {
    std::filesystem::remove_all(tmpDirPath);

    throw std::runtime_error {"updateIfNecessary: failed with unknown error"};
  }
}
#endif
