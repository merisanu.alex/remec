#pragma once

#include <memory>
#include <tracing/ITracer.hpp>

namespace perfetto
{
class TracingSession;
}

class Tracer final : public ITracer
{
public:
  Tracer();
  ~Tracer();

  void beginTrace(Category category,
                  std::string_view name,
                  const std::optional<std::uint64_t> &id,
                  const std::unordered_map<std::string, std::string> &params)
      const override;

  void endTrace(Category category) const override;

private:
  std::FILE *m_fd = nullptr;
  std::unique_ptr<perfetto::TracingSession> m_session;
};
