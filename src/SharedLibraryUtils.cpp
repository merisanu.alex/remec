#include "SharedLibraryUtils.hpp"

#include <fmt/format.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif

void *open_library(const std::filesystem::path &path)
{
#ifdef _WIN32
  auto lib = LoadLibraryW(path.c_str());

  if (not lib) {
    throw std::runtime_error {fmt::format(
        "Failed loading plugin '{}', error: '{}'", path.string(), "unknown")};
  }

  return reinterpret_cast<void *>(lib);
#else
  void *lib = dlopen(path.c_str(), RTLD_LAZY);

  if (not lib) {
    const auto dlErr = dlerror();

    throw std::runtime_error {
        fmt::format("Failed loading plugin '{}', error: '{}'",
                    path.string(),
                    dlErr ? dlErr : "unknown")};
  }

  return lib;
#endif
}

void close_library(void *lib)
{
#ifdef _WIN32
  FreeLibrary(reinterpret_cast<HINSTANCE>(lib));
#else
  dlclose(lib);
#endif
}

void *get_library_function(void *lib, std::string_view functionName)
{
#ifdef _WIN32
  auto libInstance = reinterpret_cast<HINSTANCE>(lib);

  auto result = GetProcAddress(libInstance, functionName.data());

  if (not result) {
    close_library(lib);

    throw std::runtime_error {
        fmt::format("Failed getting plugin function '{}', error: '{}'",
                    functionName,
                    "unknown")};
  }

  return result;
#else
  auto result = dlsym(lib, functionName.data());

  if (not result) {
    const auto dlErr = dlerror();
    const auto errMsg =
        fmt::format("Failed getting plugin function '{}', error: '{}'",
                    functionName,
                    dlErr ? dlErr : "unknown");

    close_library(lib);

    throw std::runtime_error {errMsg};
  }

  return result;
#endif
}
