#include <Utils.hpp>
#include <fmt/format.h>
#include <util/Version.hpp>

Version version_from_str(std::string_view sv)
{
  const auto bits = split(sv, '.');

  if (bits.size() != 3) {
    throw std::invalid_argument {fmt::format("Cannot parse version '{}'", sv)};
  }

  return {std::stoi(bits[0]), std::stoi(bits[1]), std::stoi(bits[2])};
}

std::string version_to_str(const Version &ver) noexcept
{
  return fmt::format("{}.{}.{}", ver.major, ver.minor, ver.patch);
}

bool Version::operator==(const Version &rhs) const noexcept
{
  return major == rhs.major and minor == rhs.minor and patch == rhs.patch;
}

bool Version::operator>(const Version &rhs) const noexcept
{
  if (major > rhs.major) {
    return true;
  }

  if (major < rhs.major) {
    return false;
  }

  if (minor > rhs.minor) {
    return true;
  }

  if (minor < rhs.minor) {
    return false;
  }

  if (patch > rhs.patch) {
    return true;
  }

  return false;
}

bool Version::operator<(const Version &rhs) const noexcept
{
  return rhs > *this;
}
