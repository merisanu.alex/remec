#include "FGDGenerator.hpp"

#include "SharedLibraryUtils.hpp"
#include "Utils.hpp"

#include <common/IPlugin.hpp>
#include <sstream>

std::string FGDGenerator::generate(
    const std::vector<std::filesystem::path> &pluginsPaths) const noexcept
{
  std::stringstream ss;

  for (const auto &pluginLibPath : pluginsPaths) {
    auto lib = open_library(pluginLibPath);

    auto classNameF = get_library_function_T<className_t>(lib, className_s);
    auto descrF     = get_library_function_T<description_t>(lib, description_s);
    auto solidF     = get_library_function_T<solid_t>(lib, solid_s);
    auto studioF    = get_library_function_T<studio_t>(lib, studio_s);
    auto ownPropsF =
        get_library_function_T<ownProperties_t>(lib, ownProperties_s);

    if (const auto str = own_properties_to_str(
            *ownPropsF(), classNameF(), descrF(), solidF(), studioF());
        not str.empty()) {
      ss << str << std::endl;
    }

    close_library(lib);
  }

  return ss.str();
}
