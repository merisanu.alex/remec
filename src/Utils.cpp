#include "Utils.hpp"

#include "IEntityFactory.hpp"
#include "tracing/ITracer.hpp"

#include <algorithm>
#include <common/Entity.hpp>
#include <common/Face.hpp>
#include <fmt/format.h>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <regex>
#include <sstream>
#include <tga.h>

std::string read_whole_file(const std::filesystem::path &path)
{
  std::stringstream ss;
  ss << std::ifstream {path}.rdbuf();

  return ss.str();
}

std::string erase_comments(std::string_view fileContent)
{
  std::string trimmedFileStr {fileContent};
  std::size_t offset           = 0u;
  std::size_t numOfApostrophes = 0u;

  while (1) {
    const std::size_t commentStart = trimmedFileStr.find_first_of('/', offset);

    if (commentStart == std::string::npos)
      break;

    numOfApostrophes += std::count(
        trimmedFileStr.begin() + offset, trimmedFileStr.begin() + commentStart, '\"');
    if (numOfApostrophes % 2 == 1) {
      offset = commentStart + 1;
      continue;
    }

    std::size_t commentEnd =
        trimmedFileStr.find_first_of('\n', commentStart);
    if (commentEnd == std::string::npos) {
      commentEnd = trimmedFileStr.length();
    }

    for (auto i = commentStart; i < commentEnd; ++i) {
      trimmedFileStr[i] = ' ';
    }

    offset = commentEnd;
  }

  return trimmedFileStr;
}

std::vector<Brush>
replace_brushes_textures(std::vector<Brush> brushes,
                         std::string_view newTexture) noexcept
{
  for (auto &brush : brushes) {
    for (auto &face : brush) {
      face.texture = newTexture;
    }
  }

  return brushes;
}

Brush replace_brush_textures(Brush brush, std::string_view newTexture) noexcept
{
  for (auto &face : brush) {
    face.texture = newTexture;
  }

  return brush;
}

std::vector<Brush> brushes_from_str(std::string_view str)
{
  if (str.empty()) {
    return {};
  }

  std::vector<Brush> results;
  std::size_t offset = 0u;

  while (1) {
    const auto brushStartPos = str.find('{', offset);
    const auto noMoreBrushes = brushStartPos == std::string::npos;

    if (noMoreBrushes) {
      break;
    }

    std::size_t brushEndPos = brushStartPos;
    std::size_t testCharOffset;
    do {
      brushEndPos    = str.find_first_of('}', brushEndPos + 1);
      testCharOffset = str.find_first_of("\n\r[", brushEndPos);
    } while (testCharOffset != std::string::npos and str[testCharOffset] == '[');

    if (brushEndPos == std::string::npos) {
      throw std::runtime_error {"Invalid brush !"};
    }

    std::istringstream ss {
        str.substr(brushStartPos, brushEndPos - brushStartPos).data(),
        std::ios::binary};

    Brush brush;
    char dummy;
    ss >> dummy;

    while (dummy != '}') {
      Face face;

      ss >> dummy >> face.point1.x >> face.point1.y >> face.point1.z >> dummy >>
          dummy >> face.point2.x >> face.point2.y >> face.point2.z >> dummy >>
          dummy >> face.point3.x >> face.point3.y >> face.point3.z >> dummy >>
          face.texture >> dummy >> face.textureAxis1.x >> face.textureAxis1.y >>
          face.textureAxis1.z >> face.textureAxis1Offset >> dummy >> dummy >>
          face.textureAxis2.x >> face.textureAxis2.y >> face.textureAxis2.z >>
          face.textureAxis2Offset >> dummy >> face.textureRotation >>
          face.textureScaleX >> face.textureScaleY;

      if (face.texture.empty()) {
        break;
      }

      brush.push_back(face);
    }

    results.push_back(brush);
    offset = brushEndPos;
  }

  return results;
}

std::string brush_to_str(const Brush &brush)
{
  std::ostringstream ss;

  ss << '{' << std::endl;

  for (const auto &face : brush) {
    if (face.texture.empty()) {
      throw std::runtime_error {"Face has no texture !"};
    }

    ss << fmt::format(
              "( {x1:.{decimals_count}f} {y1:.{decimals_count}f} "
              "{z1:.{decimals_count}f} ) ( {x2:.{decimals_count}f} "
              "{y2:.{decimals_count}f} "
              "{z2:.{decimals_count}f} ) ( {x3:.{decimals_count}f} "
              "{y3:.{decimals_count}f} {z3:.{decimals_count}f} ) "
              "{texture_name} [ {tx1:.{decimals_count}f} "
              "{ty1:.{decimals_count}f} {tz1:.{decimals_count}f} "
              "{toffs1:.{decimals_count}f} ] [ {tx2:.{decimals_count}f} "
              "{ty2:.{decimals_count}f} "
              "{tz2:.{decimals_count}f} {toffs2:.{decimals_count}f} ] "
              "{rotation:.{decimals_count}f} {scaleX:.{decimals_count}f} "
              "{scaleY:.{decimals_count}f}",
              fmt::arg("decimals_count", DecimalsCount),
              fmt::arg("x1", face.point1.x),
              fmt::arg("y1", face.point1.y),
              fmt::arg("z1", face.point1.z),
              fmt::arg("x2", face.point2.x),
              fmt::arg("y2", face.point2.y),
              fmt::arg("z2", face.point2.z),
              fmt::arg("x3", face.point3.x),
              fmt::arg("y3", face.point3.y),
              fmt::arg("z3", face.point3.z),
              fmt::arg("texture_name", face.texture),
              fmt::arg("tx1", face.textureAxis1.x),
              fmt::arg("ty1", face.textureAxis1.y),
              fmt::arg("tz1", face.textureAxis1.z),
              fmt::arg("toffs1", face.textureAxis1Offset),
              fmt::arg("tx2", face.textureAxis2.x),
              fmt::arg("ty2", face.textureAxis2.y),
              fmt::arg("tz2", face.textureAxis2.z),
              fmt::arg("toffs2", face.textureAxis2Offset),
              fmt::arg("rotation", face.textureRotation),
              fmt::arg("scaleX", face.textureScaleX),
              fmt::arg("scaleY", face.textureScaleY))
       << std::endl;
  }

  ss << '}';

  return ss.str();
}

std::string brushes_to_str(const std::vector<Brush> &brushes)
{
  std::ostringstream ss;

  for (const auto &brush : brushes) {
    ss << brush_to_str(brush) << std::endl;
  }

  return ss.str();
}

std::string get_origin(const std::vector<Brush> &brushes)
{
  if (brushes.empty()) {
    throw std::runtime_error {"Failed extracting origin from empty brushes!"};
  }

  const auto &firstBrush = brushes[0];
  std::set<Point3D> intersectPoints;

  for (const Face &face1 : firstBrush) {
    for (const Face &face2 : firstBrush) {
      if (face1 == face2) {
        continue;
      }

      for (const Face &face3 : firstBrush) {
        if (face1 == face3 || face2 == face3) {
          continue;
        }

        auto intersection = get_intersection_point(face1, face2, face3);
        if (intersection != std::nullopt) {
          intersectPoints.insert(*intersection);
        }
      }
    }
  }

  const auto erase_outside_point = [&intersectPoints, &firstBrush](auto &it) {
    for (const Face &face : firstBrush) {
      if (get_point_position_on_face(*it, face) == PointPosition::BelowFace) {
        it = intersectPoints.erase(it); // erase returns iterator to the next element
        return;
      }
    }

    ++it;
  };

  for (auto it = intersectPoints.begin(); it != intersectPoints.end();) {
    erase_outside_point(it);
  }

  std::pair<double, double> minMaxX =
      find_min_max_coordinate(intersectPoints, Coordinate::X);
  std::pair<double, double> minMaxY =
      find_min_max_coordinate(intersectPoints, Coordinate::Y);
  std::pair<double, double> minMaxZ =
      find_min_max_coordinate(intersectPoints, Coordinate::Z);

  Point3D origin = {(minMaxX.first + minMaxX.second) / 2,
                    (minMaxY.first + minMaxY.second) / 2,
                    (minMaxZ.first + minMaxZ.second) / 2};
  return origin_to_str(origin);
}

std::string origin_to_str(const Point3D &originPoint) noexcept
{
  return fmt::format(
      "{:.{decimals_count}f} {:.{decimals_count}f} {:.{decimals_count}f}",
      originPoint.x,
      originPoint.y,
      originPoint.z,
      fmt::arg("decimals_count", DecimalsCount));
}

Point3D origin_from_str(std::string_view str)
{
  if (str.empty()) {
    throw std::runtime_error {"Failed converting string to origin point"};
  }

  std::stringstream ss {str.data()};
  Point3D result;
  ss >> result.x >> result.y >> result.z;

  return result;
}

std::string properties_map_to_str(const PropertiesMap &properties) noexcept
{
  const auto sortedKeys = sort(map(properties,
                                   [](const auto &kv) {
                                     return kv.first;
                                   }),
                               SizeAwareLexicographicCompare);

  return std::accumulate(std::begin(sortedKeys),
                         std::end(sortedKeys),
                         std::string(),
                         [&properties](const auto soFar, const auto &key) {
                           return soFar + '\n' +
                                  fmt::format(
                                      R"("{}" "{}")", key, properties.at(key));
                         });
}

namespace
{
std::string variant_to_str(const decltype(OwnProperty::defaultValue) &value)
{
  if (const auto strValue = std::get_if<std::string>(&value)) {
    return *strValue;
  }

  if (const auto intValue = std::get_if<int>(&value)) {
    return std::to_string(*intValue);
  }

  if (const auto doubleValue = std::get_if<double>(&value)) {
    return std::to_string(*doubleValue);
  }

  return {};
}
} // namespace

std::string own_properties_to_str(const std::vector<OwnProperty> &ownProperties,
                                  std::string_view className,
                                  std::string_view description,
                                  const bool isSolid,
                                  const bool isStudio) noexcept
{
  std::stringstream ss;

  for (const auto &ownProperty : ownProperties) {
    const auto strDefaultValue = variant_to_str(ownProperty.defaultValue);

    const auto defaultValue = (ownProperty.type == OwnProperty::Type::Choices or
                               ownProperty.type == OwnProperty::Type::Integer)
                                  ? ": " + strDefaultValue
                              : ownProperty.type == OwnProperty::Type::Flags
                                  ? "= "
                                  : (": \"" + strDefaultValue + "\"");

    const auto propDescription = ownProperty.type == OwnProperty::Type::Flags
                                     ? ""
                                     : " : \"" + ownProperty.description + "\"";

    ss << "\n\t"
       << fmt::format(R"({name}({type}){description} {default})",
                      fmt::arg("name", ownProperty.name),
                      fmt::arg("type", property_type_to_str(ownProperty.type)),
                      fmt::arg("description", propDescription),
                      fmt::arg("default", defaultValue));

    if (ownProperty.type == OwnProperty::Type::Choices) {
      std::string choices;

      for (const auto &[index, label, _] : ownProperty.values) {
        (void)_;

        choices += fmt::format("\n\t\t{} : \"{}\"", index, label);
      }

      ss << fmt::format(" =\n\t[{}\n\t]", choices);
    } else if (ownProperty.type == OwnProperty::Type::Flags) {
      std::string flags = "\n\t[";

      for (const auto &[index, label, propDefaultValue] : ownProperty.values) {
        flags += fmt::format(
            "\n\t\t{}: \"{}\" : {}", index, label, propDefaultValue);
      }

      ss << flags << "\n\t]";
    }
  }

  const auto propertiesStr = ss.str();

  return fmt::format(
      R"(@{entity_type}Class {entity_studio}= {entity_name} : "{entity_description}"
[{entity_properties}
]
)",
      fmt::arg("entity_type", isSolid ? "Solid" : "Point"),
      fmt::arg("entity_name", className),
      fmt::arg("entity_studio", isStudio ? "sprite() " : ""),
      fmt::arg("entity_description", description),
      fmt::arg("entity_properties", propertiesStr));
}

std::string get_sky_name(std::string_view str) noexcept
{
  constexpr char SkyKey[]  = R"("skyname" ")";
  constexpr auto SkyKeyLen = ArrayLen(SkyKey);

  auto startPos = str.find(SkyKey);

  if (startPos == std::string::npos) {
    return {};
  }

  startPos += SkyKeyLen - 1;

  const auto endPos = str.find('"', startPos);

  if (endPos == std::string::npos) {
    return {};
  }

  return std::string {str.substr(startPos, endPos - startPos)};
}

std::vector<IPluginSmartPtr>
get_remec_entities(std::string_view str, const IEntityFactory &entityFactory)
{
  const std::string skyName = get_sky_name(str);

  std::vector<IPluginSmartPtr> entities;
  std::size_t totalOffset = 0u;

  constexpr char RemecPrefix[] = R"("classname" "remec_)";
  const auto emplace_found_entity = [&entities, &RemecPrefix, &skyName, &entityFactory]
                                    (std::string_view str, const std::size_t startOffset, const std::size_t endOffset) {
    const std::string_view entityStr = str.substr(startOffset, endOffset - startOffset + 1);

    if (entityStr.find(RemecPrefix) == std::string::npos) {
      return;
    }

    entities.emplace_back(entityFactory.makeEntity(
        str, startOffset, endOffset - startOffset + 1, skyName));
  };

  while (1) {
    const std::size_t entityStartOffset = str.find_first_of('{', totalOffset);

    if (entityStartOffset == std::string::npos) {
      break;
    }

    std::size_t nextBracketOffset = entityStartOffset;
    std::size_t numOfApostrophes;
    do {
      nextBracketOffset = str.find_first_of("{}", nextBracketOffset + 1);

      if (nextBracketOffset == std::string::npos) {
        throw std::runtime_error("Invalid entity, bracket missing!");
      }

      numOfApostrophes  = std::count(str.begin() + entityStartOffset, str.begin() + nextBracketOffset, '\"');
    } while (numOfApostrophes % 2 == 1);

    if (str[nextBracketOffset] == '}') {  // entity has no solid brushes
      emplace_found_entity(str, entityStartOffset, nextBracketOffset);
      totalOffset = nextBracketOffset;

      continue;
    }

    while (1) {
      nextBracketOffset = str.find_first_of('}', nextBracketOffset + 1);

      if (nextBracketOffset == std::string::npos) {
        throw std::runtime_error("Invalid entity, bracket missing!");
      }

      const bool inTextureName = str[str.find_first_of("\n\r[", nextBracketOffset)] == '[';

      if (inTextureName) {
        continue;
      }

      nextBracketOffset = str.find_first_of("{}", nextBracketOffset + 1);

      if (nextBracketOffset == std::string::npos) {
        throw std::runtime_error("Invalid entity, bracket missing!");
      }

      if (str[nextBracketOffset] == '}') {  // no more brushes
        emplace_found_entity(str, entityStartOffset, nextBracketOffset);
        totalOffset = nextBracketOffset;

        break;
      }
    }
  }

  return entities;
}

std::string replace_all(const std::string &str,
                        const std::string &find,
                        const std::string &replace) noexcept
{
  std::string result;
  std::size_t find_len = find.size();
  std::size_t pos, from = 0;

  while (std::string::npos != (pos = str.find(find, from))) {
    result.append(str, from, pos - from);
    result.append(replace);
    from = pos + find_len;
  }

  result.append(str, from, std::string::npos);

  return result;
}

std::vector<std::string> split(const std::string &str, char delim)
{
  std::vector<std::string> result;
  std::stringstream ss {str};
  std::string item;

  while (std::getline(ss, item, delim)) {
    result.push_back(item);
  }

  return result;
}

std::vector<std::string> split(std::string_view str, char delim)
{
  std::vector<std::string> result;
  std::stringstream ss;
  ss << str;
  std::string item;

  while (std::getline(ss, item, delim)) {
    result.push_back(item);
  }

  return result;
}

std::string ltrim(std::string s) noexcept
{
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(ch);
          }));

  return s;
}

std::string property_type_to_str(const OwnProperty::Type type) noexcept
{
  switch (type) {
  case OwnProperty::Type::Integer:
    return "integer";

  case OwnProperty::Type::String:
    return "string";

  case OwnProperty::Type::Color255:
    return "color255";

  case OwnProperty::Type::Choices:
    return "choices";

  case OwnProperty::Type::Flags:
    return "flags";

  case OwnProperty::Type::TargetDestination:
    return "target_destination";

  case OwnProperty::Type::TargetSource:
    return "target_source";
  }

  return "unknown";
}

bool ends_with(std::string_view str, std::string_view withWhat)
{
  if (str.length() < withWhat.length()) {
    return false;
  }

  return str.substr(str.length() - withWhat.length()) == withWhat;
}

bool SizeAwareLexicographicCompare(std::string_view s1,
                                   std::string_view s2) noexcept
{
  if (s1.size() == s2.size()) {
    return s1 < s2;
  }

  return s1.size() < s2.size();
}

RGB get_dominant_color(const std::vector<std::filesystem::path> &images)
{
  REMEC_TRACE(Category::remec_util, "get_dominant_color");

  if (images.empty()) {
    throw std::runtime_error {"No images !"};
  }

  std::map<RGB, std::uint64_t> pixelColorsCountMap;

  for (const auto &image : images) {
    REMEC_TRACE(Category::remec_util, image.filename().string());

#ifdef _WIN32
#pragma warning(disable : 4996)
#endif
    const auto f = std::fopen(image.string().c_str(), "rb");

    if (not f) {
      throw std::runtime_error {"Cannot read file " + image.string()};
    }

    tga::StdioFileInterface file(f);
    tga::Decoder decoder(&file);
    tga::Header header;

    if (not decoder.readHeader(header)) {
      throw std::runtime_error {"Failed reading TGA header from " +
                                image.string()};
    }

    tga::Image tgaImg;
    tgaImg.bytesPerPixel = header.bytesPerPixel();
    tgaImg.rowstride     = header.width * header.bytesPerPixel();

    std::vector<std::uint8_t> buffer(tgaImg.rowstride * header.height);
    tgaImg.pixels = &buffer[0];

    if (not decoder.readImage(header, tgaImg)) {
      throw std::runtime_error {"Failed reading TGA image from " +
                                image.string()};
    }

    std::fclose(f);

    for (auto i = 0u; i < buffer.size(); i += 4) {
      // ignore alpha channel
      RGB currentPixel = {buffer[i], buffer[i + 1], buffer[i + 2]};

      auto element = pixelColorsCountMap.find(currentPixel);
      if (element == pixelColorsCountMap.end()) {
        pixelColorsCountMap.insert(
            std::pair<RGB, std::uint64_t>(currentPixel, 1));
      } else {
        element->second++;
      }
    }
  }

  const auto max = std::max_element(pixelColorsCountMap.begin(),
                                    pixelColorsCountMap.end(),
                                    [](const auto &l, const auto &r) {
                                      return l.second < r.second;
                                    });

  return max->first;
}

std::pair<PropertiesMap, std::vector<Brush>>
parse_entity(std::string_view str, const SolidMappingT &solidMapping)
{
  PropertiesMap properties;

  std::size_t pos = 0u;

  while (1) {
    auto nameStartPos = str.find_first_of("\"", pos);

    if (nameStartPos == std::string::npos) {
      break;
    }

    nameStartPos += 1;

    const auto nameEndPos = str.find_first_of("\"", nameStartPos);

    if (nameEndPos == std::string::npos) {
      throw std::runtime_error {"Invalid property name, cannot find end !"};
    }

    auto valueStartPos = str.find_first_of("\"", nameEndPos + 1);

    if (valueStartPos == std::string::npos) {
      throw std::runtime_error {"Invalid property value, cannot find start !"};
    }

    valueStartPos += 1;

    const auto valueEndPos = str.find_first_of("\"", valueStartPos);

    if (valueEndPos == std::string::npos) {
      throw std::runtime_error {"Invalid property value, cannot find end !"};
    }

    properties[std::string(
        str.substr(nameStartPos, nameEndPos - nameStartPos))] =
        str.substr(valueStartPos, valueEndPos - valueStartPos);

    pos = valueEndPos + 1u;
  }

  const auto entityHasClassName = properties.count("classname") != 0;

  if (not entityHasClassName) {
    throw std::runtime_error {"REMEC entity has no classname !"};
  }

  const auto entityClassName = properties.at("classname");

  if (not solidMapping.count(entityClassName)) {
    throw std::runtime_error {
        fmt::format("REMEC has no mapping for '{}' !", entityClassName)};
  }

  const auto entityIsSolid = solidMapping.at(entityClassName);

  std::string brushes;

  if (entityIsSolid) {
    std::size_t brushesListPosStart = str.find_first_of('{', pos);

    if (brushesListPosStart == std::string::npos) {
      throw std::runtime_error {"Invalid brushes list !"};
    }

    brushes =
        str.substr(brushesListPosStart, str.length() - brushesListPosStart - 2);
  }

  return std::make_pair(properties, brushes_from_str(brushes));
}

std::pair<double, double>
find_min_max_coordinate(const Brush &brush,
                        const Coordinate &coordinate) noexcept
{
  if (brush.empty()) {
    return {0.0, 0.0};
  }

  double min =  std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();

  const auto update_extremum = [&coordinate](const Point3D &point, double &extremum_value, const Extremum &extremum) {
    const double coordinateValue = get_point_coordinate_value(point, coordinate);
    switch (extremum) {
    case Extremum::Min:
      if (coordinateValue < extremum_value)
        extremum_value = coordinateValue;
      break;
    case Extremum::Max:
      if (coordinateValue > extremum_value)
        extremum_value = coordinateValue;
      break;
    }
  };

  for (const auto &face : brush) {
    update_extremum(face.point1, min, Extremum::Min);
    update_extremum(face.point2, min, Extremum::Min);
    update_extremum(face.point3, min, Extremum::Min);

    update_extremum(face.point1, max, Extremum::Max);
    update_extremum(face.point2, max, Extremum::Max);
    update_extremum(face.point3, max, Extremum::Max);
  }

  return {min, max};
}

std::pair<double, double>
find_min_max_coordinate(const std::vector<Brush> &brushes,
                        const Coordinate &coordinate) noexcept
{
  if (brushes.empty()) {
    return {0.0, 0.0};
  }

  double min =  std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();

  for (const auto &brush : brushes) {
    const auto [minFace, maxFace] = find_min_max_coordinate(brush, coordinate);
    if (minFace < min)
      min = minFace;
    if (maxFace > max)
      max = maxFace;
  }

  return {min, max};
}

std::pair<double, double>
find_min_max_coordinate(const std::set<Point3D> &points,
                        const Coordinate &coordinate) noexcept
{
  if (points.empty()) {
    return {0.0, 0.0};
  }

  double min =  std::numeric_limits<double>::max();
  double max = -std::numeric_limits<double>::max();

  for (const auto &point : points) {
    const auto coordinateValue = get_point_coordinate_value(point, coordinate);
    if (coordinateValue < min)
      min = coordinateValue;
    if (coordinateValue > max)
      max = coordinateValue;
  }

  return {min, max};
}

double find_min_coordinate(const Brush &brush,
                           const Coordinate &coordinate) noexcept
{
  return find_min_max_coordinate(brush, coordinate).first;
}

double find_min_coordinate(const std::vector<Brush> &brushes,
                           const Coordinate &coordinate) noexcept
{
  return find_min_max_coordinate(brushes, coordinate).first;
}

double find_max_coordinate(const Brush &brush,
                           const Coordinate &coordinate) noexcept
{
  return find_min_max_coordinate(brush, coordinate).second;
}

double find_max_coordinate(const std::vector<Brush> &brushes,
                           const Coordinate &coordinate) noexcept
{
  return find_min_max_coordinate(brushes, coordinate).second;
}

double get_point_coordinate_value(const Point3D &point,
                                  const Coordinate &coordinate)
{
  switch (coordinate) {
  case Coordinate::X:
    return point.x;
  case Coordinate::Y:
    return point.y;
  case Coordinate::Z:
    return point.z;
  default:
    throw std::runtime_error {"Invalid coordinate!"};
  }
}

#ifdef _WIN32
bool isAtty() noexcept
{
  return false;
}
#else
#include <unistd.h>

bool isAtty() noexcept
{
  return isatty(1);
}
#endif
