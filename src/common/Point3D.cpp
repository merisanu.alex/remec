#include <cmath>
#include <common/Point3D.hpp>

Point3D Point3D::operator+(const Point3D &other) const noexcept
{
  return Point3D {x + other.x, y + other.y, z + other.z};
}

Point3D Point3D::operator-(const Point3D &other) const noexcept
{
  return Point3D {x - other.x, y - other.y, z - other.z};
}

Point3D Point3D::operator*(const double scalar) noexcept
{
  return Point3D {x * scalar, y * scalar, z * scalar};
}

Point3D &Point3D::operator+=(const Point3D &other) noexcept
{
  x += other.x;
  y += other.y;
  z += other.z;

  return *this;
}

Point3D &Point3D::operator*=(const double scalar) noexcept
{
  x *= scalar;
  y *= scalar;
  z *= scalar;

  return *this;
}

Point3D &Point3D::operator/=(const double scalar) noexcept
{
  x /= scalar;
  y /= scalar;
  z /= scalar;

  return *this;
}

bool Point3D::operator==(const Point3D &other) const noexcept
{
  return (std::abs(x - other.x) < epsilon) &&
         (std::abs(y - other.y) < epsilon) &&
         (std::abs(z - other.z) < epsilon);
}

bool Point3D::operator<(const Point3D &other) const noexcept
{
  if (std::abs(x - other.x) > epsilon)
    return x < other.x;

  if (std::abs(y - other.y) > epsilon)
    return y < other.y;

  if (std::abs(z - other.z) > epsilon)
    return z < other.z;

  return false;
}

Point3D &Point3D::normalize() noexcept
{
  return *this /= get_magnitude();
}

double Point3D::get_magnitude() const noexcept
{
  return std::sqrt((std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2)));
}

Point3D Point3D::cross_product(const Point3D &point1,
                               const Point3D &point2) noexcept
{
  return Point3D {point1.y * point2.z - point1.z * point2.y,
                  point1.z * point2.x - point1.x * point2.z,
                  point1.x * point2.y - point1.y * point2.x};
}

double Point3D::dot_product(const Point3D &point1,
                            const Point3D &point2) noexcept
{
  return (point1.x * point2.x) +
         (point1.y * point2.y) +
         (point1.z * point2.z);
}
