#include <common/IPlugin.hpp>

PluginDeleter::PluginDeleter(deleteInstance_t deleterFunc)
    : m_deleterFunc {deleterFunc}
{
}

void PluginDeleter::operator()(IPlugin *plugin) const noexcept
{
  if (m_deleterFunc) {
    m_deleterFunc(plugin);
  } else {
    delete plugin;
  }
}
