#include "FGDGenerator.hpp"
#include "PluginDiscovery.hpp"
#include "PluginEntityFactoryImpl.hpp"
#include "Rewriter.hpp"
#include "UpdateManager.hpp"
#include "Utils.hpp"
#include "common/LogWrapper.hpp"
#include "tracing/ITracer.hpp"

#ifdef REMEC_ENABLE_TRACING
#include "tracing/Tracer.hpp"
#endif

#include <filesystem>
#include <fmt/format.h>
#include <fstream>
#include <iostream>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <string>

std::shared_ptr<spdlog::logger> g_remecLogger;
spdlog::logger *g_logger = nullptr;

std::unique_ptr<ITracer> g_remecTracer;
ITracer *g_tracer = nullptr;

int main(int argc, const char *argv[])
{
  g_remecLogger = spdlog::stderr_color_st("remec");

  if (argc >= 5 && argv[4] == std::string_view("debug")) {
    g_remecLogger->set_level(spdlog::level::debug);
  }
  else {
    g_remecLogger->set_level(spdlog::level::info);
  }

  g_logger = g_remecLogger.get();

#ifdef REMEC_ENABLE_TRACING
  g_remecTracer = std::make_unique<Tracer>();

  logging(Level::Debug, "Using tracer");
#endif

  g_tracer = g_remecTracer.get();

  REMEC_TRACE(Category::remec, "all");

  const auto parentPath = std::filesystem::path {argv[0]}.parent_path();

  const auto originalPluginsPaths = discover_plugins(parentPath);
  const auto pluginsPaths =
      argc == 1 ? originalPluginsPaths
                : updateIfNecessary(originalPluginsPaths, parentPath);

  if (pluginsPaths.empty()) {
    throw std::runtime_error {"No plugins found!"};
  }

  if (argc == 1) {
    const auto supportedEntitiesStr = FGDGenerator {}.generate(pluginsPaths);

    std::cout << supportedEntitiesStr << std::endl;

    return 0;
  }

  if (argv[1] == std::string_view("--update-only")) {
    return 0;
  }

  try {
    const auto inputMapPath = std::filesystem::path {argv[1]};

    if (not std::filesystem::exists(inputMapPath)) {
      throw std::invalid_argument {inputMapPath.string() + " does not exist!"};
    }

    if (not ends_with(inputMapPath.string(), ".remec.map")) {
      throw std::invalid_argument {inputMapPath.string() +
                                   " does not end in '.remec.map'!"};
    }

    if (argc < 3) {
      throw std::invalid_argument {
          "mode parameter 'tb' or 'jack' is required!"};
    }

    const auto tbOrJack = std::string(argv[2]);

    const auto halfLifePath = argc >= 4 ? argv[3] : "";

    const PluginEntityFactoryImpl entityFactory {
        halfLifePath, pluginsPaths, tbOrJack};

    const auto originalFileStr = read_whole_file(inputMapPath);
    const auto trimmedFileStr  = erase_comments(originalFileStr);
    const auto entities = get_remec_entities(trimmedFileStr, entityFactory);

    for (const auto &entity : entities) {
      logging(Level::Info,
              "Found entity '{}'{}",
              asColor<Color::green>(entity->getEntityClassName()),
              entity->isTargetNameMandatory()
                  ? fmt::format(" called '{}'",
                                asColor<Color::blue>(entity->getName()))
                  : "");
    }

    Rewriter rewriter {originalFileStr};

    const auto inFilePathNoExt = inputMapPath.filename().stem().string();
    const auto inFilePathNoRemec =
        inFilePathNoExt.substr(0, inFilePathNoExt.length() - 6);

    const auto outputMapPath =
        inputMapPath.parent_path() / (inFilePathNoRemec + ".map");

    logging(Level::Info,
            "Generating {}",
            asColor<Color::green>(outputMapPath.string()));

    std::ofstream {outputMapPath} << rewriter.rewrite(entities) << std::endl;
  }
  catch (std::exception &ex) {
    const auto errorMsg = fmt::format("{}: {}", typeid(ex).name(), ex.what());

    const auto fillerCount = errorMsg.length();

    logging(Level::Error, std::string(fillerCount, 'v'));
    logging(Level::Error, errorMsg);
    logging(Level::Error, std::string(fillerCount, '^'));

    return 1;
  }
  catch (...) {
    const std::string errorMsg = "unknown error";
    const auto fillerCount     = errorMsg.length();

    logging(Level::Error, std::string(fillerCount, 'v'));
    logging(Level::Error, errorMsg);
    logging(Level::Error, std::string(fillerCount, '^'));

    return 2;
  }

  return 0;
}
