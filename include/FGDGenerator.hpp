#pragma once

#include <filesystem>
#include <string>
#include <vector>

class FGDGenerator
{
public:
  std::string generate(
      const std::vector<std::filesystem::path> &pluginsPaths) const noexcept;
};
