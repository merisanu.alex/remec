#pragma once

#include <cstdint>

struct RGB
{
  std::uint8_t r = 0;
  std::uint8_t g = 0;
  std::uint8_t b = 0;

  bool operator==(const RGB &other) const noexcept;
  bool operator<(const RGB &other) const noexcept;
};
