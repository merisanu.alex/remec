#pragma once

#include <common/Face.hpp>
#include <vector>

using Brush = std::vector<Face>;
