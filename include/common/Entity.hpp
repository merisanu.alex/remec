#pragma once

#include <common/Context.hpp>
#include <common/IPlugin.hpp>
#include <filesystem>

class Entity : public IPlugin
{
public:
  Entity(Context context,
         std::size_t originalStartPos,
         std::size_t originalLen,
         PropertiesMap properties,
         std::vector<Brush> brushes);

  std::size_t getOriginalStartPos() const noexcept override;
  std::size_t getOriginalLen() const noexcept override;
  std::string getName() const noexcept override;
  std::vector<Brush> getBrushes() const noexcept override;

  const PropertiesMap &getProperties() const override;
  PropertiesMap getFilteredProperties() const noexcept override;

  const OwnProperty &getOwnProperty(const std::string &name) const override;

  const Context &getContext() const noexcept;

private:
  const Context m_context;

  const std::size_t m_originalStartPos;
  const std::size_t m_originalLen;
  const PropertiesMap m_properties;
  const std::vector<Brush> m_brushes;

  std::string getStringProperty(const std::string &name) const override;
  double getDoubleProperty(const std::string &name) const override;
  int getIntProperty(const std::string &name) const override;
};
