#pragma once

#include <common/Entity.hpp>
#include <fmt/format.h>

template <typename T>
class CustomEntity : public Entity
{
public:
  CustomEntity(Context context,
               std::size_t originalStartPos,
               std::size_t originalLen,
               PropertiesMap properties,
               std::vector<Brush> inBrushes)
      : Entity {context,
                originalStartPos,
                originalLen,
                std::move(properties),
                std::move(inBrushes)}
  {
    const auto &props      = getProperties();
    const auto myClassName = props.at("classname");

    if (myClassName != T::ClassName) {
      throw std::invalid_argument {
          fmt::format("{} initialized with properties of entity type {}",
                      T::ClassName,
                      myClassName)};
    }

    if (not T::Solid) {
      if (not props.count("origin")) {
        throw std::invalid_argument {
            fmt::format("{} requires an origin property", T::ClassName)};
      }
    }
  }

  std::string_view getEntityClassName() const noexcept override final
  {
    return T::ClassName;
  }

  std::string_view getEntityDescription() const noexcept override final
  {
    return T::Description;
  }

  std::string_view getEntityVersion() const noexcept override final
  {
    return T::Version;
  }

  bool isTargetNameMandatory() const noexcept override final
  {
    return T::TargetNameMandatory;
  }

  bool isSolid() const noexcept override final { return T::Solid; }
  bool isStudio() const noexcept override final { return T::Studio; }

  const std::vector<OwnProperty> *
  getOwnProperties() const noexcept override final
  {
    static const auto adjustedOwnProps = [this]
    {
      auto ownProps = T::GetOwnProperties();

      ownProps.push_back(OwnProperty {"_tb_layer", "", "", OwnProperty::Type::String, {}});

      return ownProps;
    }();

    return &adjustedOwnProps;
  }

  std::string getLayer() const noexcept
  {
    const auto tbLayer = getOrDefault<std::string>("_tb_layer");

    if (tbLayer.empty()) {
      return {};
    }

    return fmt::format(R"("_tb_layer" "{}")", tbLayer) + '\n';
  }
};
