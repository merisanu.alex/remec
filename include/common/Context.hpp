#pragma once

#include <filesystem>
#include <string>

namespace spdlog
{
class logger;
} // namespace spdlog

class ITracer;

struct Context
{
  std::string skyName;
  std::filesystem::path halfLifePath;
  std::string tbOrJack;
  spdlog::logger *logger = nullptr;
  ITracer *tracer        = nullptr;
};
