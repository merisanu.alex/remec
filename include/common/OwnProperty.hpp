#pragma once

#include <string>
#include <variant>
#include <vector>

struct OwnProperty
{
  enum class Type
  {
    Integer,
    String,
    Color255,
    Choices,
    Flags,
    TargetDestination,
    TargetSource,
  };

  struct Value
  {
    const int index = 1;
    const std::string label;
    const int defaultValue = 0;
  };

  const std::string name;
  const std::variant<std::string, int, double> defaultValue;
  const std::string description;
  const Type type;
  const std::vector<Value> values;
};
