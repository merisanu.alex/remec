#pragma once

#include <common/Brush.hpp>
#include <common/Context.hpp>
#include <common/OwnProperty.hpp>
#include <common/PropertiesMap.hpp>
#include <tl/expected.hpp>

class IPlugin {
public:
  virtual ~IPlugin() noexcept = default;

  virtual std::size_t getOriginalStartPos() const noexcept = 0;
  virtual std::size_t getOriginalLen() const noexcept      = 0;
  virtual std::string getName() const noexcept             = 0;
  virtual std::vector<Brush> getBrushes() const noexcept   = 0;

  virtual std::string_view getEntityClassName() const noexcept   = 0;
  virtual std::string_view getEntityDescription() const noexcept = 0;
  virtual std::string_view getEntityVersion() const noexcept     = 0;
  virtual bool isTargetNameMandatory() const noexcept            = 0;
  virtual bool isSolid() const noexcept                          = 0;
  virtual bool isStudio() const noexcept                         = 0;

  virtual tl::expected<std::string, std::string> serialize() const          = 0;
  virtual const std::vector<OwnProperty> *getOwnProperties() const noexcept = 0;

  virtual const PropertiesMap &getProperties() const           = 0;
  virtual PropertiesMap getFilteredProperties() const noexcept = 0;

  virtual const OwnProperty &getOwnProperty(const std::string &name) const = 0;

  template <typename T = std::string>
  T getOrDefault(const std::string &name) const;

private:
  virtual std::string getStringProperty(const std::string &name) const = 0;
  virtual double getDoubleProperty(const std::string &name) const      = 0;
  virtual int getIntProperty(const std::string &name) const            = 0;
};

template <typename T>
T IPlugin::getOrDefault(const std::string &name) const
{
  const auto &property = getOwnProperty(name);

  try {
    if constexpr (std::is_same_v<T, double>) {
      return getDoubleProperty(name);
    }
    else if constexpr (std::is_same_v<T, int>) {
      return getIntProperty(name);
    }
    else {
      return getStringProperty(name);
    }
  }
  catch (...) {
    if (const auto defaultValue = std::get_if<T>(&property.defaultValue)) {
      return *defaultValue;
    }

    throw std::runtime_error {"getOrDefault"};
  }
}

#define xstr(s) str(s)
#define str(s) #s

#ifdef _WIN32
#define EXPORT_SYMBOL __declspec(dllexport)
#else
#define EXPORT_SYMBOL
#endif

using newInstance_t = IPlugin *(*)(Context,
                                   std::size_t,
                                   std::size_t,
                                   PropertiesMap,
                                   std::vector<Brush>);
#define REMEC_NEW_INSTANCE_M plugin_newInstance
constexpr auto newInstance_s = xstr(REMEC_NEW_INSTANCE_M);

using deleteInstance_t = void (*)(IPlugin *);
#define REMEC_DELETE_INST_M plugin_deleteInstance
constexpr auto deleteInstance_s = xstr(REMEC_DELETE_INST_M);

using version_t = const char *(*)();
#define REMEC_VERSION_T plugin_version
constexpr auto version_s = xstr(REMEC_VERSION_T);

using className_t = const char *(*)();
#define REMEC_CLASS_NAME_M plugin_getClassName
constexpr auto className_s = xstr(REMEC_CLASS_NAME_M);

using description_t = const char *(*)();
#define REMEC_DESCRIPTION_M plugin_description
constexpr auto description_s = xstr(REMEC_DESCRIPTION_M);

using targetNameMandatory_t = bool (*)();
#define REMEC_TGT_NAME_MANDATORY_M plugin_targetNameMandatory
constexpr auto tgtNameMandatory_s = xstr(REMEC_TGT_NAME_MANDATORY_M);

using solid_t = bool (*)();
#define REMEC_SOLID_M plugin_solid
constexpr auto solid_s = xstr(REMEC_SOLID_M);

using studio_t = bool (*)();
#define REMEC_STUDIO_M plugin_studio
constexpr auto studio_s = xstr(REMEC_STUDIO_M);

using ownProperties_t = const std::vector<OwnProperty> *(*)();
#define REMEC_OWN_PROPS_M plugin_ownProperties
constexpr auto ownProperties_s = xstr(REMEC_OWN_PROPS_M);

#undef xstr
#undef str

#ifndef REGISTER_PLUGIN
#define REGISTER_PLUGIN(type)                                               \
  extern "C" {                                                              \
  EXPORT_SYMBOL IPlugin *REMEC_NEW_INSTANCE_M(Context context,              \
                                              std::size_t originalStartPos, \
                                              std::size_t originalLen,      \
                                              PropertiesMap properties,     \
                                              std::vector<Brush> brushes)   \
  {                                                                         \
    return new type(std::move(context),                                     \
                    originalStartPos,                                       \
                    originalLen,                                            \
                    std::move(properties),                                  \
                    std::move(brushes));                                    \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL const char *REMEC_VERSION_T()                               \
  {                                                                         \
    return type::Version;                                                   \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL const char *REMEC_CLASS_NAME_M()                            \
  {                                                                         \
    return type::ClassName;                                                 \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL const char *REMEC_DESCRIPTION_M()                           \
  {                                                                         \
    return type::Description;                                               \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL bool REMEC_TGT_NAME_MANDATORY_M()                           \
  {                                                                         \
    return type::TargetNameMandatory;                                       \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL bool REMEC_SOLID_M()                                        \
  {                                                                         \
    return type::Solid;                                                     \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL bool REMEC_STUDIO_M()                                       \
  {                                                                         \
    return type::Studio;                                                    \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL const std::vector<OwnProperty> *REMEC_OWN_PROPS_M()         \
  {                                                                         \
    return &type::GetOwnProperties();                                       \
  }                                                                         \
                                                                            \
  EXPORT_SYMBOL void REMEC_DELETE_INST_M(IPlugin *instance)                 \
  {                                                                         \
    delete instance;                                                        \
  }                                                                         \
  }
#endif

struct PluginDeleter {
  PluginDeleter(deleteInstance_t deleterFunc = nullptr);

  PluginDeleter(const PluginDeleter &)            = default;
  PluginDeleter &operator=(const PluginDeleter &) = default;
  PluginDeleter(PluginDeleter &&)                 = default;
  PluginDeleter &operator=(PluginDeleter &&)      = default;

  void operator()(IPlugin *plugin) const noexcept;

  deleteInstance_t m_deleterFunc;
};

using IPluginSmartPtr = std::unique_ptr<IPlugin, PluginDeleter>;
