#pragma once

#include <common/Point3D.hpp>
#include <optional>
#include <string>

struct Face
{
  Point3D point1;
  Point3D point2;
  Point3D point3;

  std::string texture;

  Point3D textureAxis1;
  double textureAxis1Offset = 0.0;

  Point3D textureAxis2;
  double textureAxis2Offset = 0.0;

  double textureRotation = 0.0;
  double textureScaleX   = 1.0;
  double textureScaleY   = 1.0;

  mutable std::optional<Point3D> normal;
  mutable std::optional<double> offset;

  bool operator==(const Face &other) const noexcept;

  Point3D &get_normal_vector() const noexcept;
  void initialize_normal_vector() const noexcept;
  double get_face_offset() const noexcept;

  Face() = default;
  Face(const Point3D &p1, const Point3D &p2, const Point3D &p3);
};

std::optional<Point3D> get_intersection_point(const Face &face1,
                                              const Face &face2,
                                              const Face &face3) noexcept;

enum class PointPosition
{
  BelowFace,
  OnFace,
  AboveFace
};
PointPosition get_point_position_on_face(const Point3D &point,
                                         const Face &face) noexcept;
