#pragma once

#include <filesystem>
#include <string_view>

void *open_library(const std::filesystem::path &path);
void close_library(void *lib);

void *get_library_function(void *lib, std::string_view functionName);

template <typename T>
T get_library_function_T(void *lib, std::string_view functionName)
{
  return reinterpret_cast<T>(get_library_function(lib, functionName));
}
