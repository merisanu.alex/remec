#pragma once

#include "util/RGB.hpp"

#include <algorithm>
#include <common/Brush.hpp>
#include <common/IPlugin.hpp>
#include <common/OwnProperty.hpp>
#include <common/Point3D.hpp>
#include <common/PropertiesMap.hpp>
#include <filesystem>
#include <memory>
#include <set>
#include <string>
#include <vector>

class Entity;

constexpr auto DecimalsCount = 6;

template <typename T, std::size_t Size>
constexpr auto ArrayLen(const T (&)[Size])
{
  return Size;
}

std::string read_whole_file(const std::filesystem::path &path);
std::string erase_comments(std::string_view fileContent);

constexpr auto DefaultReplacementTexture = "AAATRIGGER";

std::vector<Brush> replace_brushes_textures(
    std::vector<Brush> brushes,
    std::string_view newTexture = DefaultReplacementTexture) noexcept;

Brush replace_brush_textures(
    Brush brush,
    std::string_view newTexture = DefaultReplacementTexture) noexcept;

std::vector<Brush> brushes_from_str(std::string_view str);
std::string brushes_to_str(const std::vector<Brush> &brushes);

std::string brush_to_str(const std::vector<Brush> &brushes) = delete;
std::string brush_to_str(const Brush &brush);

std::string get_origin(const std::vector<Brush> &brushes);
std::string origin_to_str(const Point3D &originPoint) noexcept;
Point3D origin_from_str(std::string_view str);

std::string properties_map_to_str(const PropertiesMap &properties) noexcept;

std::string own_properties_to_str(const std::vector<OwnProperty> &ownProperties,
                                  std::string_view className,
                                  std::string_view description,
                                  const bool isSolid,
                                  const bool isStudio) noexcept;

std::string property_type_to_str(const OwnProperty::Type type) noexcept;

class IEntityFactory;

std::vector<IPluginSmartPtr>
get_remec_entities(std::string_view str, const IEntityFactory &entityFactory);

std::string replace_all(const std::string &str,
                        const std::string &find,
                        const std::string &replace) noexcept;

std::vector<std::string> split(const std::string &str, char delim);
std::vector<std::string> split(std::string_view str, char delim);

std::string ltrim(std::string s) noexcept;

template <typename ContainerT,
          typename BinaryOp = std::less<typename ContainerT::value_type>>
ContainerT sort(ContainerT &&data, BinaryOp &&op = {}) noexcept
{
  std::sort(std::begin(data), std::end(data), std::forward<BinaryOp>(op));

  return std::move(data);
}

template <typename ContainerT>
bool contains(const ContainerT &data,
              const typename ContainerT::value_type &value) noexcept
{
  return std::find(std::begin(data), std::end(data), value) != std::end(data);
}

bool SizeAwareLexicographicCompare(std::string_view s1,
                                   std::string_view s2) noexcept;

template <typename ContainerT, typename UnaryOp>
auto map(const ContainerT &input, UnaryOp &&op)
{
  std::vector<decltype(op(std::declval<typename ContainerT::value_type>()))>
      result;

  std::transform(std::begin(input),
                 std::end(input),
                 std::back_inserter(result),
                 std::forward<UnaryOp>(op));

  return result;
}

template <typename ContainerT, typename UnaryOp>
auto filter(const ContainerT &input, UnaryOp &&op)
{
  ContainerT result;

  std::copy_if(std::begin(input),
               std::end(input),
               std::back_inserter(result),
               std::forward<UnaryOp>(op));

  return result;
}

bool ends_with(std::string_view str, std::string_view withWhat);

std::string get_sky_name(std::string_view str) noexcept;

RGB get_dominant_color(const std::vector<std::filesystem::path> &images);

static constexpr auto SharedLibraryExtension()
{
#ifdef _WIN32
  return ".dll";
#else
  return ".so";
#endif
}

using SolidMappingT = std::unordered_map<std::string, bool>;

std::pair<PropertiesMap, std::vector<Brush>>
parse_entity(std::string_view str, const SolidMappingT &solidMapping);

enum class Coordinate {X, Y, Z};
enum class Extremum {Min, Max};

std::pair<double, double>
find_min_max_coordinate(const Brush &brush,
                        const Coordinate &coordinate) noexcept;
std::pair<double, double>
find_min_max_coordinate(const std::vector<Brush> &brushes,
                        const Coordinate &coordinate) noexcept;
std::pair<double, double>
find_min_max_coordinate(const std::set<Point3D> &points,
                        const Coordinate &coordinate) noexcept;

double find_min_coordinate(const Brush &brush,
                           const Coordinate &coordinate) noexcept;
double find_min_coordinate(const std::vector<Brush> &brushes,
                           const Coordinate &coordinate) noexcept;
double find_max_coordinate(const Brush &brush,
                           const Coordinate &coordinate) noexcept;
double find_max_coordinate(const std::vector<Brush> &brushes,
                           const Coordinate &coordinate) noexcept;

double get_point_coordinate_value(const Point3D &point,
                                  const Coordinate &coordinate);

bool isAtty() noexcept;
