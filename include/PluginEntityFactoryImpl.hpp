#pragma once

#include "IEntityFactory.hpp"

#include <filesystem>
#include <memory>
#include <unordered_map>
#include <vector>

class IPlugin;

class PluginEntityFactoryImpl final : public IEntityFactory
{
public:
  PluginEntityFactoryImpl(
      std::filesystem::path halfLifePath,
      std::vector<std::filesystem::path> pluginLibrariesPaths,
      std::string_view tbOrJack);

  IPluginSmartPtr makeEntity(std::string_view str,
                             std::size_t originalStartPos,
                             std::size_t originalLen,
                             std::string_view skyName) const override;

  void *getLibraryForClassName(std::string_view className) const;

private:
  struct LibraryCloser
  {
    void operator()(void *lib) const;
  };

  const std::filesystem::path m_halfLifePath;
  const std::string m_tbOrJack;

  std::vector<std::unique_ptr<void, LibraryCloser>> m_pluginsLibs;

  std::unordered_map<std::string, bool> getSolidMapping() const;
};
