#pragma once

#include <filesystem>
#include <vector>

std::vector<std::filesystem::path>
updateIfNecessary(const std::vector<std::filesystem::path> &pluginsPaths,
                  const std::filesystem::path &pluginsDirPath);
