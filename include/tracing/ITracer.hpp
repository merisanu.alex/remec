#pragma once

#include <cstdint>
#include <optional>
#include <string>
#include <string_view>
#include <unordered_map>

#ifndef REMEC_TRACE
#ifdef _WIN32
#define REMEC_TRACE(category, name, ...)
#define REMEC_TRACE_BEGIN(category, name, ...)
#define REMEC_TRACE_END(category)
#else
#define REMEC_INTERNAL_CONCAT2(a, b) a##b
#define REMEC_INTERNAL_CONCAT(a, b) REMEC_INTERNAL_CONCAT2(a, b)
#define REMEC_UID(prefix) REMEC_INTERNAL_CONCAT(prefix, __LINE__)

#define REMEC_TRACE(category, name, ...)                        \
  const auto REMEC_UID(remec_trace_scope) =                     \
      g_tracer ? g_tracer->trace(category, name, ##__VA_ARGS__) \
               : ITracer::OnExit(nullptr, category)

#define REMEC_TRACE_BEGIN(category, name, ...) \
  if (g_tracer)                                \
  g_tracer->beginTrace(category, name, ##__VA_ARGS__)

#define REMEC_TRACE_END(category) \
  if (g_tracer)                   \
  g_tracer->endTrace(category)
#endif
#endif

enum class Category
{
  remec,
  remec_plugin,
  remec_plugin_discovery,
  remec_entity_factory,
  remec_update_manager,
  remec_rewriter,
  remec_util,
};

class ITracer
{
public:
  virtual ~ITracer() = default;

  virtual void beginTrace(Category category,
                          std::string_view name,
                          const std::optional<std::uint64_t> &id = std::nullopt,
                          const std::unordered_map<std::string, std::string>
                              &params = {}) const = 0;

  virtual void endTrace(Category category) const = 0;

  struct OnExit
  {
    OnExit(const ITracer *tracer, Category category)
        : m_tracer {tracer}
        , m_category {category}
    {
    }

    ~OnExit()
    {
      if (m_tracer) {
        m_tracer->endTrace(m_category);
      }
    }

    OnExit(const OnExit &)            = delete;
    OnExit(OnExit &&)                 = delete;
    OnExit &operator=(const OnExit &) = delete;
    OnExit &operator=(OnExit &&)      = delete;

  private:
    const ITracer *m_tracer;
    Category m_category;
  };

  [[nodiscard]] OnExit
  trace(Category category,
        std::string_view name,
        std::optional<std::uint64_t> id                     = std::nullopt,
        std::unordered_map<std::string, std::string> params = {}) const
  {
    this->beginTrace(category, name, std::move(id), std::move(params));

    return OnExit {this, category};
  }
};

extern ITracer *g_tracer;
