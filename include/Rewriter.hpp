#pragma once

#include <common/IPlugin.hpp>
#include <memory>
#include <string>
#include <vector>

class Rewriter
{
public:
  Rewriter(std::string_view original);

  std::string rewrite(const std::vector<IPluginSmartPtr> &entities) const;

private:
  const std::string_view m_original;
};
