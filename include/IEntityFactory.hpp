#pragma once

#include <common/IPlugin.hpp>
#include <memory>
#include <string_view>

class IEntityFactory
{
public:
  virtual ~IEntityFactory() noexcept = default;

  virtual IPluginSmartPtr makeEntity(std::string_view str,
                                     std::size_t originalStartPos,
                                     std::size_t originalLen,
                                     std::string_view skyName) const = 0;
};
