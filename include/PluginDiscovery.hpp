#pragma once

#include <filesystem>
#include <vector>

std::vector<std::filesystem::path>
discover_plugins(const std::filesystem::path &pluginsDirPath);
