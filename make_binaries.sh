#!/bin/bash

rm -rf release_build || true

STATE=$(sudo virsh domstate win10)

if [ "${STATE}" == "running" ];
then
  echo "[  0] VM already running..."
else
  echo "[  0] Starting VM..."
  sudo virsh start win10 > /dev/null
fi

VM_IP=$(sudo virsh domifaddr win10 | tail -n 2 | head -n 1 | cut -d/ -f1 | cut -d' ' -f21)

ssh alex@${VM_IP} "rmdir /S /Q buildenv" &> /dev/null
ssh_return_code=$?

counter=0
while [ ${ssh_return_code} != 0 ]
do
  echo -en "[0.5] Waiting for VM to start ${counter}...\r"
  counter=$(( $counter + 1 ))

  VM_IP=$(sudo virsh domifaddr win10 | tail -n 2 | head -n 1 | cut -d/ -f1 | cut -d' ' -f21)
  ssh alex@${VM_IP} "rmdir /S /Q buildenv" &> /dev/null
  ssh_return_code=$?
done

echo

set -e

ssh alex@${VM_IP} "mkdir buildenv"
ssh alex@${VM_IP} "cd buildenv && mkdir remec"

shopt -s extglob

echo "[0.9] Copying project..."
scp -r !(*.git|.vscode|compile_commands.json|*build|*.fgd) "alex@${VM_IP}:C:/Users/alex/buildenv/remec" > /dev/null

echo "[  1] Compiling binary..."

ssh alex@${VM_IP} "cd \"C:/Users/alex/buildenv/remec\" && mkdir build && cd build && \
\"C:/Program Files/CMake/bin/cmake.exe\" .. -DREMEC_AUTO_PLUGIN_UPDATE=OFF && \"C:/Program Files/CMake/bin/cmake.exe\" --build . --target ALL_BUILD --config Release --parallel 16 && \
\"C:/Program Files/CMake/bin/cmake.exe\" .. -DREMEC_AUTO_PLUGIN_UPDATE=OFF && \"C:/Program Files/CMake/bin/cmake.exe\" --build . --target RUN_TESTS --config Release --parallel 16"

mkdir release_build || true

scp "alex@${VM_IP}:C:/Users/alex/buildenv/remec/build/Release/remec.exe" release_build/

for plug in `ls -1 src/plugins`
do
  scp "alex@${VM_IP}:C:/Users/alex/buildenv/remec/build/Release/${plug}.dll" release_build/
done

rm -rf linux_build || true
mkdir linux_build && pushd linux_build
cmake -DREMEC_AUTO_PLUGIN_UPDATE=OFF -DCMAKE_BUILD_TYPE=Release .. && make -j 32 && make test && ./remec > ../release_build/remec.fgd
cp remec ../release_build/
cp *.so ../release_build/
popd
rm -rf linux_build

cd release_build && zip remec.zip remec.exe *.dll *.so remec.fgd remec && rm remec.exe remec.fgd remec *.so *.dll

echo "[2.5] Binary generated"


echo "[  3] Shutting VM down..."
sudo virsh shutdown win10 > /dev/null
echo "[3.5] VM shutdown"
