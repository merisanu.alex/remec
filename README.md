# REMEC

REMEC is a meta-entity "compiler", it provides "new" entites that
should make life easier for mappers everywhere.

## New entities

- [**AutoResetSwitch**](src/plugins/AutoResetSwitch/AutoResetSwitch.md)
- [**AutoResetWall**](src/plugins/AutoResetWall/AutoResetWall.md)
- [**DelayedTeleport**](src/plugins/DelayedTeleport/DelayedTeleport.md)
- [**HPBooster**](src/plugins/HPBooster/HPBooster.md)
- [**MultiBhopBlock**](src/plugins/MultiBhopBlock/MultiBhopBlock.md)
- [**MultiManager**](src/plugins/MultiManager/MultiManager.md)
- [**MultiTargetTeleport**](src/plugins/MultiTargetTeleport/MultiTargetTeleport.md)
- [**NTrigger**](src/plugins/NTrigger/NTrigger.md)
- [**TriggerRandomTarget**](src/plugins/TriggerRandomTarget/TriggerRandomTarget.md)
- [**SlipRamp**](src/plugins/SlipRamp/SlipRamp.md)
- [**SkyLight**](src/plugins/SkyLight/SkyLight.md)
- [**LitModel**](src/plugins/LitModel/LitModel.md)
- [**TriggerSequence**](src/plugins/TriggerSequence/TriggerSequence.md)

## Installation

Go to the [Releases](https://gitlab.com/merisanu.alex/remec/-/releases) page and download the latest `remec.zip` and extract the zip somewhere, then continue to [Usage](#usage).

## Usage

1. Load `remec.fgd` in `Hammer/Jack/Trenchbroom`.

   The new entites should now be available in the editor.

2. Press **F9** in your map editor, select the `Copy file` step and change the `Parameters` field from:

   `$path/$file.$ext $bspdir/$file.$ext`

   to

   `$path/$file.$ext $bspdir/$file.remec.$ext`

3. Add a new compilation step by pressing "New" and move the new step so it is after the `Copy file` step and before the `$csg.exe` step
4. Select the newly added step, press the `Cmds` button, choose executable and navigate to where you extracted `REMEC` and pick the `remec` executable
5. Set the `Parameters` field to:
   1. `$bspdir/$file.remec.$ext tb C:\Path\to\your\HalfLife` if you are using **TrenchBroom**
   2. `$bspdir/$file.remec.$ext jack C:\Path\to\your\HalfLife` if you are using **Jack/Hammer**
6. Check `Wait for termination`, `Use Process Window` and `Ensure File Post exists` where you also need to add `$bspdir/$file.$ext`
7. That is all! Press `OK` and it should run `remec` and then compile your map normally.

## Compiling from source

### Dependencies

#### Required

- C++ 20 compiler (GCC, Clang, MSVC)
- [CMake](https://cmake.org)
- [OpenSSL 1.1.1](https://github.com/openssl/openssl) _optional: only needed when compiling with REMEC_AUTO_PLUGIN_UPDATE=ON_

#### Auto installed

- [fmt](https://github.com/fmtlib/fmt)
- [nlohmann](https://github.com/nlohmann/json)
- [httplib](https://github.com/yhirose/cpp-httplib)
- [11Zip](https://github.com/Sygmei/11Zip.git) _optional: only needed when compiling with REMEC_AUTO_PLUGIN_UPDATE=ON_
- [tga](https://github.com/aseprite/tga)
- [spdlog](https://github.com/gabime/spdlog)
- [Catch2](https://github.com/catchorg/Catch2)

### Extra

Change `REMEC_AUTO_PLUGIN_UPDATE=OFF` to `REMEC_AUTO_PLUGIN_UPDATE=ON` to enable auto updating of plugins (requires OpenSSL 3.0.0 and 11Zip)

### Compiling on Linux/OSX

```bash
mkdir build
cd build
cmake -DREMEC_AUTO_PLUGIN_UPDATE=OFF ..
cmake --build .
```

### Compiling on Windows

```bash
mkdir build
cd build
cmake -DREMEC_AUTO_PLUGIN_UPDATE=OFF ..
cmake --build . --target ALL_BUILD --config Release
```
